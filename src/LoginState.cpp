#include "LoginState.h"

LoginState::LoginState(QWidget* parent /*QMainWindow* window, Ui_Login uiLoginIn*/) : QWidget(parent)
{ 
	setupUserInterface();

	setupSignals();
}

void LoginState::setupUserInterface()
{
	uiLogin.setupUi(this);

}

void LoginState::setupSignals()
{
	connect(uiLogin.loginBtn, SIGNAL(clicked()), this, SLOT(login()));
	connect(uiLogin.signupBtn, SIGNAL(clicked()), this, SLOT(signUp()));

	connect(uiLogin.createAccountBtn, SIGNAL(clicked()), this, SLOT(createAccount()));

}

void LoginState::login()
{
	std::shared_ptr<GP> tempGP;

	std::shared_ptr<Specialist> tempSpecialist;


	QString username;
	QString password;

	username = uiLogin.userNamelineEdit->text();
	password = uiLogin.passwordLineEdit->text();

	QSqlQuery query;

	bool bConfirmed = false;


	if (query.exec("SELECT * FROM gps WHERE username='" + username + "' and password='" + password + "'"))
	{
		int count = 0;

		while (query.next()) 
		{
			count++;
		}

		if (count == 1)
		{

			query.first();

			uiLogin.loginStatusLabel->setText("Username and Password are correct!");
			bConfirmed = true;

			unsigned int gpId = query.value(0).toInt();

			tempGP = std::make_shared<GP>(GP(gpId, "",query.value(1).toString(), query.value(2).toString(), query.value(3).toString(), query.value(4).toString()));
		}

		if (count > 1)
		{
			uiLogin.loginStatusLabel->setText("Duplicate username and password!");
		}

		if (count < 1)
		{
			uiLogin.loginStatusLabel->setText("Username and Password are not correct!");
			bConfirmed = false;
		}

	}

	if (bConfirmed)
	{
		// Sets the current active gp to be the 
		UserManager::Instance()->setActiveGP(tempGP);


		//INSERT INTO gps(first_name, last_name, username, password, email, phone_number) VALUES('Dinis', 'Firmino', 'DYN', 'pass', 'dinismarques95@gmail.com', 07748935562)


		StateManager::StateManagerInstance()->changeState(new MainScreenState());
	}
	else {

	}





}

void LoginState::signUp()
{
	//uiLogin.loginPage->hide();

	uiLogin.stackedWidget->setCurrentIndex(1);
	//uiLogin.signupPage->show();

}

void LoginState::createAccount()
{
	bool ok = false;

	QString firstName = uiLogin.firstNameEdit->text();
	QString lastName = uiLogin.lastNameEdit->text();
	QString user = uiLogin.userNameEditSignup->text();
	QString pass = uiLogin.passwordEditSignup->text();
	QString accountType = uiLogin.accountType->currentText();

	std::shared_ptr<Facility> pNewFacility;
	std::shared_ptr<Address> pNewAddress;


	QString name = uiLogin.nameEdit->text();
	QString type = uiLogin.facilityType->currentText();

	QString street = uiLogin.streetEdit->text();
	QString city = uiLogin.cityEdit->text();
	QString postcode = uiLogin.postcodeEdit->text();

	

	if (!uiLogin.firstNameEdit->text().isEmpty()&& 
		!uiLogin.lastNameEdit->text().isEmpty() && 
		!uiLogin.userNameEditSignup->text().isEmpty() && 
		!uiLogin.passwordEditSignup->text().isEmpty() &&
		!uiLogin.nameEdit->text().isEmpty() &&
		!uiLogin.streetEdit->text().isEmpty() &&
		!uiLogin.cityEdit->text().isEmpty()&&
		!uiLogin.postcodeEdit->text().isEmpty()) {
		ok = true;
	}
	else {
		uiLogin.errorLabel->setText("Please fill in all fields to succesfully create your account");
	}


	if (ok) {
		// Insert new patient query 
		QSqlQuery insertNewGPQuery;
		QSqlQuery insertNewAddress;
		QSqlQuery insertNewFacility;


		insertNewAddress.prepare("INSERT INTO addresses (street, city, post_code) VALUES (:strt,:ct, :pstcd)");
		insertNewAddress.bindValue(":strt", street);
		insertNewAddress.bindValue(":ct", city);
		insertNewAddress.bindValue(":pstcd", postcode);

		if (!insertNewAddress.exec()) qDebug() << insertNewAddress.lastError().text();

		unsigned int newAddressId;

		if (!insertNewAddress.exec("SELECT LAST_INSERT_ID()")) qDebug() << insertNewAddress.lastError().text();
		insertNewAddress.first();

		newAddressId = insertNewAddress.value(0).toInt();


		insertNewFacility.prepare("INSERT INTO facility (name, type, addresses_id) VALUES (:nm,:pType, :adId)");
		insertNewFacility.bindValue(":nm", name);
		insertNewFacility.bindValue(":pType", type);
		insertNewFacility.bindValue(":adId", newAddressId);


		if (!insertNewFacility.exec()) qDebug() << insertNewFacility.lastError().text();

		unsigned int newFacilityID;

		if (!insertNewFacility.exec("SELECT LAST_INSERT_ID()")) qDebug() << insertNewFacility.lastError().text();
		insertNewFacility.first();

		newFacilityID = insertNewFacility.value(0).toInt();


		pNewAddress = std::make_shared<Address>(Address(newAddressId, street, city, postcode));

		pNewFacility = std::make_shared<Facility>(Facility(newFacilityID, name, type));
		pNewFacility->setAddress(pNewAddress);

		insertNewGPQuery.prepare("INSERT INTO gps (first_name, last_name, username, password, email, phone_number, facility_id) VALUES (:fn,:ln, :usr, :pass, :eml, :number, :facId)");
		insertNewGPQuery.bindValue(":fn", firstName);
		insertNewGPQuery.bindValue(":ln", lastName);
		insertNewGPQuery.bindValue(":usr",user);
		insertNewGPQuery.bindValue(":pass", pass);
		insertNewGPQuery.bindValue(":eml", "");
		insertNewGPQuery.bindValue(":number", 0);
		insertNewGPQuery.bindValue(":facId", newFacilityID);


		if (!insertNewGPQuery.exec()) qDebug() << insertNewGPQuery.lastError().text();

		uiLogin.stackedWidget->setCurrentIndex(0);

	}
 

	//uiLogin.signupPage->show();

}



