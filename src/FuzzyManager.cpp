#include "FuzzyManager.h"

FuzzyManager * FuzzyManager::pFuzzyManager;

FuzzyManager * FuzzyManager::Instance()
{
	if (!pFuzzyManager)
	{
		return pFuzzyManager = new FuzzyManager();
	}
	else {

		return pFuzzyManager;
	}
}

FuzzyManager::FuzzyManager()
{
	pSymptomAssessor = std::make_shared<SymptomAssessor>();
	pHealthAssessor = std::make_shared<HealthAssessor>();
	pAgeAssessor = std::make_shared<AgeAssessor>();

	pEmergencyAssessor = std::make_shared<EmergencyAssessor>();
	pLifestyleAssessor = std::make_shared<LifestyleAssessor>();
}

FuzzyManager::~FuzzyManager()
{

}


double FuzzyManager::getSymptomSeverity(std::shared_ptr<Symptom> symptomIn)
{
	pSymptomAssessor->setDiscomfort(symptomIn->getDiscomfort());
	pSymptomAssessor->setDuration(symptomIn->getDurationInDays());

	return pSymptomAssessor->getSeverity();
}

double FuzzyManager::evaluateReferralEmergency(std::shared_ptr<Referral> referralIn)
{
	// The referral to evaluate the emergency of 
	std::shared_ptr<Referral> referralToEvaluate = referralIn;

	// Evaluate age
	unsigned int age = referralToEvaluate->getPatient()->getAge();

	pAgeAssessor->setAge(age);
	double priority = pAgeAssessor->getPriority();

	// Evaluate symptoms 
	double fTotalSeverity = referralToEvaluate->getOverallSymptomSeverity();

	// Evaluate health 
	unsigned int bloodPressure = referralToEvaluate->getPatient()->getMedicalData()->getBloodPressure();
	unsigned int cholesterol = referralToEvaluate->getPatient()->getMedicalData()->getCholesterol();

	pHealthAssessor->setBloodPressure(bloodPressure);
	pHealthAssessor->setCholesterol(cholesterol);

	double fHealth = pHealthAssessor->getHealth();

	// Evaluate lifestyle 
	//QString exerciseStatus = referralToEvaluate->getPatient()->getLifestyle()->getExerciseStatus();
	//QString drinkerStatus = referralToEvaluate->getPatient()->getLifestyle()->getDrinkerStatus();
	//QString smokerStatus = referralToEvaluate->getPatient()->getLifestyle()->getSmokerStatus();

	//pLifestyleAssessor->setAlcohol(drinkerStatus)


	// Evaluate emergency of referral 
	pEmergencyAssessor->setOverralSymptomSeverity(fTotalSeverity);
	pEmergencyAssessor->setPriority(priority);
	pEmergencyAssessor->setHealth(fHealth);


	double emergency = pEmergencyAssessor->getEmergency();


	return emergency;


}




