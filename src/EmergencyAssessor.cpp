#include "EmergencyAssessor.h"


EmergencyAssessor::EmergencyAssessor()
{
	engine = new Engine;
	engine->setName("EmergencyAssessment");

	inputVariable1 = new InputVariable;
	inputVariable1->setEnabled(true);
	inputVariable1->setName("OverallSymptomSeverity");
	inputVariable1->setRange(0.000, 60.000);
	inputVariable1->addTerm(new Ramp("Low", 8.000, 3.500));
	inputVariable1->addTerm(new Ramp("Low", 3.000, 3.000));
	inputVariable1->addTerm(new Ramp("VeryHigh", 20.000, 29.400));
	inputVariable1->addTerm(new Trapezoid("Medium", 3.500, 7.000, 13.000, 16.500));
	inputVariable1->addTerm(new Trapezoid("High", 13.200, 15.900, 22.500, 25.800));
	engine->addInputVariable(inputVariable1);

	inputVariable2 = new InputVariable;
	inputVariable2->setEnabled(true);
	inputVariable2->setName("Health");
	inputVariable2->setRange(0.000, 10.000);
	inputVariable2->addTerm(new Rectangle("VeryGood", 0.000, 1.500));
	inputVariable2->addTerm(new Rectangle("Good", 1.500, 3.500));
	inputVariable2->addTerm(new Rectangle("Medium", 3.500, 6.500));
	inputVariable2->addTerm(new Rectangle("Bad", 6.500, 8.500));
	inputVariable2->addTerm(new Rectangle("VeryBad", 8.500, 10.000));
	engine->addInputVariable(inputVariable2);

	inputVariable3 = new InputVariable;
	inputVariable3->setEnabled(true);
	inputVariable3->setName("Priority");
	inputVariable3->setRange(0.000, 10.000);
	inputVariable3->addTerm(new Ramp("VeryLow", 1.400, 0.900));
	inputVariable3->addTerm(new Trapezoid("Low", 0.800, 1.200, 3.500, 4.000));
	inputVariable3->addTerm(new Trapezoid("Medium", 3.200, 3.750, 6.250, 7.000));
	inputVariable3->addTerm(new Trapezoid("High", 6.000, 6.500, 8.600, 9.000));
	inputVariable3->addTerm(new Ramp("VeryHigh", 8.500, 9.000));
	engine->addInputVariable(inputVariable3);

	outputVariable = new OutputVariable;
	outputVariable->setEnabled(true);
	outputVariable->setName("Emergency");
	outputVariable->setRange(0.000, 10.000);
	outputVariable->fuzzyOutput()->setAccumulation(new Maximum);
	outputVariable->setDefuzzifier(new Centroid(200));
	outputVariable->setDefaultValue(fl::nan);
	outputVariable->setLockPreviousOutputValue(false);
	outputVariable->setLockOutputValueInRange(false);
	outputVariable->addTerm(new Ramp("VeryLow", 1.400, 0.900));
	outputVariable->addTerm(new Trapezoid("Low", 0.800, 1.200, 3.500, 4.000));
	outputVariable->addTerm(new Trapezoid("Medium", 3.200, 3.750, 6.250, 7.000));
	outputVariable->addTerm(new Trapezoid("High", 6.000, 6.500, 8.600, 9.000));
	outputVariable->addTerm(new Ramp("VeryHigh", 8.500, 9.000));
	engine->addOutputVariable(outputVariable);

	ruleBlock = new RuleBlock;
	ruleBlock->setEnabled(true);
	ruleBlock->setName("EmergencyRuleBlock");
	ruleBlock->setConjunction(new Minimum);
	ruleBlock->setDisjunction(fl::null);
	ruleBlock->setActivation(new Minimum);
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is Low and Health is VeryGood and Priority is VeryLow then Emergency is VeryLow", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is Low and Health is VeryGood and Priority is Low then Emergency is VeryLow", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is Low and Health is VeryGood and Priority is Medium then Emergency is Low", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is Low and Health is VeryGood and Priority is High then Emergency is Medium", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is Low and Health is VeryGood and Priority is VeryHigh then Emergency is High", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is Low and Health is Good and Priority is VeryLow then Emergency is VeryLow", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is Low and Health is Good and Priority is Low then Emergency is Low", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is Low and Health is Good and Priority is Medium then Emergency is Medium", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is Low and Health is Good and Priority is High then Emergency is Medium", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is Low and Health is Good and Priority is VeryHigh then Emergency is High", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is Low and Health is Medium and Priority is VeryLow then Emergency is Low", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is Low and Health is Medium and Priority is Low then Emergency is Medium", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is Low and Health is Medium and Priority is Medium then Emergency is Medium", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is Low and Health is Medium and Priority is High then Emergency is VeryHigh", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is Low and Health is Medium and Priority is VeryHigh then Emergency is VeryHigh", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is Low and Health is Bad and Priority is VeryLow then Emergency is High", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is Low and Health is Bad and Priority is Low then Emergency is High", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is Low and Health is Bad and Priority is Medium then Emergency is High", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is Low and Health is Bad and Priority is High then Emergency is High", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is Low and Health is Bad and Priority is VeryHigh then Emergency is VeryHigh", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is Low and Health is VeryBad and Priority is VeryLow then Emergency is High", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is Low and Health is VeryBad and Priority is Low then Emergency is High", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is Low and Health is VeryBad and Priority is Medium then Emergency is High", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is Low and Health is VeryBad and Priority is High then Emergency is VeryHigh", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is Low and Health is VeryBad and Priority is VeryHigh then Emergency is VeryHigh", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is Medium and Health is VeryGood and Priority is VeryLow then Emergency is VeryLow", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is Medium and Health is VeryGood and Priority is Low then Emergency is Low", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is Medium and Health is VeryGood and Priority is Medium then Emergency is Medium", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is Medium and Health is VeryGood and Priority is High then Emergency is Medium", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is Medium and Health is VeryGood and Priority is VeryHigh then Emergency is Medium", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is Medium and Health is Good and Priority is VeryLow then Emergency is Low", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is Medium and Health is Good and Priority is Low then Emergency is Low", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is Medium and Health is Good and Priority is Medium then Emergency is Medium", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is Medium and Health is Good and Priority is High then Emergency is Medium", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is Medium and Health is Good and Priority is VeryHigh then Emergency is High", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is Medium and Health is Medium and Priority is VeryLow then Emergency is Low", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is Medium and Health is Medium and Priority is Low then Emergency is Low", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is Medium and Health is Medium and Priority is Medium then Emergency is Medium", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is Medium and Health is Medium and Priority is High then Emergency is High", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is Medium and Health is Medium and Priority is VeryHigh then Emergency is High", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is Medium and Health is Bad and Priority is VeryLow then Emergency is Medium", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is Medium and Health is Bad and Priority is Low then Emergency is High", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is Medium and Health is Bad and Priority is Medium then Emergency is High", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is Medium and Health is Bad and Priority is High then Emergency is VeryHigh", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is Medium and Health is Bad and Priority is VeryHigh then Emergency is VeryHigh", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is Medium and Health is VeryBad and Priority is VeryLow then Emergency is High", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is Medium and Health is VeryBad and Priority is Low then Emergency is High", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is Medium and Health is VeryBad and Priority is Medium then Emergency is High", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is Medium and Health is VeryBad and Priority is High then Emergency is VeryHigh", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is Medium and Health is VeryBad and Priority is VeryHigh then Emergency is VeryHigh", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is High and Health is VeryGood and Priority is VeryLow then Emergency is Medium", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is High and Health is VeryGood and Priority is Low then Emergency is Medium", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is High and Health is VeryGood and Priority is Medium then Emergency is High", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is High and Health is VeryGood and Priority is High then Emergency is High", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is High and Health is VeryGood and Priority is VeryHigh then Emergency is VeryHigh", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is High and Health is Good and Priority is VeryLow then Emergency is Medium", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is High and Health is Good and Priority is Low then Emergency is High", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is High and Health is Good and Priority is Medium then Emergency is High", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is High and Health is Good and Priority is High then Emergency is High", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is High and Health is Good and Priority is VeryHigh then Emergency is High", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is High and Health is Medium and Priority is VeryLow then Emergency is High", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is High and Health is Medium and Priority is Low then Emergency is High", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is High and Health is Medium and Priority is Medium then Emergency is High", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is High and Health is Medium and Priority is High then Emergency is VeryHigh", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is High and Health is Medium and Priority is VeryHigh then Emergency is VeryHigh", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is High and Health is Bad and Priority is VeryLow then Emergency is High", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is High and Health is Bad and Priority is Low then Emergency is VeryHigh", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is High and Health is Bad and Priority is Medium then Emergency is VeryHigh", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is High and Health is Bad and Priority is High then Emergency is VeryHigh", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is High and Health is Bad and Priority is VeryHigh then Emergency is VeryHigh", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is High and Health is VeryBad and Priority is VeryLow then Emergency is VeryHigh", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is High and Health is VeryBad and Priority is Low then Emergency is VeryHigh", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is High and Health is VeryBad and Priority is Medium then Emergency is VeryHigh", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is High and Health is VeryBad and Priority is High then Emergency is VeryHigh", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is High and Health is VeryBad and Priority is VeryHigh then Emergency is VeryHigh", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is VeryHigh and Health is VeryGood and Priority is VeryLow then Emergency is Low", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is VeryHigh and Health is VeryGood and Priority is Low then Emergency is Medium", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is VeryHigh and Health is VeryGood and Priority is Medium then Emergency is Medium", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is VeryHigh and Health is VeryGood and Priority is High then Emergency is High", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is VeryHigh and Health is VeryGood and Priority is VeryHigh then Emergency is VeryHigh", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is VeryHigh and Health is Good and Priority is VeryLow then Emergency is Medium", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is VeryHigh and Health is Good and Priority is Low then Emergency is Medium", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is VeryHigh and Health is Good and Priority is Medium then Emergency is High", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is VeryHigh and Health is Good and Priority is High then Emergency is VeryHigh", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is VeryHigh and Health is Good and Priority is VeryHigh then Emergency is VeryHigh", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is VeryHigh and Health is Medium and Priority is VeryLow then Emergency is Medium", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is VeryHigh and Health is Medium and Priority is Low then Emergency is VeryHigh", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is VeryHigh and Health is Medium and Priority is Medium then Emergency is VeryHigh", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is VeryHigh and Health is Medium and Priority is High then Emergency is VeryHigh", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is VeryHigh and Health is Medium and Priority is VeryHigh then Emergency is VeryHigh", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is VeryHigh and Health is Bad and Priority is VeryLow then Emergency is High", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is VeryHigh and Health is Bad and Priority is Low then Emergency is High", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is VeryHigh and Health is Bad and Priority is Medium then Emergency is VeryHigh", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is VeryHigh and Health is Bad and Priority is High then Emergency is VeryHigh", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is VeryHigh and Health is Bad and Priority is VeryHigh then Emergency is VeryHigh", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is VeryHigh and Health is VeryBad and Priority is VeryLow then Emergency is VeryHigh", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is VeryHigh and Health is VeryBad and Priority is Low then Emergency is VeryHigh", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is VeryHigh and Health is VeryBad and Priority is Medium then Emergency is VeryHigh", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is VeryHigh and Health is VeryBad and Priority is High then Emergency is VeryHigh", engine));
	ruleBlock->addRule(fl::Rule::parse("if OverallSymptomSeverity is VeryHigh and Health is VeryBad and Priority is VeryHigh then Emergency is VeryHigh", engine));
	engine->addRuleBlock(ruleBlock);
}

void EmergencyAssessor::setOverralSymptomSeverity(double overallSymptomSeverityIn)
{
	engine->setInputValue("OverallSymptomSeverity", overallSymptomSeverityIn);

}

void EmergencyAssessor::setHealth(double healthIn)
{
	engine->setInputValue("Health", healthIn);

}

void EmergencyAssessor::setPriority(double priorityIn)
{
	engine->setInputValue("Priority", priorityIn);

}

double EmergencyAssessor::getEmergency()
{
	engine->process();
	return engine->getOutputValue("Emergency");
}


