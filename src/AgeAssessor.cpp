#include "AgeAssessor.h"


AgeAssessor::AgeAssessor()
{

	engine = new Engine;
	engine->setName("GeneralAssessment");

	inputVariable = new InputVariable;
	inputVariable->setEnabled(true);
	inputVariable->setName("Age");
	inputVariable->setRange(0.000, 100.000);
	inputVariable->addTerm(new Trapezoid("Old", 46.000, 50.000, 57.500, 61.000));
	inputVariable->addTerm(new Ramp("VeryOld", 55.500, 65.000));
	inputVariable->addTerm(new Trapezoid("YoungAdult", 19.000, 23.000, 30.000, 35.000));
	inputVariable->addTerm(new Ramp("VeryYoung", 13.000, 11.000));
	inputVariable->addTerm(new Trapezoid("Young", 11.000, 13.500, 19.000, 21.000));
	inputVariable->addTerm(new Trapezoid("Adult", 31.000, 34.500, 46.500, 50.000));
	engine->addInputVariable(inputVariable);

	outputVariable = new OutputVariable;
	outputVariable->setEnabled(true);
	outputVariable->setName("Priority");
	outputVariable->setRange(0.000, 10.000);
	outputVariable->fuzzyOutput()->setAccumulation(new Maximum);
	outputVariable->setDefuzzifier(new Centroid(200));
	outputVariable->setDefaultValue(fl::nan);
	outputVariable->setLockPreviousOutputValue(false);
	outputVariable->setLockOutputValueInRange(false);
	outputVariable->addTerm(new Ramp("VeryLow", 1.400, 0.900));
	outputVariable->addTerm(new Trapezoid("Low", 0.800, 1.200, 3.500, 4.000));
	outputVariable->addTerm(new Trapezoid("Medium", 3.200, 3.750, 6.250, 7.000));
	outputVariable->addTerm(new Trapezoid("High", 6.000, 6.500, 8.600, 9.000));
	outputVariable->addTerm(new Ramp("VeryHigh", 8.500, 9.000));
	engine->addOutputVariable(outputVariable);

	ruleBlock = new RuleBlock;
	ruleBlock->setEnabled(true);
	ruleBlock->setName("Symptom_Severity_Ruleblock");
	ruleBlock->setConjunction(new Minimum);
	ruleBlock->setDisjunction(fl::null);
	ruleBlock->setActivation(new Minimum);
	ruleBlock->addRule(fl::Rule::parse("if Age is VeryYoung then Priority is VeryHigh", engine));
	ruleBlock->addRule(fl::Rule::parse("if Age is Old then Priority is High", engine));
	ruleBlock->addRule(fl::Rule::parse("if Age is VeryOld then Priority is VeryHigh", engine));
	ruleBlock->addRule(fl::Rule::parse("if Age is YoungAdult then Priority is Medium", engine));
	ruleBlock->addRule(fl::Rule::parse("if Age is Young then Priority is High", engine));
	ruleBlock->addRule(fl::Rule::parse("if Age is Adult then Priority is Low", engine));
	engine->addRuleBlock(ruleBlock);

}

void AgeAssessor::setAge(int ageIn)
{
	engine->setInputValue("Age", ageIn);
}

double AgeAssessor::getPriority()
{
	engine->process();
	return engine->getOutputValue("Priority");
}


