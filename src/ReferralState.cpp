#include "ReferralState.h"

ReferralState::ReferralState(bool bViewReferral, QWidget* parent) : QWidget(parent)
{

	setupUserInterface();

	setupSignals();

	if (bViewReferral)
	{
		uiReferral.stackedWidget->setCurrentIndex(1);

		loadActiveReferral();

		updateLists();
		refreshSpecialist();
		refreshLists();

	}
	else {
		uiReferral.stackedWidget->setCurrentIndex(0);

		// Load patients into table view for selecting new patient for referral

		patientSelectQueryModel = new QSqlQueryModel();

		QSqlQuery query;

		query.prepare("SELECT * FROM patients");

		query.exec();

		patientSelectQueryModel->setQuery(query);

		uiReferral.patientTableView->setModel(patientSelectQueryModel);

	}


}

void ReferralState::setupUserInterface()
{
	uiReferral.setupUi(this);

}

void ReferralState::setupSignals()
{

	connect(uiReferral.newPatientReferralBtn, SIGNAL(clicked()), this, SLOT(newReferral()));


	connect(uiReferral.patientTableView, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(loadExistingPatient(const QModelIndex&)));

	connect(uiReferral.submitButton, SIGNAL(clicked()), this, SLOT(submitReferral()));
	connect(uiReferral.saveButton, SIGNAL(clicked()), this, SLOT(saveReferral()));

	connect(uiReferral.backButton, SIGNAL(clicked()), this, SLOT(back()));
	connect(uiReferral.healthDataBtn, SIGNAL(clicked()), this, SLOT(viewHealthData()));
	connect(uiReferral.lifestyleData, SIGNAL(clicked()), this, SLOT(viewLifestyleData()));
	connect(uiReferral.chooseSpecialistBtn, SIGNAL(clicked()), this, SLOT(chooseSpecialist()));

	connect(uiReferral.refreshSpecialist, SIGNAL(clicked()), this, SLOT(refreshSpecialist()));

	connect(uiReferral.addSymptomBtn, SIGNAL(clicked()), this, SLOT(addSymptom()));
	connect(uiReferral.refreshListsBtn, SIGNAL(clicked()), this, SLOT(refreshLists()));

	connect(uiReferral.symptomsTableView, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(viewSymptom(const QModelIndex&)));

	connect(uiReferral.treatmentsTableView, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(viewTreatment(const QModelIndex&)));
	//connect(uiReferral.currentConditionsList, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(loadExistingPatient(const QModelIndex&)));
	//connect(uiReferral.pastConditionsList, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(loadExistingPatient(const QModelIndex&)));
	//connect(uiReferral.familyConditionsList, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(loadExistingPatient(const QModelIndex&)));


	connect(uiReferral.newPatientReferralBtn, SIGNAL(clicked()), this, SLOT(newReferral()));

	connect(uiReferral.sendReferralBtn, SIGNAL(clicked()), this, SLOT(confirmReferral()));
	connect(uiReferral.submitButton, SIGNAL(clicked()), this, SLOT(submitReferral()));


	connect(uiReferral.evaluateEmergencyBtn, SIGNAL(clicked()), this, SLOT(evaluateEmergency()));

}

void ReferralState::loadNewReferral() //!< Insert a new referral entry into the database for updating  
{

	std::shared_ptr<Referral> tempReferral;
	std::shared_ptr<Patient> tempPatient;
	std::shared_ptr<Specialist>tempSpecialist;



	// Insert new patient query 
	QSqlQuery insertNewPatientQuery;
	QSqlQuery insertNewSpecialistQuery;

	QSqlQuery insertNewMedicalData;
	QSqlQuery insertNewLifestyleData;



	if (!insertNewPatientQuery.exec("INSERT INTO patients (first_name, last_name, age, gender, height, weight, address, city, post_code, phone_number, email) VALUES ('','','','','','','','','','','')")) qDebug() << insertNewPatientQuery.lastError().text();
	
	unsigned int newPatientID;
	
	if (!insertNewPatientQuery.exec("SELECT LAST_INSERT_ID()")) qDebug() << insertNewPatientQuery.lastError().text();
	insertNewPatientQuery.first();

	newPatientID = insertNewPatientQuery.value(0).toInt();


	if (insertNewMedicalData.prepare("INSERT INTO health_data (patients_id, blood_pressure, cholesterol) VALUES (:id, :bp, :chol)")) qDebug() << insertNewMedicalData.lastError().text();
	insertNewMedicalData.bindValue(":id", newPatientID);
	insertNewMedicalData.bindValue(":bp", 60);
	insertNewMedicalData.bindValue(":chol", 90);


	if (!insertNewMedicalData.exec()) qDebug() << insertNewMedicalData.lastError().text();

	if (insertNewLifestyleData.prepare("INSERT INTO lifestyle (patients_id, smoker, exercise, drinker) VALUES (:id, :smk, :exer, :drink)")) qDebug() << insertNewLifestyleData.lastError().text();
	insertNewLifestyleData.bindValue(":id", newPatientID);
	insertNewLifestyleData.bindValue(":smk", "NONE");
	insertNewLifestyleData.bindValue(":exer", "NONE");
	insertNewLifestyleData.bindValue(":drink", "NONE");


	if (!insertNewLifestyleData.exec()) qDebug() << insertNewLifestyleData.lastError().text();


	std::shared_ptr<Address> newAddress = std::make_shared<Address>();
	tempPatient = std::make_shared<Patient>(Patient(newPatientID, QString(""), QString(""), 0));
	tempPatient->setAddress(newAddress);
	// Insert new referral query 





	unsigned int newReferralID;
	QString defaultStatus = "NEW";
	QString defaultReason = QString("");

	QSqlQuery insertNewReferralQuery;

	insertNewReferralQuery.prepare("INSERT INTO referrals (patient_id, gps_id, status, referral_reason, referral_date) VALUES (:p_id, :gp_id, :stat, :reason, :date)");
	insertNewReferralQuery.bindValue(":p_id", tempPatient->getID());
	insertNewReferralQuery.bindValue(":gp_id", UserManager::Instance()->getActiveGP()->getID());
	//insertNewReferralQuery.bindValue(":spec_id", newSpecialistID);
	insertNewReferralQuery.bindValue(":stat", defaultStatus);
	insertNewReferralQuery.bindValue(":reason", defaultReason);
	insertNewReferralQuery.bindValue(":date", QDate::currentDate() );

	if (!insertNewReferralQuery.exec()) qDebug() << insertNewReferralQuery.lastError().text();


	if (!insertNewReferralQuery.exec("SELECT LAST_INSERT_ID()")) qDebug() << insertNewReferralQuery.lastError().text();
	insertNewReferralQuery.last();

	newReferralID = insertNewReferralQuery.value(0).toInt();



	tempReferral = std::make_shared<Referral>(Referral(newReferralID, QDate::currentDate()));
	tempReferral->setReferralReason(defaultReason);
	tempReferral->setStatus(defaultStatus);
	tempReferral->addSpecialist(tempSpecialist);
	tempReferral->addGeneralPractitioner(UserManager::Instance()->getActiveGP());
	tempReferral->addPatient(tempPatient);
		

	//if (!query.exec("SELECT * FROM referrals")) qDebug() << query.lastError().text();

	ReferralManager::Instance()->setActiveReferral(tempReferral);
}

void ReferralState::loadActiveReferral()
{

	uiReferral.referralID->setText(QString::number(ReferralManager::Instance()->getActiveReferral()->getID()));
	uiReferral.referralDate->setDate(ReferralManager::Instance()->getActiveReferral()->getDate());

	uiReferral.patientIdTextEdit->setText(QString::number(ReferralManager::Instance()->getActiveReferral()->getPatient()->getID()));
	uiReferral.patientFirstName->setText(ReferralManager::Instance()->getActiveReferral()->getPatient()->getFirstName());
	uiReferral.patientLastName->setText(ReferralManager::Instance()->getActiveReferral()->getPatient()->getLastName());
	//uiReferral.dobDate->setDate(ReferralManager::Instance()->getActiveReferral()->getPatient()->getDOB());
	uiReferral.genderComboBox->setCurrentText(ReferralManager::Instance()->getActiveReferral()->getPatient()->getGender());
	uiReferral.ageSpinbox->setValue(ReferralManager::Instance()->getActiveReferral()->getPatient()->getAge());


	//uiReferral.patientHeight->setText(QString::number(ReferralManager::Instance()->getActiveReferral()->getPatient()->getHeight()));
	//uiReferral.patientWeight->setText(QString::number(ReferralManager::Instance()->getActiveReferral()->getPatient()->getWeight()));

	uiReferral.postcode->setText(ReferralManager::Instance()->getActiveReferral()->getPatient()->getAddress()->getPostcode());
	uiReferral.streetText->setText(ReferralManager::Instance()->getActiveReferral()->getPatient()->getAddress()->getStreet());
	uiReferral.cityText->setText(ReferralManager::Instance()->getActiveReferral()->getPatient()->getAddress()->getCity());

	uiReferral.phoneNumberTex->setText(ReferralManager::Instance()->getActiveReferral()->getPatient()->getNumber());
	uiReferral.emailText->setText(ReferralManager::Instance()->getActiveReferral()->getPatient()->getEmail());

	
	uiReferral.gpIDEdit->setText(QString::number(ReferralManager::Instance()->getActiveReferral()->getGP()->getID()));
	uiReferral.gpFirstName->setText(ReferralManager::Instance()->getActiveReferral()->getGP()->getLastName());
	uiReferral.gpLastName->setText(ReferralManager::Instance()->getActiveReferral()->getGP()->getFirstName());

}

void ReferralState::evaluateEmergency()
{


	double emergency = FuzzyManager::Instance()->evaluateReferralEmergency(ReferralManager::Instance()->getActiveReferral());

	uiReferral.referralEmergencyResultLabel->setText(QString::number(emergency));


	// Switch to final widget 
	//uiReferral.stackedWidget->setCurrentIndex(3);

}

void ReferralState::confirmReferral()
{

	//ReferralManager::Instance()->getActiveReferral()->setID(uiReferral.referralID->text().toUInt());
	ReferralManager::Instance()->getActiveReferral()->setStatus("SENT");

	ReferralManager::Instance()->getActiveReferral()->getPatient()->setName("", uiReferral.patientFirstName->text(), uiReferral.patientLastName->text());
	//ReferralManager::Instance()->getActiveReferral()->getPatient()->setDOB(uiReferral.dobDate->date());
	ReferralManager::Instance()->getActiveReferral()->getPatient()->setGender(uiReferral.genderComboBox->currentText());
	//ReferralManager::Instance()->getActiveReferral()->getPatient()->setWeight(uiReferral.patientWeight->text().toFloat());
	//ReferralManager::Instance()->getActiveReferral()->getPatient()->setHeight(uiReferral.patientHeight->text().toFloat());

	std::shared_ptr<Address> tempAddress = std::make_shared<Address>(Address(0 ,uiReferral.streetText->text(), uiReferral.cityText->text(), uiReferral.postcode->text()));
	ReferralManager::Instance()->getActiveReferral()->getPatient()->setAddress(tempAddress);
	ReferralManager::Instance()->getActiveReferral()->getPatient()->setContactDetails(uiReferral.phoneNumberTex->text(), uiReferral.emailText->text());


	ReferralManager::Instance()->getActiveReferral()->getSpecialist()->setName("", uiReferral.specialistFirstName_2->text(), uiReferral.specialistLastName_2->text());

	// SET ADDRESS OF SPECIALIST !
	//ReferralManager::Instance()->getActiveReferral()->getSpecialist()->setad(uiReferral.phoneNumberTex->text(), uiReferral.emailText->text());

	//ReferralManager::Instance()->getActiveReferral()->getSpecialist()->setSpeciality(uiReferral.comboBox_2->currentText());


	// Insert new referral query 
	QSqlQuery updateReferralQuery;

	updateReferralQuery.prepare("UPDATE referrals SET status = :stat, referral_reason = :reason, referral_date = :date WHERE id = :idParam");
	updateReferralQuery.bindValue(":idParam", ReferralManager::Instance()->getActiveReferral()->getID());
	updateReferralQuery.bindValue(":stat", ReferralManager::Instance()->getActiveReferral()->getStatus());

	updateReferralQuery.bindValue(":reason", ReferralManager::Instance()->getActiveReferral()->getReferralReasonText());
	updateReferralQuery.bindValue(":date", ReferralManager::Instance()->getActiveReferral()->getDate());


	if (!updateReferralQuery.exec()) qDebug() << updateReferralQuery.lastError().text();


	uiReferral.stackedWidget->setCurrentIndex(2);
	// Switch to final widget 
	//uiReferral.stackedWidget->setCurrentIndex(3);

	//StateManager::StateManagerInstance()->changeState(new MainScreenState());

	StateManager::StateManagerInstance()->changeState(new MainScreenState());



}


void ReferralState::saveReferral()
{

	ReferralManager::Instance()->getActiveReferral()->setStatus("SAVED");

	ReferralManager::Instance()->getActiveReferral()->getPatient()->setName("", uiReferral.patientFirstName->text(), uiReferral.patientLastName->text());
//	ReferralManager::Instance()->getActiveReferral()->getPatient()->setDOB(uiReferral.dobDate->date());
	ReferralManager::Instance()->getActiveReferral()->getPatient()->setGender(uiReferral.genderComboBox->currentText());
	//ReferralManager::Instance()->getActiveReferral()->getPatient()->setWeight(uiReferral.patientWeight->text().toFloat());
//	ReferralManager::Instance()->getActiveReferral()->getPatient()->setHeight(uiReferral.patientHeight->text().toFloat());
	ReferralManager::Instance()->getActiveReferral()->getPatient()->setAge(uiReferral.ageSpinbox->value());

	std::shared_ptr<Address> tempAddress = std::make_shared<Address>(Address(0 ,uiReferral.streetText->text(), uiReferral.cityText->text(), uiReferral.postcode->text()));
	ReferralManager::Instance()->getActiveReferral()->getPatient()->setAddress(tempAddress);
	ReferralManager::Instance()->getActiveReferral()->getPatient()->setContactDetails(uiReferral.phoneNumberTex->text(), uiReferral.emailText->text());


	ReferralManager::Instance()->getActiveReferral()->getSpecialist()->setName("", uiReferral.specialistFirstName_2->text(), uiReferral.specialistLastName_2->text());

	// SET ADDRESS OF SPECIALIST !
	//ReferralManager::Instance()->getActiveReferral()->getSpecialist()->setad(uiReferral.phoneNumberTex->text(), uiReferral.emailText->text());

	//ReferralManager::Instance()->getActiveReferral()->getSpecialist()->setSpeciality(uiReferral.comboBox_2->currentText());


	// Insert new referral query 
	QSqlQuery updateReferralQuery;
	QSqlQuery updatePatientQuery;
	QSqlQuery updateSpecialistQuery;

	updateReferralQuery.prepare("UPDATE referrals SET status = :stat, referral_reason = :reason, referral_date = :date WHERE id = :idParam");
	updateReferralQuery.bindValue(":idParam", ReferralManager::Instance()->getActiveReferral()->getID());
	updateReferralQuery.bindValue(":stat", ReferralManager::Instance()->getActiveReferral()->getStatus());

	updateReferralQuery.bindValue(":reason", ReferralManager::Instance()->getActiveReferral()->getReferralReasonText());
	updateReferralQuery.bindValue(":date", ReferralManager::Instance()->getActiveReferral()->getDate());


	if (!updateReferralQuery.exec()) qDebug() << updateReferralQuery.lastError().text();


	updatePatientQuery.prepare("UPDATE patients SET first_name = :fn, last_name = :ln, age = :pAge, gender = :gend, height = :h, weight = :w, address = :adr, city = :pCity, post_code = :pc,  phone_number = :pn, email = :em WHERE id = :idParam");
	updatePatientQuery.bindValue(":idParam", ReferralManager::Instance()->getActiveReferral()->getPatient()->getID());
	updatePatientQuery.bindValue(":fn", ReferralManager::Instance()->getActiveReferral()->getPatient()->getFirstName());

	updatePatientQuery.bindValue(":ln", ReferralManager::Instance()->getActiveReferral()->getPatient()->getLastName());
	updatePatientQuery.bindValue(":pAge", ReferralManager::Instance()->getActiveReferral()->getPatient()->getAge());
	updatePatientQuery.bindValue(":gend", ReferralManager::Instance()->getActiveReferral()->getPatient()->getGender());
	updatePatientQuery.bindValue(":h", ReferralManager::Instance()->getActiveReferral()->getPatient()->getHeight());
	updatePatientQuery.bindValue(":w", ReferralManager::Instance()->getActiveReferral()->getPatient()->getWeight());
	updatePatientQuery.bindValue(":adr", ReferralManager::Instance()->getActiveReferral()->getPatient()->getAddress()->getStreet());
	updatePatientQuery.bindValue(":pCity", ReferralManager::Instance()->getActiveReferral()->getPatient()->getAddress()->getCity());
	updatePatientQuery.bindValue(":pc", ReferralManager::Instance()->getActiveReferral()->getPatient()->getAddress()->getPostcode());
	updatePatientQuery.bindValue(":pn", ReferralManager::Instance()->getActiveReferral()->getPatient()->getNumber());
	updatePatientQuery.bindValue(":em", ReferralManager::Instance()->getActiveReferral()->getPatient()->getEmail());

	if (!updatePatientQuery.exec()) qDebug() << updatePatientQuery.lastError().text();


	updateSpecialistQuery.prepare("UPDATE specialists SET first_name = :fn, last_name = :ln, speciality = :spec WHERE id = :idParam");
	updateSpecialistQuery.bindValue(":idParam", ReferralManager::Instance()->getActiveReferral()->getSpecialist()->getID());
	updateSpecialistQuery.bindValue(":fn", ReferralManager::Instance()->getActiveReferral()->getSpecialist()->getFirstName());
	updateSpecialistQuery.bindValue(":ln", ReferralManager::Instance()->getActiveReferral()->getSpecialist()->getLastName());
	updateSpecialistQuery.bindValue(":spec", ReferralManager::Instance()->getActiveReferral()->getSpecialist()->getSpeciality());


	if (!updateSpecialistQuery.exec()) qDebug() << updateSpecialistQuery.lastError().text();

}


void ReferralState::submitReferral()
{


	std::shared_ptr<Referral> pTemp = ReferralManager::Instance()->getActiveReferral();
	
	if (ReferralManager::Instance()->getActiveReferral()->getStatus() == "NEW" || 
		ReferralManager::Instance()->getActiveReferral()->getStatus() == "SAVED") {


		uiReferral.stackedWidget->setCurrentIndex(2);

	}

	//std::shared_ptr<Patient> tempPatient = std::make_shared<Patient>();
	

	//ReferralManager::Instance()->getActiveReferral()->;



	// Switch to final widget 

	//uiReferral.stackedWidget->setCurrentIndex(3);



}


void ReferralState::newReferral()
{
	loadNewReferral();

	loadActiveReferral();

	//uiReferral.referralID->setText(QString::number(ReferralManager::Instance()->getActiveReferral()->getID()));
	//uiReferral.referralDate->setDate(ReferralManager::Instance()->getActiveReferral()->getDate());

	//uiReferral.patientIdTextEdit->setText(QString::number(ReferralManager::Instance()->getActiveReferral()->getPatient()->getID()));
	

	uiReferral.stackedWidget->setCurrentIndex(1);

	// Switch to final widget 
	//uiReferral.stackedWidget->setCurrentIndex(3);



}

void ReferralState::loadExistingPatient(const QModelIndex& index)
{
	QSqlRecord record = patientSelectQueryModel->record(index.row());

	QSqlQuery selectPatientQuery;
	QSqlQuery selectLifestyleQuery;
	QSqlQuery selectHealthDataQuery;

	QSqlQuery insertNewReferralQuery;
	QSqlQuery selectSpecialistQueryID;

	unsigned int tempPatientID = record.value("id").toInt();

	// Select patient matching id column of indexed row 
	selectPatientQuery.prepare("SELECT * FROM patients WHERE id='" + QString::number(tempPatientID) + "'");

	if (!selectPatientQuery.exec())  qDebug() << selectPatientQuery.lastError().text();

	selectPatientQuery.first();

	QString patientFirstName = selectPatientQuery.value(1).toString();
	QString patientLastName = selectPatientQuery.value(2).toString();
	unsigned short int uiAgeTemp = selectPatientQuery.value(3).toInt();
	QString gender = selectPatientQuery.value(4).toString();
	QDate dob = selectPatientQuery.value(5).toDate();


	QString patientAddress = selectPatientQuery.value(8).toString();
	QString patientCity = selectPatientQuery.value(9).toString();
	QString patientPostCode = selectPatientQuery.value(10).toString();

	std::shared_ptr<Address> tempAddress = std::make_shared<Address>();
	tempAddress->setNewAddress(patientAddress, patientCity, patientPostCode);


	QString phoneNumber = selectPatientQuery.value(11).toString();
	QString email = selectPatientQuery.value(12).toString();

	std::shared_ptr<Patient> tempPatient = std::make_shared<Patient>(Patient(tempPatientID, patientFirstName, patientLastName, uiAgeTemp));
	tempPatient->setAddress(tempAddress);
	tempPatient->setAge(uiAgeTemp);
	tempPatient->setGender(gender);
	tempPatient->setContactDetails(phoneNumber, email);



	selectLifestyleQuery.prepare("SELECT smoker, drinker, exercise FROM lifestyle WHERE patients_id='" + QString::number(tempPatientID) + "'");

	if (!selectLifestyleQuery.exec())  qDebug() << selectLifestyleQuery.lastError().text();

	int count = 0;

	while (selectLifestyleQuery.next())
	{
		count++;
	}


	if (count < 1) {

		QSqlQuery insertNewMedicalData;
		QSqlQuery insertNewLifestyleData;

		if (insertNewMedicalData.prepare("INSERT INTO health_data (patients_id, blood_pressure, cholesterol) VALUES (:id, :bp, :chol)")) qDebug() << insertNewMedicalData.lastError().text();
		insertNewMedicalData.bindValue(":id", tempPatient->getID());
		insertNewMedicalData.bindValue(":bp", 60);
		insertNewMedicalData.bindValue(":chol", 90);


		if (!insertNewMedicalData.exec()) qDebug() << insertNewMedicalData.lastError().text();

		if (insertNewLifestyleData.prepare("INSERT INTO lifestyle (patients_id, smoker, exercise, drinker) VALUES (:id, :smk, :exer, :drink)")) qDebug() << insertNewLifestyleData.lastError().text();
		insertNewLifestyleData.bindValue(":id", tempPatient->getID());
		insertNewLifestyleData.bindValue(":smk", "NONE");
		insertNewLifestyleData.bindValue(":exer", "NONE");
		insertNewLifestyleData.bindValue(":drink", "NONE");


		if (!insertNewLifestyleData.exec()) qDebug() << insertNewLifestyleData.lastError().text();

	}






	std::shared_ptr<Lifestyle> patientLifestyle = std::make_shared<Lifestyle>(Lifestyle(selectLifestyleQuery.value(0).toString(), selectLifestyleQuery.value(1).toString(), selectLifestyleQuery.value(2).toString()));


	selectHealthDataQuery.prepare("SELECT blood_pressure, cholesterol FROM health_data WHERE patients_id='" + QString::number(tempPatientID) + "'");

	if (!selectHealthDataQuery.exec())  qDebug() << selectHealthDataQuery.lastError().text();

	selectHealthDataQuery.first();

	std::shared_ptr<MedicalData> medicalData = std::make_shared<MedicalData>(MedicalData(selectLifestyleQuery.value(0).toUInt(), selectLifestyleQuery.value(1).toUInt()));
	tempPatient->setLifestyle(patientLifestyle);
	tempPatient->setMedicalData(medicalData);


	//if (!selectSpecialistQueryID.exec("INSERT INTO specialists (title, first_name, last_name, speciality) VALUES ('','','','')")) qDebug() << selectSpecialistQueryID.lastError().text();

	//unsigned int tempSpecialistID;

	//if (!selectSpecialistQueryID.exec("SELECT LAST_INSERT_ID()")) qDebug() << selectSpecialistQueryID.lastError().text();
	//selectSpecialistQueryID.first();

	//tempSpecialistID = selectSpecialistQueryID.value(0).toInt();

	//std::shared_ptr<Specialist> tempSpecialist = std::make_shared<Specialist>(Specialist(tempSpecialistID, "", "", ""));

	//insertNewReferralQuery.prepare("INSERT INTO referrals (patient_id, gps_id, specialists_id) VALUES (:p_id, :gp_id, :spec_id)");
	insertNewReferralQuery.prepare("INSERT INTO referrals (patient_id, gps_id) VALUES (:p_id, :gp_id)");

	insertNewReferralQuery.bindValue(":p_id", tempPatient->getID());
	insertNewReferralQuery.bindValue(":gp_id", UserManager::Instance()->getActiveGP()->getID());
	//insertNewReferralQuery.bindValue(":spec_id", tempSpecialist->getID());

	if (!insertNewReferralQuery.exec()) qDebug() << insertNewReferralQuery.lastError().text();


	unsigned int tempReferralID;

	if (!insertNewReferralQuery.exec("SELECT LAST_INSERT_ID()")) qDebug() << insertNewReferralQuery.lastError().text();
	insertNewReferralQuery.last();

	tempReferralID = insertNewReferralQuery.value(0).toInt();


//	QDate date = date.currentDate();

	std::shared_ptr<Referral> pTempReferral = std::make_shared<Referral>(Referral(tempReferralID, QDate::currentDate()));
	//pTempReferral->addSpecialist(tempSpecialist);
	pTempReferral->addPatient(tempPatient);
	pTempReferral->addGeneralPractitioner(UserManager::Instance()->getActiveGP());


	ReferralManager::Instance()->setActiveReferral(pTempReferral);

	uiReferral.stackedWidget->setCurrentIndex(1);

	loadActiveReferral();

}

void ReferralState::back()
{
	StateManager::StateManagerInstance()->changeState(new ViewReferralsState());
}

void ReferralState::addSymptom()
{

	std::shared_ptr<Symptom> emptySymptom = std::make_shared<Symptom>();

	StateManager::StateManagerInstance()->showDialog(new SymptomDialog(emptySymptom, true));
}

void ReferralState::refreshLists()
{
	updateLists();
}

void ReferralState::viewSymptom(const QModelIndex& index)
{

	QSqlRecord record = symptomsQueryModel->record(index.row());

	unsigned int tempID = record.value("id").toUInt();
	QString tempName = record.value("name").toString();

	// Pass symptom with ID to the dialog so that a query can be made to fetch the symptom data 
	std::shared_ptr<Symptom> symptomToDisplay = std::make_shared<Symptom>(Symptom(tempID, tempName, "", 0, 0));

	StateManager::StateManagerInstance()->showDialog(new SymptomDialog(symptomToDisplay, false));

}

void ReferralState::viewCondition(const QModelIndex& index)
{

}

void ReferralState::viewTreatment(const QModelIndex& index)
{

}

void ReferralState::viewHealthData()
{
	StateManager::StateManagerInstance()->showDialog(new HealthDataDialog());


}

void ReferralState::viewLifestyleData()
{
	StateManager::StateManagerInstance()->showDialog(new LifestyleDialog());

}

void ReferralState::updateLists()
{
	symptomsQueryModel = new QSqlQueryModel();

	QSqlQuery query;

	query.prepare("SELECT id, name FROM symptoms WHERE patients_id='" + QString::number(ReferralManager::Instance()->getActiveReferral()->getPatient()->getID()) + "'");

	if (!query.exec()) qDebug() << query.lastError().text();;

	symptomsQueryModel->setQuery(query);

	uiReferral.symptomsTableView->setModel(symptomsQueryModel);
}

void ReferralState::chooseSpecialist()
{
	StateManager::StateManagerInstance()->showDialog(new ViewSpecialistsDialog());
}


void ReferralState::refreshSpecialist()
{

	QSqlQuery selectSpecialistQuery;

	// Select specialist matching id of foreign key in referral 
	selectSpecialistQuery.prepare("SELECT * FROM specialists WHERE id='" + QString::number(ReferralManager::Instance()->getActiveReferral()->getSpecialist()->getID() ) + "'");

	if (!selectSpecialistQuery.exec())  qDebug() << selectSpecialistQuery.lastError().text();

	selectSpecialistQuery.first();

	unsigned int tempSpecialistId = selectSpecialistQuery.value(0).toInt();
	QString specialistUser = selectSpecialistQuery.value(1).toString();
	QString specialistPass = selectSpecialistQuery.value(2).toString();
	QString specialistTitle = selectSpecialistQuery.value(3).toString();
	QString specialistFirstName = selectSpecialistQuery.value(4).toString();
	QString specialistLastName = selectSpecialistQuery.value(5).toString();
	QString speciality = selectSpecialistQuery.value(6).toString();

	std::shared_ptr<Specialist> tempSpecialist = std::make_shared<Specialist>(Specialist(tempSpecialistId, specialistFirstName, specialistLastName, speciality));


	uiReferral.specialistIDEdit->setText(QString::number(tempSpecialist->getID()));
	uiReferral.specialistFirstName_2->setText(tempSpecialist->getFirstName());
	uiReferral.specialistLastName_2->setText(tempSpecialist->getLastName());
	uiReferral.comboBox_2->setCurrentText(tempSpecialist->getSpeciality());

}