#include "Treatment.h"


Treatment::Treatment(std::string sNameIn, double fIntensityIn, double fEffectivenessIn)
{
	sName = sNameIn;
	fIntensity = fIntensityIn;
	fEffectiveness = fEffectivenessIn;
}


Treatment::~Treatment()
{

}

void Treatment::setDescription()
{

}

void Treatment::setIntensity(double fIntensityIn)
{
	fIntensity = fIntensityIn;
}

void Treatment::setEffectiveness(double fEffectivenessIn)
{
	fEffectiveness = fEffectivenessIn;
}

double Treatment::getIntensity()
{
	return fIntensity;
}

double Treatment::getEffectiveness()
{
	return fEffectiveness;
}