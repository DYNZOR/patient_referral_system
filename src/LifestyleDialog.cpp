#include "LifestyleDialog.h"


LifestyleDialog::LifestyleDialog(QWidget* parent) : QWidget(parent)
{


	setupUserInterface();
	setupSignals();

	// Perform update query 
	QSqlQuery selectLifestyle;

	selectLifestyle.prepare("SELECT smoker, drinker, exercise FROM lifestyle WHERE patients_id = :idParam");
	selectLifestyle.bindValue(":idParam", ReferralManager::Instance()->getActiveReferral()->getPatient()->getID());

	if (!selectLifestyle.exec()) qDebug() << selectLifestyle.lastError().text();

	selectLifestyle.first();

	QString smoker = selectLifestyle.value(0).toString();
	QString drinker = selectLifestyle.value(1).toString();
	QString exercise = selectLifestyle.value(2).toString();

	uiDialog.drinkerCombo->setCurrentText(drinker);
	uiDialog.smokerCombo->setCurrentText(smoker);
	uiDialog.exerciseCombo->setCurrentText(exercise);

}

void LifestyleDialog::setupUserInterface()
{
	uiDialog.setupUi(this);

}

void LifestyleDialog::setupSignals()
{
	connect(uiDialog.updateLifestyle, SIGNAL(clicked()), this, SLOT(updateLifestyle()));
	connect(uiDialog.closeLifestyleBtn, SIGNAL(clicked()), this, SLOT(closeLifestyleDialog()));
}

void LifestyleDialog::updateLifestyle() //!< Updates patients health data 
{

	// Store data currently in the forms
	std::shared_ptr<Lifestyle> newLifestyle = std::make_shared<Lifestyle>(Lifestyle(uiDialog.drinkerCombo->currentText(), uiDialog.smokerCombo->currentText(), uiDialog.exerciseCombo->currentText()));

	// Perform update query 
	QSqlQuery updateLifestyle;

	updateLifestyle.prepare("UPDATE lifestyle SET smoker = :smk, drinker = :drnk, exercise = :exrc WHERE patients_id = :idParam");
	updateLifestyle.bindValue(":idParam", ReferralManager::Instance()->getActiveReferral()->getPatient()->getID());
	updateLifestyle.bindValue(":smk", newLifestyle->getSmokerStatus());
	updateLifestyle.bindValue(":drnk", newLifestyle->getDrinkerStatus());
	updateLifestyle.bindValue(":exrc", newLifestyle->getExerciseStatus());


	if (!updateLifestyle.exec()) qDebug() << updateLifestyle.lastError().text();


	ReferralManager::Instance()->getActiveReferral()->getPatient()->setLifestyle(newLifestyle);
}

void LifestyleDialog::closeLifestyleDialog()
{
	StateManager::StateManagerInstance()->closeDialog();
}

