#include "StateManager.h"
#include "LoginState.h"
#include "Connection.h"
#include <QtWidgets/QApplication>
#include <QDebug>
#include <fl/Headers.h>


#include "FuzzyManager.h"


using namespace fl;

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);


	//Connection dbConnection = Connection();
	Connection::DatabaseConnection();


	StateManager* manager = StateManager::StateManagerInstance();

	

	//std::shared_ptr<Symptom> symptomTest = std::make_shared<Symptom>("Test", "Legs", 6.0, 14.0);



	//FuzzyManager* fuzzyManager = FuzzyManager::Instance();

	//double testSeverity = fuzzyManager->getSymptomSeverity(symptomTest);


	//FL_LOG("Ambient.input = " << Op::str(testSeverity));


	//manager->changeState(LoginState::Instance());
	manager->changeState(new LoginState());


	return a.exec();
}
