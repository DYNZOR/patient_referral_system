#include "MedicalData.h"

MedicalData::MedicalData()
{

}

MedicalData::MedicalData(unsigned int bpIn, unsigned int cholIn)
{
	uiBloodPressure = bpIn;
	uiCholesterolLDL = cholIn;
}

unsigned int MedicalData::getCholesterol()
{
	return uiCholesterolLDL;
}

unsigned int MedicalData::getBloodPressure()
{
	return uiBloodPressure;
}