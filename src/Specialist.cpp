#include "Specialist.h"

Specialist::Specialist()
{

}

Specialist::Specialist(unsigned int uiSpecialistIDin, QString sFirstNameIn, QString sLastNameIn, QString specialityIn)
{
	uiSpecialistID = uiSpecialistIDin;
	sFirstName = sFirstNameIn;
	sLastName = sLastNameIn;
	speciality = specialityIn;
}


Specialist::~Specialist()
{

}

void Specialist::setName(QString titleIn, QString firstNameIn, QString lastNameIn)
{
	sDoctorTitle = titleIn;
	sFirstName = firstNameIn;
	sLastName = lastNameIn;
}

//void Specialist::setAddress(std::shared_ptr<Address> addressIn)
//{
//
//}

void Specialist::setContactDetails(QString numberIn, QString emailIn)
{

	specialistEmail = emailIn;
}

unsigned int Specialist::getID()
{
	return uiSpecialistID;
}

QString Specialist::getFirstName()
{
	return sFirstName;
}

QString Specialist::getLastName()
{
	return sLastName;
}

QString Specialist::getEmail()
{
	return specialistEmail;
}

QString Specialist::getSpeciality()
{
	return speciality;
}
