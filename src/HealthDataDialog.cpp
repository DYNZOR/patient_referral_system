#include "HealthDataDialog.h"


HealthDataDialog::HealthDataDialog( QWidget* parent) : QWidget(parent)
{

	setupUserInterface();
	setupSignals();

	// Perform update query 
	QSqlQuery selectHealthData;


	selectHealthData.prepare("SELECT blood_pressure, cholesterol FROM health_data WHERE patients_id = :idParam");
	selectHealthData.bindValue(":idParam", ReferralManager::Instance()->getActiveReferral()->getPatient()->getID());

	if (!selectHealthData.exec()) qDebug() << selectHealthData.lastError().text();

	selectHealthData.first();

	unsigned int bp = selectHealthData.value(0).toUInt();
	unsigned int chol = selectHealthData.value(1).toUInt();

	uiDialog.bloodPressureSpinbox->setValue(bp);
	uiDialog.cholesterolSpinBox->setValue(chol);


}

void HealthDataDialog::setupUserInterface()
{
	uiDialog.setupUi(this);

}

void HealthDataDialog::setupSignals()
{
	connect(uiDialog.updateHealthDatabtn, SIGNAL(clicked()), this, SLOT(updateHealthData()));
	connect(uiDialog.closeHealthData, SIGNAL(clicked()), this, SLOT(closeHealthDataDialog()));

}

void HealthDataDialog::updateHealthData()
{

	// Store data currently in the forms
	std::shared_ptr<MedicalData> newMedicalData = std::make_shared<MedicalData>(MedicalData(uiDialog.bloodPressureSpinbox->value(), uiDialog.cholesterolSpinBox->value()));

	// Perform update query 
	QSqlQuery updateHealthData;

	updateHealthData.prepare("UPDATE health_data SET blood_pressure = :bp, cholesterol = :chol WHERE patients_id = :idParam");
	updateHealthData.bindValue(":idParam", ReferralManager::Instance()->getActiveReferral()->getPatient()->getID());
	updateHealthData.bindValue(":bp", newMedicalData->getBloodPressure());
	updateHealthData.bindValue(":chol", newMedicalData->getCholesterol());


	if (!updateHealthData.exec()) qDebug() << updateHealthData.lastError().text();


	ReferralManager::Instance()->getActiveReferral()->getPatient()->setMedicalData(newMedicalData);
}

void HealthDataDialog::closeHealthDataDialog()
{
	StateManager::StateManagerInstance()->closeDialog();
}