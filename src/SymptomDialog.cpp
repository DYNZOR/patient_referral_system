#include "SymptomDialog.h"


SymptomDialog::SymptomDialog(std::shared_ptr<Symptom> symptomIn, bool bIsNew, QWidget* parent) : QWidget(parent)
{
	bIsNewSymptom = bIsNew;

	activeSymptom = symptomIn;


	setupUserInterface();
	setupSignals();


	if (bIsNewSymptom)
	{
		uiDialog.addSymptomBtnDialog->setText("Add");
		uiDialog.deleteBtn->setText("Close");
	}
	else {
		uiDialog.addSymptomBtnDialog->setText("Update");
		uiDialog.deleteBtn->setText("Delete");

	}

	loadSymptom();
}

void SymptomDialog::setupUserInterface()
{
	uiDialog.setupUi(this);

}

void SymptomDialog::setupSignals()
{

	if (bIsNewSymptom)
	{
		connect(uiDialog.addSymptomBtnDialog, SIGNAL(clicked()), this, SLOT(addSymptom()));
		connect(uiDialog.deleteBtn, SIGNAL(clicked()), this, SLOT(closeDialog()));
		

	}
	else {
		connect(uiDialog.addSymptomBtnDialog, SIGNAL(clicked()), this, SLOT(updateSymptom()));
		connect(uiDialog.deleteBtn, SIGNAL(clicked()), this, SLOT(removeSymptom()));

	}
}

void SymptomDialog::loadSymptom()
{
	QSqlQuery selectSymptomQuery;
	selectSymptomQuery.prepare("SELECT * FROM symptoms WHERE id='" + QString::number(activeSymptom->getID()) + "'");

	if (!selectSymptomQuery.exec())  qDebug() << selectSymptomQuery.lastError().text();

	selectSymptomQuery.first();

	unsigned int id = selectSymptomQuery.value(0).toUInt();
	QString name = selectSymptomQuery.value(2).toString();
	QString location = selectSymptomQuery.value(4).toString();
	unsigned int discomfort = selectSymptomQuery.value(5).toUInt();
	unsigned int duration = selectSymptomQuery.value(6).toUInt();

	std::shared_ptr<Symptom> symptomToLoad = std::make_shared<Symptom>(Symptom( id, name, location, discomfort, duration));
	activeSymptom = symptomToLoad;

	uiDialog.symptomName->setText(activeSymptom->getName());

	uiDialog.locationComboBox->setCurrentText(activeSymptom->getBodyLocation());
	uiDialog.discomfortSpinBox->setValue(activeSymptom->getDiscomfort());

	uiDialog.durationSpinBox->setValue(activeSymptom->getDurationInDays());
}

void SymptomDialog::addSymptom()
{
	QSqlQuery insertNewSymptomQuery;

	std::shared_ptr<Symptom> newSymptom = std::make_shared<Symptom>(Symptom(0 ,uiDialog.symptomName->text(), uiDialog.locationComboBox->currentText(), uiDialog.discomfortSpinBox->value(), uiDialog.durationSpinBox->value()));

	insertNewSymptomQuery.prepare("INSERT INTO symptoms (patients_id, name, description, location, discomfort, time) VALUES (:p_id, :nm, :dsc, :loc, :discomfort, :time)");
	insertNewSymptomQuery.bindValue(":p_id", ReferralManager::Instance()->getActiveReferral()->getPatient()->getID());
	insertNewSymptomQuery.bindValue(":nm", newSymptom->getName());
	insertNewSymptomQuery.bindValue(":dsc", "");
	insertNewSymptomQuery.bindValue(":loc", newSymptom->getBodyLocation());

	insertNewSymptomQuery.bindValue(":discomfort", newSymptom->getDiscomfort());
	insertNewSymptomQuery.bindValue(":duration", newSymptom->getDurationInDays());

	if (!insertNewSymptomQuery.exec()) {
		qDebug() << insertNewSymptomQuery.lastError().text();
	}
	else {

		unsigned int tempID;

		if (!insertNewSymptomQuery.exec("SELECT LAST_INSERT_ID()")) qDebug() << insertNewSymptomQuery.lastError().text();
		insertNewSymptomQuery.last();

		tempID = insertNewSymptomQuery.value(0).toInt();

		newSymptom->setID(tempID);

		ReferralManager::Instance()->getActiveReferral()->addSymptom(newSymptom);

		StateManager::StateManagerInstance()->closeDialog();



	}

}

void SymptomDialog::removeSymptom()
{
	QSqlQuery deleteSymptomQuery;

	deleteSymptomQuery.prepare("DELETE FROM symptoms WHERE id = :idParam");
	deleteSymptomQuery.bindValue(":idParam", activeSymptom->getID());

	if (!deleteSymptomQuery.exec()) qDebug() << deleteSymptomQuery.lastError().text();

	StateManager::StateManagerInstance()->closeDialog();

}

void SymptomDialog::updateSymptom()
{
	QSqlQuery updateSymptomQuery;

	std::shared_ptr<Symptom> updatedSymptom = std::make_shared<Symptom>(Symptom(activeSymptom->getID(), uiDialog.symptomName->text(), uiDialog.locationComboBox->currentText(), uiDialog.discomfortSpinBox->value(), uiDialog.durationSpinBox->value()));

	updateSymptomQuery.prepare("UPDATE symptoms SET name = :nm, description = :dsc, location = :loc, discomfort = :dis, time = :dur WHERE id = :idParam");
	updateSymptomQuery.bindValue(":idParam", updatedSymptom->getID());
	updateSymptomQuery.bindValue(":nm", updatedSymptom->getName());
	updateSymptomQuery.bindValue(":dsc", "");
	updateSymptomQuery.bindValue(":loc", updatedSymptom->getBodyLocation());
	updateSymptomQuery.bindValue(":dis", updatedSymptom->getDiscomfort());
	updateSymptomQuery.bindValue(":dur", updatedSymptom->getDurationInDays());

	if (!updateSymptomQuery.exec()) qDebug() << updateSymptomQuery.lastError().text();


	ReferralManager::Instance()->getActiveReferral()->addSymptom(updatedSymptom);



	StateManager::StateManagerInstance()->closeDialog();

}

void SymptomDialog::closeDialog()
{
	StateManager::StateManagerInstance()->closeDialog();


	//uiDialog.loginPage->hide();
}


