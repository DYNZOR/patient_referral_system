#include "Patient.h"

Patient::Patient()
{

}

Patient::Patient(unsigned int uiPatientIdIn, QString sFirstNameIn, QString sLastNameIn, unsigned int ageIn)
{


	uiAge = ageIn;

	uiPatientId = uiPatientIdIn;
	sFirstName = sFirstNameIn;
	sLastName = sLastNameIn;
}


Patient::~Patient()
{

}

void Patient::setName(QString titleIn, QString firstNameIn, QString lastNameIn)
{
	sTitle = titleIn;
	sFirstName = firstNameIn;
	sLastName = lastNameIn;
}

void Patient::setAge(unsigned short int ageIn)
{
	uiAge = ageIn;
}

void Patient::setGender(QString genderIn)
{
	gender = genderIn;
}

void Patient::setHeight(float uiHeightIn)
{
	uiHeight = uiHeight;
}

void Patient::setWeight(float uiWeightIn)
{
	uiWeight = uiWeightIn;
}

void Patient::setAddress(std::shared_ptr<Address> addressIn)
{
	pCurrentPatientAdress = addressIn;
}

void Patient::setLifestyle(std::shared_ptr<Lifestyle> pLifestyleIn)
{
	pLifestyleData = pLifestyleIn;
}

void Patient::setMedicalData(std::shared_ptr<MedicalData> pMedicalDataIn)
{
	pMedicalData = pMedicalDataIn;
}

void Patient::setDOB(QDate dateIn)
{
	dateOfBirth = dateIn;
}

void Patient::setContactDetails(QString numberIn, QString emailIn)
{
	patientNumber = numberIn;
	patientEmail = emailIn;
}

std::shared_ptr<Address> Patient::getAddress() //!< Returns patient address 
{
	return pCurrentPatientAdress;
}

unsigned int Patient::getID()
{
	return uiPatientId;
}

QString Patient::getFirstName()
{
	return sFirstName;
}

QString Patient::getLastName()
{
	return sLastName;
}

QString Patient::getTitle()
{
	return sTitle;
}

QString Patient::getGender()
{
	return gender;
}

QString Patient::getNumber()
{
	return patientNumber;
}

QString Patient::getEmail()
{
	return patientEmail;
}

QDate Patient::getDOB()
{
	return dateOfBirth;
}

unsigned int Patient::getAge()
{
	return uiAge;
}

float Patient::getWeight()
{
	return uiWeight;
}

float Patient::getHeight()
{
	return uiHeight;
}

std::shared_ptr<Lifestyle> Patient::getLifestyle()
{
	return pLifestyleData;
}

std::shared_ptr<MedicalData> Patient::getMedicalData()
{
	return pMedicalData;
}