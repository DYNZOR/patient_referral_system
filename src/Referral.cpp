#include "Referral.h"
#include "FuzzyManager.h"

Referral::Referral()
{
	uiReferralID = 0;
}

Referral::Referral(unsigned int uiReferralIdIn, QDate dateIn)
{
	uiReferralID = uiReferralIdIn;
	referralDate = dateIn;
}

void Referral::addPatient(std::shared_ptr<Patient> pPatientIn)
{
	pPatient = pPatientIn;
}

void Referral::addGeneralPractitioner(std::shared_ptr<GP> pGPin)
{
	pGP = pGPin;
}

void Referral::addSpecialist(std::shared_ptr<Specialist> pSpecialistIn)
{
	pSpecialist = pSpecialistIn;
}

void Referral::addSymptom(std::shared_ptr<Symptom> pSymptomIn)
{

	std::shared_ptr<Symptom> pTempSymptom = pSymptomIn;

	// Check to see if symptom being added exists in vector already, if so update it
	for (int i = 0; i < symptoms.size(); ++i)
	{
		if (symptoms.at(i)->getID() == pTempSymptom->getID())
		{
			symptoms.at(i) = pTempSymptom;
		}
	}

	// Push back as normal if symptom is new 
	symptoms.push_back(pSymptomIn);
}

void Referral::addTreatment(std::shared_ptr<Treatment> pTreatmentIn)
{
	treatments.push_back(pTreatmentIn);
}

void Referral::addCondition(std::shared_ptr<Condition> pConditionIn)
{
	conditions.push_back(pConditionIn);
}

double Referral::getOverallSymptomSeverity()
{
	double totalSeverity = 0;

	for (int i = 0; i < symptoms.size(); ++i)
	{
		double severity = FuzzyManager::Instance()->getSymptomSeverity(symptoms.at(i));
		symptoms.at(i)->setSeverity(severity);

		totalSeverity += severity;
	}

	return totalSeverity;
}

void Referral::setDate(QDate dateIn)
{
	referralDate = dateIn;
}

QDate Referral::getDate()
{
	return referralDate;
}

unsigned int Referral::getID()
{
	return uiReferralID;
}

std::shared_ptr<Patient> Referral::getPatient()
{
	return pPatient;
}

std::shared_ptr<GP> Referral::getGP()
{
	return pGP;
}

std::shared_ptr<Specialist> Referral::getSpecialist()
{
	return pSpecialist;
}

QString Referral::getStatus()
{
	return sStatus;
}

QString Referral::getReferralReasonText()
{
	return referralReasonText;
}

void Referral::setStatus(QString statusIn)
{
	sStatus = statusIn;
}

void Referral::setReferralReason(QString reasonIn)
{
	referralReasonText = reasonIn;
}