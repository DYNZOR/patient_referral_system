#include "ViewSpecialistsDialog.h"


ViewSpecialistsDialog::ViewSpecialistsDialog(QWidget* parent) : QWidget(parent)
{


	setupUserInterface();
	setupSignals();


	specialistsQueryModel = new QSqlQueryModel();

	QSqlQuery query;

	query.prepare("SELECT id, title, first_name, last_name, speciality FROM specialists");

	query.exec();

	specialistsQueryModel->setQuery(query);

	uiDialog.specialistsTableView->setModel(specialistsQueryModel);



	//// Perform update query 
	//QSqlQuery selectLifestyle;

	//selectLifestyle.prepare("SELECT smoker, drinker, exercise FROM lifestyle WHERE patients_id = :idParam");
	//selectLifestyle.bindValue(":idParam", ReferralManager::Instance()->getActiveReferral()->getPatient()->getID());

	//if (!selectLifestyle.exec()) qDebug() << selectLifestyle.lastError().text();

	//selectLifestyle.first();

	//QString smoker = selectLifestyle.value(0).toString();
	//QString drinker = selectLifestyle.value(1).toString();
	//QString exercise = selectLifestyle.value(2).toString();

	//uiDialog.drinkerCombo->setCurrentText(drinker);
	//uiDialog.smokerCombo->setCurrentText(smoker);
	//uiDialog.exerciseCombo->setCurrentText(exercise);

}

void ViewSpecialistsDialog::setupUserInterface()
{
	uiDialog.setupUi(this);

}

void ViewSpecialistsDialog::setupSignals()
{
	connect(uiDialog.specialistsTableView, SIGNAL(clicked(QModelIndex)), this, SLOT(selectSpecialist(const QModelIndex&)));
}

void ViewSpecialistsDialog::selectSpecialist(const QModelIndex& index) //!< Updates patients health data 
{

	QSqlRecord record = specialistsQueryModel->record(index.row());


	unsigned int tempSpecialistId = record.value("id").toInt();
	QString username = record.value("username").toString();
	QString password = record.value("password").toString();
	QString frstnm = record.value("first_name").toString();
	QString lstnm = record.value("last_name").toString();
	QString speciality = record.value("speciality").toString();

	std::shared_ptr<Specialist> tempSpecialist = std::make_shared<Specialist>(Specialist(tempSpecialistId, frstnm, lstnm, speciality));


	QSqlQuery updateReferralSpecialistID;

	// Select referral matching id column of indexed row 
	updateReferralSpecialistID.prepare("UPDATE referrals SET specialists_id = :spec WHERE id = :idParam");
	updateReferralSpecialistID.bindValue(":idParam", ReferralManager::Instance()->getActiveReferral()->getID());
	updateReferralSpecialistID.bindValue(":spec", tempSpecialist->getID());
	if (!updateReferralSpecialistID.exec())  qDebug() << updateReferralSpecialistID.lastError().text();


	ReferralManager::Instance()->getActiveReferral()->addSpecialist(tempSpecialist);

	StateManager::StateManagerInstance()->closeDialog();

	//ReferralManager::Instance()->getActiveReferral()->getPatient()->setLifestyle(newLifestyle);
}

void ViewSpecialistsDialog::closeDialog()
{
	StateManager::StateManagerInstance()->closeDialog();
}

