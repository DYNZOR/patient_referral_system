#include "Facility.h"

Facility::Facility()
{

}

Facility::Facility(unsigned int idIn, QString nameIn, QString typeIn)
{
	id = idIn;
	name = nameIn;
	type = typeIn;
}

void Facility::setName(QString nameIn)
{
	name = nameIn;
}

QString Facility::getName()
{
	return name;
}

void Facility::setAddress(std::shared_ptr<Address> addressIn)
{
	pAddress = addressIn;
}

std::shared_ptr<Address> Facility::getAddress()
{
	return pAddress;
}

unsigned int Facility::getId()
{
	return id;
}

