#include "ViewReferralsState.h"

//ViewReferralsState* ViewReferralsState::pViewReferralsState;

ViewReferralsState::ViewReferralsState(QWidget* parent) : QWidget(parent)
{
	//setType(VIEW_REFERRALS);

	uiViewReferrals.setupUi(this);

	//uiViewReferrals.tableWidget->hideColumn(0);

	loadReferrals();
	loadPatients();
	loadSpecialists();

	connect(uiViewReferrals.addNewReferralBtn, SIGNAL(clicked()), this, SLOT(newReferral()));

	connect(uiViewReferrals.referralsTableView, SIGNAL(clicked(QModelIndex)), this, SLOT(selectReferral(const QModelIndex&)));
	connect(uiViewReferrals.referralsTableView, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(loadSelectedReferral(const QModelIndex&)));

	connect(uiViewReferrals.mainMenuBtn, SIGNAL(clicked()), this, SLOT(backToMainMenu()));


	//window->connect(uiViewRef.mainMenuBtn, SIGNAL(clicked()), window, SLOT(backToMainMenu()))

	//window->connect(uiViewReferrals.tableView, SIGNAL(activated(QModelIndex &index)), window, SLOT(on_row_activated(QModelIndex &index)));
}

void ViewReferralsState::newReferral()
{
	//StateManager::StateManagerInstance()->changeState(ReferralState::Instance());
	StateManager::StateManagerInstance()->changeState(new ReferralState(false));

}

void ViewReferralsState::selectReferral(const QModelIndex& index)
{
	



	
	//QModelIndexList indexes = uiViewReferrals.referralsTableView->selectionModel()->selectedIndexes();#

	//for (int i = 0; i < indexes.count(); ++i)
	//{
	//	QModelIndex tempIndex = indexes.at(i);

	//	QSqlRecord record = referralsQueryModel->record(tempIndex.row());

	//	unsigned int tempId = record.value("id").toInt();

	//}
}

void ViewReferralsState::loadSelectedReferral(const QModelIndex& index)
{

	QSqlRecord record = referralsQueryModel->record(index.row());

	unsigned int tempReferralID = record.value("id").toInt();

	QSqlQuery selectReferralQuery;
	QSqlQuery selectPatientQuery;
	QSqlQuery selectSpecialistQuery;

	// Select referral matching id column of indexed row 
	selectReferralQuery.prepare("SELECT * FROM referrals WHERE id='" + QString::number(tempReferralID) + "'");

	if (!selectReferralQuery.exec())  qDebug() << selectReferralQuery.lastError().text();

	selectReferralQuery.first();

	unsigned int tempPatientID = selectReferralQuery.value(1).toInt();
	unsigned int tempSpecialistID = selectReferralQuery.value(3).toInt();

	QDate tempDate = selectReferralQuery.value(6).toDate(); // FIX 
	QString tempReason = selectReferralQuery.value(5).toString();
	QString status = selectReferralQuery.value(4).toString();


	// Select patient matching id of foreign key in referral 
	selectPatientQuery.prepare("SELECT * FROM patients WHERE id='" + QString::number(tempPatientID) + "'");

	if (!selectPatientQuery.exec())  qDebug() << selectPatientQuery.lastError().text();

	selectPatientQuery.first();

	QString patientFirstName = selectPatientQuery.value(1).toString();
	QString patientLastName = selectPatientQuery.value(2).toString();
	unsigned short int uiAgeTemp = selectPatientQuery.value(3).toInt();
	QString gender = selectPatientQuery.value(4).toString();
	float heightTemp = selectPatientQuery.value(5).toFloat();
	float weightTemp = selectPatientQuery.value(6).toFloat();


	QString patientAddress = selectPatientQuery.value(7).toString();
	QString patientCity = selectPatientQuery.value(8).toString();
	QString patientPostCode = selectPatientQuery.value(9).toString();

	std::shared_ptr<Address> tempAddress = std::make_shared<Address>();
	tempAddress->setNewAddress(patientAddress, patientCity, patientPostCode);


	QString phoneNumber = selectPatientQuery.value(10).toString();
	QString email = selectPatientQuery.value(11).toString();

	std::shared_ptr<Patient> tempPatient = std::make_shared<Patient>(Patient(tempPatientID, patientFirstName, patientLastName, uiAgeTemp));
	tempPatient->setAddress(tempAddress);
	tempPatient->setAge(uiAgeTemp);

	//tempPatient->setGender()
	tempPatient->setHeight(heightTemp);
	tempPatient->setWeight(weightTemp);
	tempPatient->setContactDetails(phoneNumber, email);



	// Select specialist matching id of foreign key in referral 
	selectSpecialistQuery.prepare("SELECT * FROM specialists WHERE id='" + QString::number(tempSpecialistID) + "'");

	if (!selectSpecialistQuery.exec())  qDebug() << selectSpecialistQuery.lastError().text();

	selectSpecialistQuery.first();

	QString specialistUser = selectSpecialistQuery.value(1).toString();
	QString specialistPass = selectSpecialistQuery.value(2).toString();
	QString specialistTitle = selectSpecialistQuery.value(3).toString();
	QString specialistFirstName = selectSpecialistQuery.value(4).toString();
	QString specialistLastName = selectSpecialistQuery.value(5).toString();
	QString speciality = selectSpecialistQuery.value(6).toString();


	std::shared_ptr<Specialist> tempSpecialist = std::make_shared<Specialist>(Specialist(tempSpecialistID, specialistFirstName, specialistLastName, speciality));


	std::shared_ptr<Referral> pTempReferral = std::make_shared<Referral>(Referral(tempReferralID, tempDate));
	pTempReferral->addSpecialist(tempSpecialist);
	pTempReferral->addPatient(tempPatient);
	pTempReferral->addGeneralPractitioner(UserManager::Instance()->getActiveGP());

	pTempReferral->setStatus(status);
	pTempReferral->setReferralReason(tempReason);
	


	ReferralManager::Instance()->setActiveReferral(pTempReferral);

	StateManager::StateManagerInstance()->changeState(new ReferralState(true));

}

void ViewReferralsState::loadReferrals()
{
	referralsQueryModel = new QSqlQueryModel();
	//QSqlTableModel* tableModel = new QSqlTableModel(this ,Connection::DatabaseConnection()->getHandle());


	
	QSqlQuery query;

	query.prepare("SELECT * FROM referrals WHERE gps_id='" + QString::number(UserManager::Instance()->getActiveGP()->getID()) + "'");

	query.exec();

	referralsQueryModel->setQuery(query);

	//tableModel->setQuery(query);

	uiViewReferrals.referralsTableView->setModel(referralsQueryModel);


	//int num_rows, row, col;

	//if (!query.exec("SELECT count(id) as num_rows FROM referrals")) qDebug() << query.lastError().text();
	//query.first();

	//num_rows = query.value(0).toInt();
	//uiViewReferrals.tableView->setModel

	//if (!query.exec("SELECT * FROM referrals")) qDebug() << query.lastError().text();
	//
	//uiViewReferrals.tableWidget->setRowCount(num_rows);
	//uiViewReferrals.tableWidget->setColumnCount(query.record().count());

	//uiViewReferrals.tableWidget->setItem(1, 1, new QTableWidgetItem("Hello"));

	//for (row = 0;/* , query.first(); query.isValid(); query.next()*/ row < 3; ++row)
	//{
	//	for (col = 0; col < 3; ++col)
	//	{
	//		uiViewReferrals.tableWidget->setItem(row, col, new QTableWidgetItem(query.value(col).toString()));
	//	}
	//}
	
}

void ViewReferralsState::loadPatients()
{
	QSqlQueryModel* model = new QSqlQueryModel();

	QSqlQuery query;

	query.prepare("SELECT * FROM patients");

	query.exec();

	model->setQuery(query);

	uiViewReferrals.patientsTableView->setModel(model);
}


void ViewReferralsState::loadSpecialists()
{
	QSqlQueryModel* model = new QSqlQueryModel();

	QSqlQuery query;

	query.prepare("SELECT title, first_name, last_name, speciality FROM specialists");

	query.exec();

	model->setQuery(query);

	uiViewReferrals.specialistsTableView->setModel(model);

}


void ViewReferralsState::backToMainMenu()
{
	//StateManager::StateManagerInstance()->changeState(MainScreenState::Instance());
	StateManager::StateManagerInstance()->changeState(new MainScreenState());

}




