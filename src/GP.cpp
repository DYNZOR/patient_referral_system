#include "GP.h"

GP::GP()
{
	sDoctorTitle = "";
	sFirstName = "";
	sLastName = "";

	sUsername = "";
	sPassword = "";
}

GP::GP(unsigned int uiGeneralPractitionerIDin, QString titleIn, QString sFirstNameIn, QString sLastNameIn, QString sUsernameIn, QString sPasswordIn)
{

	uiGeneralPractitionerID = uiGeneralPractitionerIDin;
	sDoctorTitle = titleIn;
	sFirstName = sFirstNameIn;
	sLastName = sLastNameIn;

	sUsername = sUsernameIn;
	sPassword = sPasswordIn;
}

GP::~GP()
{

}

void GP::setName(QString titleIn, QString firstNameIn, QString lastNameIn)
{

	sDoctorTitle = titleIn;
	sFirstName = firstNameIn;
	sLastName = lastNameIn;
}

void GP::setFacility(std::shared_ptr<Facility> facilityIn)
{
	pCurrentFacility = facilityIn;
}

void GP::setContactDetails(QString numberIn, QString emailIn)
{
	sNumber = numberIn;
	sEmail = emailIn;
}

void GP::setID(unsigned int uiIDIn)
{
	uiGeneralPractitionerID = uiIDIn;
}

QString GP::getFirstName()
{
	return sFirstName;
}

QString GP::getLastName()
{
	return sLastName;
}

QString GP::getEmail()
{
	return sEmail;
}

unsigned int GP::getID()
{
	return uiGeneralPractitionerID;
}

std::shared_ptr<Facility> GP::getFacility()
{
	return pCurrentFacility;
}