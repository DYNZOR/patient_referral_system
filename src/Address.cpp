#include "Address.h"

Address::Address()
{
	street = "";
	city = "";
	postcode = "";
}

Address::Address(unsigned int idIn, QString streetIn, QString cityIn, QString postcodeIn)
{ 

	addressId = idIn;
	street = streetIn;
	city = cityIn;
	postcode = postcodeIn;
}

Address::~Address()
{

}

void Address::setNewAddress(QString streetIn, QString cityIn, QString postcodeIn)
{
	street = streetIn;
	city = cityIn;
	postcode = postcodeIn;
}

QString Address::getStreet()
{
	return street;
}

QString Address::getCity()
{
	return city;
}

QString Address::getPostcode()
{
	return postcode;
}

unsigned int Address::getID()
{
	return addressId;
}