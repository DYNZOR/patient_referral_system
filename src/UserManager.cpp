#include "UserManager.h"


UserManager * UserManager::pUserManager;

UserManager * UserManager::Instance()
{
	if (!pUserManager)
	{
		return pUserManager = new UserManager();
	}
	else {

		return pUserManager;
	}
}

UserManager::UserManager()
{
	pUserManager = nullptr;
}

UserManager::~UserManager()
{

}


void UserManager::setActiveGP(std::shared_ptr<GP> referralIn)
{


	if (!pUserManager)
	{
		pActiveGP = referralIn;
	}
	else {

		pActiveGP = referralIn;
	}
}

std::shared_ptr<GP> UserManager::getActiveGP()
{
	return pActiveGP;
}