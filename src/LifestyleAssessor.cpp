#include "LifestyleAssessor.h"


LifestyleAssessor::LifestyleAssessor()
{
	engine = new Engine;
	engine->setName("LifestyleAssessment");

	inputVariable1 = new InputVariable;
	inputVariable1->setEnabled(true);
	inputVariable1->setName("AlcoholIntake");
	inputVariable1->setRange(0.000, 4.000);
	inputVariable1->addTerm(new Rectangle("None", 0.000, 1.000));
	inputVariable1->addTerm(new Rectangle("Low", 1.000, 2.000));
	inputVariable1->addTerm(new Rectangle("Moderate", 2.000, 3.000));
	inputVariable1->addTerm(new Rectangle("High", 3.000, 4.000));
	engine->addInputVariable(inputVariable1);

	inputVariable2 = new InputVariable;
	inputVariable2->setEnabled(true);
	inputVariable2->setName("Smoke");
	inputVariable2->setRange(0.020, 4.000);
	inputVariable2->addTerm(new Rectangle("Light", 1.000, 2.000));
	inputVariable2->addTerm(new Rectangle("None", 0.000, 1.000));
	inputVariable2->addTerm(new Rectangle("Moderate", 2.000, 3.000));
	inputVariable2->addTerm(new Rectangle("Heavy", 3.000, 4.000));
	engine->addInputVariable(inputVariable2);

	inputVariable3 = new InputVariable;
	inputVariable3->setEnabled(true);
	inputVariable3->setName("Exercise");
	inputVariable3->setRange(-0.020, 4.000);
	inputVariable3->addTerm(new Rectangle("None", 0.000, 1.000));
	inputVariable3->addTerm(new Rectangle("Monthly", 1.000, 2.000));
	inputVariable3->addTerm(new Rectangle("Weekly", 2.000, 3.000));
	inputVariable3->addTerm(new Rectangle("Daily", 3.000, 4.000));
	engine->addInputVariable(inputVariable3);

	outputVariable = new OutputVariable;
	outputVariable->setEnabled(true);
	outputVariable->setName("LifestyleRating");
	outputVariable->setRange(0.000, 10.000);
	outputVariable->fuzzyOutput()->setAccumulation(new Maximum);
	outputVariable->setDefuzzifier(new Centroid(200));
	outputVariable->setDefaultValue(fl::nan);
	outputVariable->setLockPreviousOutputValue(false);
	outputVariable->setLockOutputValueInRange(false);
	outputVariable->addTerm(new Ramp("VeryLow", 1.400, 0.900));
	outputVariable->addTerm(new Trapezoid("Low", 0.800, 1.200, 3.500, 4.000));
	outputVariable->addTerm(new Trapezoid("Medium", 3.200, 3.750, 6.250, 7.000));
	outputVariable->addTerm(new Trapezoid("High", 6.000, 6.500, 8.600, 9.000));
	outputVariable->addTerm(new Ramp("VeryHigh", 8.500, 9.000));
	engine->addOutputVariable(outputVariable);

	ruleBlock = new RuleBlock;
	ruleBlock->setEnabled(true);
	ruleBlock->setName("LifestyleRuleBlock");
	ruleBlock->setConjunction(new Minimum);
	ruleBlock->setDisjunction(fl::null);
	ruleBlock->setActivation(new Minimum);
	ruleBlock->addRule(fl::Rule::parse("if AlcoholIntake is None and Smoke is Light and Exercise is None then LifestyleRating is Medium", engine));
	ruleBlock->addRule(fl::Rule::parse("if AlcoholIntake is None and Smoke is Light and Exercise is Monthly then LifestyleRating is Medium", engine));
	ruleBlock->addRule(fl::Rule::parse("if AlcoholIntake is None and Smoke is Light and Exercise is Weekly then LifestyleRating is High", engine));
	ruleBlock->addRule(fl::Rule::parse("if AlcoholIntake is None and Smoke is Light and Exercise is Daily then LifestyleRating is High", engine));
	ruleBlock->addRule(fl::Rule::parse("if AlcoholIntake is None and Smoke is None and Exercise is None then LifestyleRating is Medium", engine));
	ruleBlock->addRule(fl::Rule::parse("if AlcoholIntake is None and Smoke is None and Exercise is Monthly then LifestyleRating is Medium", engine));
	ruleBlock->addRule(fl::Rule::parse("if AlcoholIntake is None and Smoke is None and Exercise is Weekly then LifestyleRating is High", engine));
	ruleBlock->addRule(fl::Rule::parse("if AlcoholIntake is None and Smoke is None and Exercise is Daily then LifestyleRating is VeryHigh", engine));
	ruleBlock->addRule(fl::Rule::parse("if AlcoholIntake is None and Smoke is Moderate and Exercise is None then LifestyleRating is Low", engine));
	ruleBlock->addRule(fl::Rule::parse("if AlcoholIntake is None and Smoke is Moderate and Exercise is Monthly then LifestyleRating is Low", engine));
	ruleBlock->addRule(fl::Rule::parse("if AlcoholIntake is None and Smoke is Moderate and Exercise is Weekly then LifestyleRating is Medium", engine));
	ruleBlock->addRule(fl::Rule::parse("if AlcoholIntake is None and Smoke is Moderate and Exercise is Daily then LifestyleRating is High", engine));
	ruleBlock->addRule(fl::Rule::parse("if AlcoholIntake is None and Smoke is Heavy and Exercise is None then LifestyleRating is VeryLow", engine));
	ruleBlock->addRule(fl::Rule::parse("if AlcoholIntake is None and Smoke is Heavy and Exercise is Monthly then LifestyleRating is VeryLow", engine));
	ruleBlock->addRule(fl::Rule::parse("if AlcoholIntake is None and Smoke is Heavy and Exercise is Weekly then LifestyleRating is Low", engine));
	ruleBlock->addRule(fl::Rule::parse("if AlcoholIntake is None and Smoke is Heavy and Exercise is Daily then LifestyleRating is Medium", engine));
	ruleBlock->addRule(fl::Rule::parse("if AlcoholIntake is Low and Smoke is Light and Exercise is None then LifestyleRating is Low", engine));
	ruleBlock->addRule(fl::Rule::parse("if AlcoholIntake is Low and Smoke is Light and Exercise is Monthly then LifestyleRating is Medium", engine));
	ruleBlock->addRule(fl::Rule::parse("if AlcoholIntake is Low and Smoke is Light and Exercise is Weekly then LifestyleRating is Medium", engine));
	ruleBlock->addRule(fl::Rule::parse("if AlcoholIntake is Low and Smoke is Light and Exercise is Daily then LifestyleRating is High", engine));
	ruleBlock->addRule(fl::Rule::parse("if AlcoholIntake is Low and Smoke is None and Exercise is None then LifestyleRating is Low", engine));
	ruleBlock->addRule(fl::Rule::parse("if AlcoholIntake is Low and Smoke is None and Exercise is Monthly then LifestyleRating is Medium", engine));
	ruleBlock->addRule(fl::Rule::parse("if AlcoholIntake is Low and Smoke is None and Exercise is Weekly then LifestyleRating is High", engine));
	ruleBlock->addRule(fl::Rule::parse("if AlcoholIntake is Low and Smoke is None and Exercise is Daily then LifestyleRating is VeryHigh", engine));
	ruleBlock->addRule(fl::Rule::parse("if AlcoholIntake is Low and Smoke is Moderate and Exercise is None then LifestyleRating is Low", engine));
	ruleBlock->addRule(fl::Rule::parse("if AlcoholIntake is Low and Smoke is Moderate and Exercise is Monthly then LifestyleRating is Medium", engine));
	ruleBlock->addRule(fl::Rule::parse("if AlcoholIntake is Low and Smoke is Moderate and Exercise is Weekly then LifestyleRating is Medium", engine));
	ruleBlock->addRule(fl::Rule::parse("if AlcoholIntake is Low and Smoke is Moderate and Exercise is Daily then LifestyleRating is High", engine));
	ruleBlock->addRule(fl::Rule::parse("if AlcoholIntake is Low and Smoke is Heavy and Exercise is None then LifestyleRating is VeryLow", engine));
	ruleBlock->addRule(fl::Rule::parse("if AlcoholIntake is Low and Smoke is Heavy and Exercise is Monthly then LifestyleRating is VeryLow", engine));
	ruleBlock->addRule(fl::Rule::parse("if AlcoholIntake is Low and Smoke is Heavy and Exercise is Weekly then LifestyleRating is Low", engine));
	ruleBlock->addRule(fl::Rule::parse("if AlcoholIntake is Low and Smoke is Heavy and Exercise is Daily then LifestyleRating is Medium", engine));
	ruleBlock->addRule(fl::Rule::parse("if AlcoholIntake is Moderate and Smoke is Light and Exercise is None then LifestyleRating is Low", engine));
	ruleBlock->addRule(fl::Rule::parse("if AlcoholIntake is Moderate and Smoke is Light and Exercise is Monthly then LifestyleRating is Low", engine));
	ruleBlock->addRule(fl::Rule::parse("if AlcoholIntake is Moderate and Smoke is Light and Exercise is Weekly then LifestyleRating is Medium", engine));
	ruleBlock->addRule(fl::Rule::parse("if AlcoholIntake is Moderate and Smoke is Light and Exercise is Daily then LifestyleRating is High", engine));
	ruleBlock->addRule(fl::Rule::parse("if AlcoholIntake is Moderate and Smoke is None and Exercise is None then LifestyleRating is Medium", engine));
	ruleBlock->addRule(fl::Rule::parse("if AlcoholIntake is Moderate and Smoke is None and Exercise is Monthly then LifestyleRating is Medium", engine));
	ruleBlock->addRule(fl::Rule::parse("if AlcoholIntake is Moderate and Smoke is None and Exercise is Weekly then LifestyleRating is High", engine));
	ruleBlock->addRule(fl::Rule::parse("if AlcoholIntake is Moderate and Smoke is None and Exercise is Daily then LifestyleRating is VeryHigh", engine));
	ruleBlock->addRule(fl::Rule::parse("if AlcoholIntake is Moderate and Smoke is Moderate and Exercise is None then LifestyleRating is Low", engine));
	ruleBlock->addRule(fl::Rule::parse("if AlcoholIntake is Moderate and Smoke is Moderate and Exercise is Monthly then LifestyleRating is Low", engine));
	ruleBlock->addRule(fl::Rule::parse("if AlcoholIntake is Moderate and Smoke is Moderate and Exercise is Weekly then LifestyleRating is Medium", engine));
	ruleBlock->addRule(fl::Rule::parse("if AlcoholIntake is Moderate and Smoke is Moderate and Exercise is Daily then LifestyleRating is High", engine));
	ruleBlock->addRule(fl::Rule::parse("if AlcoholIntake is Moderate and Smoke is Heavy and Exercise is None then LifestyleRating is VeryLow", engine));
	ruleBlock->addRule(fl::Rule::parse("if AlcoholIntake is Moderate and Smoke is Heavy and Exercise is Monthly then LifestyleRating is Low", engine));
	ruleBlock->addRule(fl::Rule::parse("if AlcoholIntake is Moderate and Smoke is Heavy and Exercise is Weekly then LifestyleRating is Low", engine));
	ruleBlock->addRule(fl::Rule::parse("if AlcoholIntake is Moderate and Smoke is Heavy and Exercise is Daily then LifestyleRating is Medium", engine));
	ruleBlock->addRule(fl::Rule::parse("if AlcoholIntake is High and Smoke is Light and Exercise is None then LifestyleRating is Low", engine));
	ruleBlock->addRule(fl::Rule::parse("if AlcoholIntake is High and Smoke is Light and Exercise is Monthly then LifestyleRating is Low", engine));
	ruleBlock->addRule(fl::Rule::parse("if AlcoholIntake is High and Smoke is Light and Exercise is Weekly then LifestyleRating is Medium", engine));
	ruleBlock->addRule(fl::Rule::parse("if AlcoholIntake is High and Smoke is Light and Exercise is Daily then LifestyleRating is High", engine));
	ruleBlock->addRule(fl::Rule::parse("if AlcoholIntake is High and Smoke is None and Exercise is None then LifestyleRating is Low", engine));
	ruleBlock->addRule(fl::Rule::parse("if AlcoholIntake is High and Smoke is None and Exercise is Monthly then LifestyleRating is Low", engine));
	ruleBlock->addRule(fl::Rule::parse("if AlcoholIntake is High and Smoke is None and Exercise is Weekly then LifestyleRating is Medium", engine));
	ruleBlock->addRule(fl::Rule::parse("if AlcoholIntake is High and Smoke is None and Exercise is Daily then LifestyleRating is High", engine));
	ruleBlock->addRule(fl::Rule::parse("if AlcoholIntake is High and Smoke is Moderate and Exercise is None then LifestyleRating is VeryLow", engine));
	ruleBlock->addRule(fl::Rule::parse("if AlcoholIntake is High and Smoke is Moderate and Exercise is Monthly then LifestyleRating is Low", engine));
	ruleBlock->addRule(fl::Rule::parse("if AlcoholIntake is High and Smoke is Moderate and Exercise is Weekly then LifestyleRating is Low", engine));
	ruleBlock->addRule(fl::Rule::parse("if AlcoholIntake is High and Smoke is Moderate and Exercise is Daily then LifestyleRating is Medium", engine));
	ruleBlock->addRule(fl::Rule::parse("if AlcoholIntake is High and Smoke is Heavy and Exercise is None then LifestyleRating is VeryLow", engine));
	ruleBlock->addRule(fl::Rule::parse("if AlcoholIntake is High and Smoke is Heavy and Exercise is Monthly then LifestyleRating is VeryLow", engine));
	ruleBlock->addRule(fl::Rule::parse("if AlcoholIntake is High and Smoke is Heavy and Exercise is Weekly then LifestyleRating is VeryLow", engine));
	ruleBlock->addRule(fl::Rule::parse("if AlcoholIntake is High and Smoke is Heavy and Exercise is Daily then LifestyleRating is Low", engine));
	engine->addRuleBlock(ruleBlock);

}

void LifestyleAssessor::setAlcohol(int alcoholIn)
{
	engine->setInputValue("AlcoholIntake", alcoholIn);

}

void LifestyleAssessor::setSmoke(int smokeIn)
{
	engine->setInputValue("Smoke", smokeIn);

}

void LifestyleAssessor::setExercise(int exerciseIn)
{
	engine->setInputValue("Exercise", exerciseIn);

}

double LifestyleAssessor::getRating()
{
	engine->process();
	return engine->getOutputValue("LifestyleRating");
}