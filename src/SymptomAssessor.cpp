#include "SymptomAssessor.h"


SymptomAssessor::SymptomAssessor()
{

	engine = new Engine;
	engine->setName("SymptomAssessment");

	inputVariable1 = new InputVariable;
	inputVariable1->setEnabled(true);
	inputVariable1->setName("Discomfort");
	inputVariable1->setRange(0.000, 10.000);
	inputVariable1->addTerm(new Ramp("VeryLow", 2.000, 1.500));
	inputVariable1->addTerm(new Ramp("Low", 3.000, 3.000));
	inputVariable1->addTerm(new Trapezoid("Low", 1.500, 2.000, 3.700, 4.000));
	inputVariable1->addTerm(new Trapezoid("Medium", 3.500, 4.000, 6.650, 7.150));
	inputVariable1->addTerm(new Trapezoid("High", 6.500, 7.000, 8.625, 9.000));
	inputVariable1->addTerm(new Ramp("VeryHigh", 8.400, 9.000));
	engine->addInputVariable(inputVariable1);

	inputVariable2 = new InputVariable;
	inputVariable2->setEnabled(true);
	inputVariable2->setName("Duration");
	inputVariable2->setRange(0.000, 360.000);
	inputVariable2->addTerm(new ZShape("veryshort_duration", 3.600, 14.400));
	inputVariable2->addTerm(new Gaussian("short_duration", 18.000, 9.000));
	inputVariable2->addTerm(new Gaussian("medium_duration", 61.200, 27.000));
	inputVariable2->addTerm(new Gaussian("long_duration", 180.000, 55.800));
	inputVariable2->addTerm(new SShape("verylong_duration", 169.200, 324.000));
	engine->addInputVariable(inputVariable2);

	outputVariable = new OutputVariable;
	outputVariable->setEnabled(true);
	outputVariable->setName("SymptomSeverity");
	outputVariable->setRange(0.000, 10.000);
	outputVariable->fuzzyOutput()->setAccumulation(new Maximum);
	outputVariable->setDefuzzifier(new Centroid(200));
	outputVariable->setDefaultValue(fl::nan);
	outputVariable->setLockPreviousOutputValue(false);
	outputVariable->setLockOutputValueInRange(false);
	outputVariable->addTerm(new Ramp("VeryLow", 1.400, 0.900));
	outputVariable->addTerm(new Trapezoid("Low", 0.800, 1.200, 3.500, 4.000));
	outputVariable->addTerm(new Trapezoid("Medium", 3.200, 3.750, 6.250, 7.000));
	outputVariable->addTerm(new Trapezoid("High", 6.000, 6.500, 8.600, 9.000));
	outputVariable->addTerm(new Ramp("VeryHigh", 8.500, 9.000));
	engine->addOutputVariable(outputVariable);


    ruleBlock = new RuleBlock;
	ruleBlock->setEnabled(true);
	ruleBlock->setName("Symptom_Severity_Ruleblock");
	ruleBlock->setConjunction(new Minimum);
	ruleBlock->setDisjunction(fl::null);
	ruleBlock->setActivation(new Minimum);
	ruleBlock->addRule(fl::Rule::parse("if Discomfort is VeryLow and Duration is veryshort_duration  then SymptomSeverity is VeryLow", engine));
	ruleBlock->addRule(fl::Rule::parse("if Discomfort is VeryLow and Duration is short_duration then SymptomSeverity is Low", engine));
	ruleBlock->addRule(fl::Rule::parse("if Discomfort is VeryLow and Duration is medium_duration then SymptomSeverity is Low", engine));
	ruleBlock->addRule(fl::Rule::parse("if Discomfort is VeryLow and Duration is long_duration then SymptomSeverity is Medium", engine));
	ruleBlock->addRule(fl::Rule::parse("if Discomfort is VeryLow and Duration is verylong_duration then SymptomSeverity is Medium", engine));
	ruleBlock->addRule(fl::Rule::parse("if Discomfort is Low and Duration is veryshort_duration  then SymptomSeverity is VeryLow", engine));
	ruleBlock->addRule(fl::Rule::parse("if Discomfort is Low and Duration is short_duration then SymptomSeverity is Low", engine));
	ruleBlock->addRule(fl::Rule::parse("if Discomfort is Low and Duration is medium_duration then SymptomSeverity is Medium", engine));
	ruleBlock->addRule(fl::Rule::parse("if Discomfort is Low and Duration is long_duration then SymptomSeverity is High", engine));
	ruleBlock->addRule(fl::Rule::parse("if Discomfort is Low and Duration is verylong_duration then SymptomSeverity is High", engine));
	ruleBlock->addRule(fl::Rule::parse("if Discomfort is Medium and Duration is veryshort_duration then SymptomSeverity is Medium", engine));
	ruleBlock->addRule(fl::Rule::parse("if Discomfort is Medium and Duration is short_duration then SymptomSeverity is Medium", engine));
	ruleBlock->addRule(fl::Rule::parse("if Discomfort is Medium and Duration is medium_duration then SymptomSeverity is Medium", engine));
	ruleBlock->addRule(fl::Rule::parse("if Discomfort is Medium and Duration is long_duration then SymptomSeverity is High", engine));
	ruleBlock->addRule(fl::Rule::parse("if Discomfort is Medium and Duration is verylong_duration then SymptomSeverity is VeryHigh", engine));
	ruleBlock->addRule(fl::Rule::parse("if Discomfort is High and Duration is veryshort_duration then SymptomSeverity is High", engine));
	ruleBlock->addRule(fl::Rule::parse("if Discomfort is High and Duration is short_duration then SymptomSeverity is High", engine));
	ruleBlock->addRule(fl::Rule::parse("if Discomfort is High and Duration is medium_duration then SymptomSeverity is High", engine));
	ruleBlock->addRule(fl::Rule::parse("if Discomfort is High and Duration is long_duration then SymptomSeverity is VeryHigh", engine));
	ruleBlock->addRule(fl::Rule::parse("if Discomfort is High and Duration is verylong_duration then SymptomSeverity is VeryHigh", engine));
	ruleBlock->addRule(fl::Rule::parse("if Discomfort is VeryHigh and Duration is veryshort_duration then SymptomSeverity is High", engine));
	ruleBlock->addRule(fl::Rule::parse("if Discomfort is VeryHigh and Duration is short_duration then SymptomSeverity is VeryHigh", engine));
	ruleBlock->addRule(fl::Rule::parse("if Discomfort is VeryHigh and Duration is medium_duration then SymptomSeverity is VeryHigh", engine));
	ruleBlock->addRule(fl::Rule::parse("if Discomfort is VeryHigh and Duration is long_duration then SymptomSeverity is VeryHigh", engine));
	ruleBlock->addRule(fl::Rule::parse("if Discomfort is VeryHigh and Duration is verylong_duration then SymptomSeverity is VeryHigh", engine));
	engine->addRuleBlock(ruleBlock);


}

void SymptomAssessor::setDiscomfort(double valueIn)
{

	// Check to see if system already has input variable 

	// Check to see if data
	engine->setInputValue("Discomfort", valueIn);

}

void SymptomAssessor::setDuration(double daysValueIn)
{
	engine->setInputValue("Duration", daysValueIn);
}

double SymptomAssessor::getSeverity()
{
	engine->process();
	return engine->getOutputValue("SymptomSeverity");
}



