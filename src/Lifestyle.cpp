#include "Lifestyle.h"

Lifestyle::Lifestyle()
{

}

Lifestyle::Lifestyle(QString drinkerIn, QString smokerIn, QString exerciseIn)
{
	drinker = drinkerIn;
	smoker = smokerIn;
	exercise = exerciseIn;
}

QString Lifestyle::getDrinkerStatus()
{
	return drinker;
}
QString Lifestyle::getExerciseStatus()
{
	return exercise;
}
QString Lifestyle::getSmokerStatus()
{
	return smoker;
}
