#include "StateManager.h"

StateManager* StateManager::stateManager;


void StateManager::changeState(QWidget* newState)
{
	if (!states.empty())
	{
		states.back()->deleteLater();
		states.pop_back();
	}
		
	states.push_back(newState);
	states.back()->show();
}

void StateManager::pushState(QWidget* newState)
{

	// Check if states are empty 
	// If are pause current state 

	states.push_back(newState);
}

void StateManager::popState(QWidget* newState)
{

}

void StateManager::showDialog(QWidget* newDialog)
{
	//if (!activeDialog)
	//{
		activeDialog = newDialog;
		activeDialog->show();
	//}
}

void StateManager::closeDialog()
{
	activeDialog->deleteLater();
	activeDialog = nullptr;
}


QWidget* StateManager::getCurrentState()
{

	return states.back();

	//return currentState;
}
