#include "MainScreenState.h"

//#include <QtCore/QVariant>

//MainScreenState* MainScreenState::mainScreenState;

MainScreenState::MainScreenState(QWidget* parent) : QWidget(parent)
{
	//setType(MAINSCREEN);

	uiMainScreen.setupUi(this);
	connect(uiMainScreen.viewReferralsBtn, SIGNAL(clicked()), this, SLOT(viewReferrals()));
	connect(uiMainScreen.logoutBtn, SIGNAL(clicked()), this, SLOT(logOut()));

}


void MainScreenState::setupUserInterface()
{
	//uiLogin->setupUi(window);

}
void MainScreenState::setupSignals()
{
	//window->connect(uiLogin.pushButton, SIGNAL(clicked()), window, SLOT(login()));

}

void MainScreenState::viewReferrals()
{
	//StateManager::StateManagerInstance()->changeState(ViewReferralsState::Instance());
	StateManager::StateManagerInstance()->changeState(new ViewReferralsState());

	//pStateManager->getCurrentState()->connect()

}

void MainScreenState::logOut()
{
	//StateManager::StateManagerInstance()->changeState(ViewReferralsState::Instance());
	StateManager::StateManagerInstance()->changeState(new LoginState());

	//pStateManager->getCurrentState()->connect()

}