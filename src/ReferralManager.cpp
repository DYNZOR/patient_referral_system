#include "ReferralManager.h"


ReferralManager * ReferralManager::pReferralManager;

ReferralManager * ReferralManager::Instance()
{
	if (!pReferralManager)
	{
		return pReferralManager = new ReferralManager();
	}
	else {

		return pReferralManager;
	}
}

ReferralManager::ReferralManager()
{
	pActiveReferral = nullptr;
}

ReferralManager::~ReferralManager()
{

}

void ReferralManager::createReferral()
{

}

void ReferralManager::setActiveReferral(std::shared_ptr<Referral> referralIn)
{
	if (!pActiveReferral)
	{
		pActiveReferral = referralIn;
	}
	else {
		// Perform referral swap 
		pActiveReferral = referralIn;
	}
}

std::shared_ptr<Referral> ReferralManager::getActiveReferral()
{
	return pActiveReferral;
}