#include "Symptom.h"

Symptom::Symptom()
{
	sName = "";
	sBodyLocation = "";
	fDiscomfort = 0;
	fDurationDays = 0;
}

Symptom::Symptom(unsigned int idIn, QString sNameIn, QString sBodyLocationIn, unsigned int discomfortIn, unsigned int daysIn)
{
	uiSymptomID = idIn;
	sName = sNameIn;
	sBodyLocation = sBodyLocationIn;
	fDiscomfort = discomfortIn;
	fDurationDays = daysIn;
}

Symptom::~Symptom()
{

}

void Symptom::setDescription()
{

}

void Symptom::setDiscomfort(unsigned int discomfortIn)
{
	fDiscomfort = discomfortIn;
}

void Symptom::setDurationInDays(unsigned int daysIn)
{
	fDurationDays = daysIn;
}
void Symptom::setSeverity(double severityIn)
{
	fSeverity = severityIn;
}

void Symptom::setID(unsigned int idIn)
{
	uiSymptomID = idIn;
}

unsigned int Symptom::getID()
{
	return uiSymptomID;
}

unsigned int Symptom::getDiscomfort()
{
	return fDiscomfort;
}

unsigned int Symptom::getDurationInDays()
{
	return fDurationDays;
}

double Symptom::getSeverity()
{
	return fSeverity;
}

QString Symptom::getBodyLocation()
{
	return sBodyLocation;
}

QString Symptom::getName()
{
	return sName;
}
