#include "Connection.h"
Connection* Connection::pDbConnection;

Connection::Connection()
{
	sHostName = "localhost";
	sDatabaseName = "referral_system_db";
	sUserName = "root";
	sPassword = "";

	connect();
}

Connection::~Connection()
{

}

void Connection::connect()
{
	db = QSqlDatabase::addDatabase("QMYSQL");

	db.setHostName(sHostName);
	db.setDatabaseName(sDatabaseName);
	db.setPort(3306);
	db.setUserName(sUserName);
	db.setPassword(sPassword);

	if (!db.open())
	{
		qDebug() << "Database connection failed!";
		

		establishedConnection = false;
	}
	else {
		establishedConnection = true;
	}
}

bool Connection::connected()
{
	return establishedConnection;
}

QSqlDatabase Connection::getHandle()
{
	return db;
}

