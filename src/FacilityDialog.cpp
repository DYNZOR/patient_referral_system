#include "FacilityDialog.h"


FacilityDialog::FacilityDialog(QWidget* parent) : QWidget(parent)
{

	setupUserInterface();
	setupSignals();

}

void FacilityDialog::setupUserInterface()
{
	uiDialog.setupUi(this);

}

void FacilityDialog::setupSignals()
{
	connect(uiDialog.addFacilityBtn, SIGNAL(clicked()), this, SLOT(addFacility()));
}

void FacilityDialog::addFacility()
{
	QSqlQuery insertNewAddress;
	QSqlQuery insertNewFacility;

	std::shared_ptr<Facility> pNewFacility;
	std::shared_ptr<Address> pNewAddress;


	QString name = uiDialog.nameEdit->text();
	QString type = uiDialog.facilityType->currentText();

	QString street = uiDialog.streetEdit->text();
	QString city = uiDialog.cityEdit->text();
	QString postcode = uiDialog.postcodeEdit->text();


	insertNewAddress.prepare("INSERT INTO addresses (street, city, postcode) VALUES (:strt,:ct, :pstcd)");
	insertNewAddress.bindValue(":strt", name);
	insertNewAddress.bindValue(":ct", city);
	insertNewAddress.bindValue(":pstcd", postcode);

	unsigned int newAddressId;

	if (!insertNewAddress.exec("SELECT LAST_INSERT_ID()")) qDebug() << insertNewAddress.lastError().text();
	insertNewAddress.first();

	newAddressId = insertNewAddress.value(0).toInt();


	insertNewFacility.prepare("INSERT INTO facility (name, type, addresses_id) VALUES (:nm,:pType, :adId)");
	insertNewFacility.bindValue(":nm", name);
	insertNewFacility.bindValue(":pType", type);
	insertNewFacility.bindValue(":adId", newAddressId);


	if (!insertNewFacility.exec()) qDebug() << insertNewFacility.lastError().text();

	unsigned int newFacilityID;

	if (!insertNewFacility.exec("SELECT LAST_INSERT_ID()")) qDebug() << insertNewFacility.lastError().text();
	insertNewFacility.first();

	newFacilityID = insertNewFacility.value(0).toInt();
	

	pNewAddress = std::make_shared<Address>(Address( newAddressId,street, city, postcode));

	pNewFacility = std::make_shared<Facility>(Facility(newFacilityID, name, type));
	pNewFacility->setAddress(pNewAddress);
	
	
	UserManager::Instance()->getActiveGP()->setFacility(pNewFacility);

}
