#include "HealthAssessor.h"


HealthAssessor::HealthAssessor()
{

	engine = new Engine;
	engine->setName("HealthAssessment");

	inputVariable1 = new InputVariable;
	inputVariable1->setEnabled(true);
	inputVariable1->setName("Cholesterol");
	inputVariable1->setRange(0.000, 500.000);
	inputVariable1->addTerm(new Ramp("Low", 200.000, 150.000));
	inputVariable1->addTerm(new Triangle("Medium", 180.000, 200.000, 250.000));
	inputVariable1->addTerm(new Triangle("High", 220.000, 250.000, 300.000));
	inputVariable1->addTerm(new Ramp("VeryHigh", 275.000, 350.000));
	engine->addInputVariable(inputVariable1);

	inputVariable2 = new InputVariable;
	inputVariable2->setEnabled(true);
	inputVariable2->setName("BloodPressure");
	inputVariable2->setRange(0.080, 300.000);
	inputVariable2->addTerm(new Ramp("Low", 135.000, 110.000));
	inputVariable2->addTerm(new Triangle("Medium", 120.000, 135.000, 155.000));
	inputVariable2->addTerm(new Triangle("High", 140.000, 155.000, 175.000));
	inputVariable2->addTerm(new Ramp("VeryHigh", 155.000, 175.000));
	engine->addInputVariable(inputVariable2);

	outputVariable = new OutputVariable;
	outputVariable->setEnabled(true);
	outputVariable->setName("Health");
	outputVariable->setRange(0.000, 10.000);
	outputVariable->fuzzyOutput()->setAccumulation(new Maximum);
	outputVariable->setDefuzzifier(new Centroid(200));
	outputVariable->setDefaultValue(fl::nan);
	outputVariable->setLockPreviousOutputValue(false);
	outputVariable->setLockOutputValueInRange(false);
	outputVariable->addTerm(new Rectangle("VeryGood", 0.000, 1.500));
	outputVariable->addTerm(new Rectangle("Good", 1.500, 3.500));
	outputVariable->addTerm(new Rectangle("Medium", 3.500, 6.500));
	outputVariable->addTerm(new Rectangle("Bad", 6.500, 8.500));
	outputVariable->addTerm(new Rectangle("VeryBad", 8.500, 10.000));
	engine->addOutputVariable(outputVariable);

	ruleBlock = new RuleBlock;
	ruleBlock->setEnabled(true);
	ruleBlock->setName("");
	ruleBlock->setConjunction(new Minimum);
	ruleBlock->setDisjunction(fl::null);
	ruleBlock->setActivation(new Minimum);
	ruleBlock->addRule(fl::Rule::parse("if Cholesterol is Low and BloodPressure is Low then Health is VeryGood", engine));
	ruleBlock->addRule(fl::Rule::parse("if Cholesterol is Low and BloodPressure is Medium then Health is Good", engine));
	ruleBlock->addRule(fl::Rule::parse("if Cholesterol is Low and BloodPressure is High then Health is Medium", engine));
	ruleBlock->addRule(fl::Rule::parse("if Cholesterol is Low and BloodPressure is VeryHigh then Health is Bad", engine));
	ruleBlock->addRule(fl::Rule::parse("if Cholesterol is Medium and BloodPressure is Low then Health is Good", engine));
	ruleBlock->addRule(fl::Rule::parse("if Cholesterol is Medium and BloodPressure is Medium then Health is Medium", engine));
	ruleBlock->addRule(fl::Rule::parse("if Cholesterol is Medium and BloodPressure is High then Health is Medium", engine));
	ruleBlock->addRule(fl::Rule::parse("if Cholesterol is Medium and BloodPressure is VeryHigh then Health is Bad", engine));
	ruleBlock->addRule(fl::Rule::parse("if Cholesterol is High and BloodPressure is Low then Health is Medium", engine));
	ruleBlock->addRule(fl::Rule::parse("if Cholesterol is High and BloodPressure is Medium then Health is Bad", engine));
	ruleBlock->addRule(fl::Rule::parse("if Cholesterol is High and BloodPressure is High then Health is Bad", engine));
	ruleBlock->addRule(fl::Rule::parse("if Cholesterol is High and BloodPressure is VeryHigh then Health is VeryBad", engine));
	ruleBlock->addRule(fl::Rule::parse("if Cholesterol is VeryHigh and BloodPressure is Low then Health is Bad", engine));
	ruleBlock->addRule(fl::Rule::parse("if Cholesterol is VeryHigh and BloodPressure is Medium then Health is Bad", engine));
	ruleBlock->addRule(fl::Rule::parse("if Cholesterol is VeryHigh and BloodPressure is High then Health is VeryBad", engine));
	ruleBlock->addRule(fl::Rule::parse("if Cholesterol is VeryHigh and BloodPressure is VeryHigh then Health is VeryBad", engine));
	engine->addRuleBlock(ruleBlock);
}

void HealthAssessor::setCholesterol(unsigned int cholesterolIn)
{
	engine->setInputValue("Cholesterol", cholesterolIn);

}

void HealthAssessor::setBloodPressure(unsigned int bpIn)
{
	engine->setInputValue("BloodPressure", bpIn);
}

double HealthAssessor::getHealth()
{
	engine->process();
	return engine->getOutputValue("Health");
}