/********************************************************************************
** Form generated from reading UI file 'view_referrals.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_VIEW_REFERRALS_H
#define UI_VIEW_REFERRALS_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTableView>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ViewReferrals
{
public:
    QTabWidget *tabWidget;
    QWidget *tab;
    QTableView *referralsTableView;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QPushButton *addNewReferralBtn;
    QPushButton *mainMenuBtn;
    QWidget *tab_2;
    QTableView *patientsTableView;
    QWidget *tab_3;
    QTableView *specialistsTableView;

    void setupUi(QWidget *ViewReferrals)
    {
        if (ViewReferrals->objectName().isEmpty())
            ViewReferrals->setObjectName(QStringLiteral("ViewReferrals"));
        ViewReferrals->resize(789, 637);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(ViewReferrals->sizePolicy().hasHeightForWidth());
        ViewReferrals->setSizePolicy(sizePolicy);
        tabWidget = new QTabWidget(ViewReferrals);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        tabWidget->setGeometry(QRect(0, 10, 791, 641));
        tab = new QWidget();
        tab->setObjectName(QStringLiteral("tab"));
        referralsTableView = new QTableView(tab);
        referralsTableView->setObjectName(QStringLiteral("referralsTableView"));
        referralsTableView->setGeometry(QRect(20, 10, 751, 491));
        referralsTableView->setSelectionBehavior(QAbstractItemView::SelectRows);
        verticalLayoutWidget = new QWidget(tab);
        verticalLayoutWidget->setObjectName(QStringLiteral("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(20, 510, 751, 54));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        addNewReferralBtn = new QPushButton(verticalLayoutWidget);
        addNewReferralBtn->setObjectName(QStringLiteral("addNewReferralBtn"));

        verticalLayout->addWidget(addNewReferralBtn);

        mainMenuBtn = new QPushButton(verticalLayoutWidget);
        mainMenuBtn->setObjectName(QStringLiteral("mainMenuBtn"));

        verticalLayout->addWidget(mainMenuBtn);

        tabWidget->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QStringLiteral("tab_2"));
        patientsTableView = new QTableView(tab_2);
        patientsTableView->setObjectName(QStringLiteral("patientsTableView"));
        patientsTableView->setGeometry(QRect(20, 20, 741, 491));
        tabWidget->addTab(tab_2, QString());
        tab_3 = new QWidget();
        tab_3->setObjectName(QStringLiteral("tab_3"));
        specialistsTableView = new QTableView(tab_3);
        specialistsTableView->setObjectName(QStringLiteral("specialistsTableView"));
        specialistsTableView->setGeometry(QRect(20, 20, 741, 491));
        tabWidget->addTab(tab_3, QString());

        retranslateUi(ViewReferrals);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(ViewReferrals);
    } // setupUi

    void retranslateUi(QWidget *ViewReferrals)
    {
        ViewReferrals->setWindowTitle(QApplication::translate("ViewReferrals", "View Referrals", 0));
        addNewReferralBtn->setText(QApplication::translate("ViewReferrals", "New Referral", 0));
        mainMenuBtn->setText(QApplication::translate("ViewReferrals", "Back", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("ViewReferrals", "Stored Referrals", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QApplication::translate("ViewReferrals", "Patients ", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab_3), QApplication::translate("ViewReferrals", "Specialists", 0));
    } // retranslateUi

};

namespace Ui {
    class ViewReferrals: public Ui_ViewReferrals {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_VIEW_REFERRALS_H
