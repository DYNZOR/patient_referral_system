/********************************************************************************
** Form generated from reading UI file 'login.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LOGIN_H
#define UI_LOGIN_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Login
{
public:
    QStackedWidget *stackedWidget;
    QWidget *loginPage;
    QWidget *formLayoutWidget;
    QFormLayout *formLayout;
    QLabel *loginStatusLabel;
    QLabel *label;
    QLineEdit *userNamelineEdit;
    QLabel *label_2;
    QLineEdit *passwordLineEdit;
    QLabel *label_9;
    QComboBox *comboBox;
    QPushButton *loginBtn;
    QPushButton *signupBtn;
    QLabel *label_7;
    QWidget *signupPage;
    QLabel *label_8;
    QWidget *formLayoutWidget_2;
    QFormLayout *formLayout_2;
    QLabel *label_6;
    QLineEdit *firstNameEdit;
    QLabel *label_5;
    QLineEdit *lastNameEdit;
    QLabel *label_3;
    QLineEdit *userNameEditSignup;
    QLabel *label_4;
    QLineEdit *passwordEditSignup;
    QLabel *typeLabel;
    QComboBox *accountType;
    QLabel *label_10;
    QWidget *formLayoutWidget_5;
    QFormLayout *formLayout_5;
    QLabel *label_22;
    QLineEdit *nameEdit;
    QLabel *label_19;
    QLineEdit *streetEdit;
    QLabel *label_20;
    QLineEdit *cityEdit;
    QLabel *label_21;
    QLineEdit *postcodeEdit;
    QLabel *typeLabel_3;
    QComboBox *facilityType;
    QLabel *label_11;
    QPushButton *createAccountBtn;
    QLabel *errorLabel;
    QPushButton *backToLogin;

    void setupUi(QWidget *Login)
    {
        if (Login->objectName().isEmpty())
            Login->setObjectName(QStringLiteral("Login"));
        Login->setEnabled(true);
        Login->resize(387, 315);
        stackedWidget = new QStackedWidget(Login);
        stackedWidget->setObjectName(QStringLiteral("stackedWidget"));
        stackedWidget->setGeometry(QRect(0, 0, 381, 311));
        loginPage = new QWidget();
        loginPage->setObjectName(QStringLiteral("loginPage"));
        formLayoutWidget = new QWidget(loginPage);
        formLayoutWidget->setObjectName(QStringLiteral("formLayoutWidget"));
        formLayoutWidget->setGeometry(QRect(70, 70, 251, 181));
        formLayout = new QFormLayout(formLayoutWidget);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        formLayout->setContentsMargins(0, 0, 0, 0);
        loginStatusLabel = new QLabel(formLayoutWidget);
        loginStatusLabel->setObjectName(QStringLiteral("loginStatusLabel"));
        loginStatusLabel->setAlignment(Qt::AlignCenter);

        formLayout->setWidget(0, QFormLayout::SpanningRole, loginStatusLabel);

        label = new QLabel(formLayoutWidget);
        label->setObjectName(QStringLiteral("label"));

        formLayout->setWidget(1, QFormLayout::LabelRole, label);

        userNamelineEdit = new QLineEdit(formLayoutWidget);
        userNamelineEdit->setObjectName(QStringLiteral("userNamelineEdit"));
        userNamelineEdit->setEnabled(true);

        formLayout->setWidget(1, QFormLayout::FieldRole, userNamelineEdit);

        label_2 = new QLabel(formLayoutWidget);
        label_2->setObjectName(QStringLiteral("label_2"));

        formLayout->setWidget(2, QFormLayout::LabelRole, label_2);

        passwordLineEdit = new QLineEdit(formLayoutWidget);
        passwordLineEdit->setObjectName(QStringLiteral("passwordLineEdit"));
        passwordLineEdit->setMinimumSize(QSize(175, 0));
        passwordLineEdit->setInputMethodHints(Qt::ImhEmailCharactersOnly|Qt::ImhHiddenText|Qt::ImhNoAutoUppercase|Qt::ImhNoPredictiveText|Qt::ImhSensitiveData);
        passwordLineEdit->setEchoMode(QLineEdit::Password);

        formLayout->setWidget(2, QFormLayout::FieldRole, passwordLineEdit);

        label_9 = new QLabel(formLayoutWidget);
        label_9->setObjectName(QStringLiteral("label_9"));

        formLayout->setWidget(3, QFormLayout::LabelRole, label_9);

        comboBox = new QComboBox(formLayoutWidget);
        comboBox->setObjectName(QStringLiteral("comboBox"));

        formLayout->setWidget(3, QFormLayout::FieldRole, comboBox);

        loginBtn = new QPushButton(formLayoutWidget);
        loginBtn->setObjectName(QStringLiteral("loginBtn"));

        formLayout->setWidget(5, QFormLayout::SpanningRole, loginBtn);

        signupBtn = new QPushButton(formLayoutWidget);
        signupBtn->setObjectName(QStringLiteral("signupBtn"));

        formLayout->setWidget(6, QFormLayout::SpanningRole, signupBtn);

        label_7 = new QLabel(loginPage);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setGeometry(QRect(160, 10, 71, 51));
        QFont font;
        font.setFamily(QStringLiteral("MS Shell Dlg 2"));
        font.setPointSize(18);
        font.setBold(true);
        font.setItalic(false);
        font.setWeight(75);
        label_7->setFont(font);
        stackedWidget->addWidget(loginPage);
        signupPage = new QWidget();
        signupPage->setObjectName(QStringLiteral("signupPage"));
        label_8 = new QLabel(signupPage);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setGeometry(QRect(100, 10, 191, 51));
        label_8->setFont(font);
        formLayoutWidget_2 = new QWidget(signupPage);
        formLayoutWidget_2->setObjectName(QStringLiteral("formLayoutWidget_2"));
        formLayoutWidget_2->setGeometry(QRect(10, 70, 191, 151));
        formLayout_2 = new QFormLayout(formLayoutWidget_2);
        formLayout_2->setObjectName(QStringLiteral("formLayout_2"));
        formLayout_2->setContentsMargins(0, 0, 0, 0);
        label_6 = new QLabel(formLayoutWidget_2);
        label_6->setObjectName(QStringLiteral("label_6"));

        formLayout_2->setWidget(2, QFormLayout::LabelRole, label_6);

        firstNameEdit = new QLineEdit(formLayoutWidget_2);
        firstNameEdit->setObjectName(QStringLiteral("firstNameEdit"));
        firstNameEdit->setEnabled(true);

        formLayout_2->setWidget(2, QFormLayout::FieldRole, firstNameEdit);

        label_5 = new QLabel(formLayoutWidget_2);
        label_5->setObjectName(QStringLiteral("label_5"));

        formLayout_2->setWidget(3, QFormLayout::LabelRole, label_5);

        lastNameEdit = new QLineEdit(formLayoutWidget_2);
        lastNameEdit->setObjectName(QStringLiteral("lastNameEdit"));
        lastNameEdit->setEnabled(true);

        formLayout_2->setWidget(3, QFormLayout::FieldRole, lastNameEdit);

        label_3 = new QLabel(formLayoutWidget_2);
        label_3->setObjectName(QStringLiteral("label_3"));

        formLayout_2->setWidget(4, QFormLayout::LabelRole, label_3);

        userNameEditSignup = new QLineEdit(formLayoutWidget_2);
        userNameEditSignup->setObjectName(QStringLiteral("userNameEditSignup"));
        userNameEditSignup->setEnabled(true);

        formLayout_2->setWidget(4, QFormLayout::FieldRole, userNameEditSignup);

        label_4 = new QLabel(formLayoutWidget_2);
        label_4->setObjectName(QStringLiteral("label_4"));

        formLayout_2->setWidget(5, QFormLayout::LabelRole, label_4);

        passwordEditSignup = new QLineEdit(formLayoutWidget_2);
        passwordEditSignup->setObjectName(QStringLiteral("passwordEditSignup"));
        passwordEditSignup->setEchoMode(QLineEdit::Password);

        formLayout_2->setWidget(5, QFormLayout::FieldRole, passwordEditSignup);

        typeLabel = new QLabel(formLayoutWidget_2);
        typeLabel->setObjectName(QStringLiteral("typeLabel"));

        formLayout_2->setWidget(6, QFormLayout::LabelRole, typeLabel);

        accountType = new QComboBox(formLayoutWidget_2);
        accountType->setObjectName(QStringLiteral("accountType"));

        formLayout_2->setWidget(6, QFormLayout::FieldRole, accountType);

        label_10 = new QLabel(formLayoutWidget_2);
        label_10->setObjectName(QStringLiteral("label_10"));
        QFont font1;
        font1.setBold(true);
        font1.setWeight(75);
        label_10->setFont(font1);

        formLayout_2->setWidget(1, QFormLayout::LabelRole, label_10);

        formLayoutWidget_5 = new QWidget(signupPage);
        formLayoutWidget_5->setObjectName(QStringLiteral("formLayoutWidget_5"));
        formLayoutWidget_5->setGeometry(QRect(210, 70, 171, 151));
        formLayout_5 = new QFormLayout(formLayoutWidget_5);
        formLayout_5->setObjectName(QStringLiteral("formLayout_5"));
        formLayout_5->setContentsMargins(0, 0, 0, 0);
        label_22 = new QLabel(formLayoutWidget_5);
        label_22->setObjectName(QStringLiteral("label_22"));

        formLayout_5->setWidget(2, QFormLayout::LabelRole, label_22);

        nameEdit = new QLineEdit(formLayoutWidget_5);
        nameEdit->setObjectName(QStringLiteral("nameEdit"));
        nameEdit->setEnabled(true);

        formLayout_5->setWidget(2, QFormLayout::FieldRole, nameEdit);

        label_19 = new QLabel(formLayoutWidget_5);
        label_19->setObjectName(QStringLiteral("label_19"));

        formLayout_5->setWidget(3, QFormLayout::LabelRole, label_19);

        streetEdit = new QLineEdit(formLayoutWidget_5);
        streetEdit->setObjectName(QStringLiteral("streetEdit"));
        streetEdit->setEnabled(true);

        formLayout_5->setWidget(3, QFormLayout::FieldRole, streetEdit);

        label_20 = new QLabel(formLayoutWidget_5);
        label_20->setObjectName(QStringLiteral("label_20"));

        formLayout_5->setWidget(4, QFormLayout::LabelRole, label_20);

        cityEdit = new QLineEdit(formLayoutWidget_5);
        cityEdit->setObjectName(QStringLiteral("cityEdit"));
        cityEdit->setEnabled(true);

        formLayout_5->setWidget(4, QFormLayout::FieldRole, cityEdit);

        label_21 = new QLabel(formLayoutWidget_5);
        label_21->setObjectName(QStringLiteral("label_21"));

        formLayout_5->setWidget(5, QFormLayout::LabelRole, label_21);

        postcodeEdit = new QLineEdit(formLayoutWidget_5);
        postcodeEdit->setObjectName(QStringLiteral("postcodeEdit"));

        formLayout_5->setWidget(5, QFormLayout::FieldRole, postcodeEdit);

        typeLabel_3 = new QLabel(formLayoutWidget_5);
        typeLabel_3->setObjectName(QStringLiteral("typeLabel_3"));

        formLayout_5->setWidget(6, QFormLayout::LabelRole, typeLabel_3);

        facilityType = new QComboBox(formLayoutWidget_5);
        facilityType->setObjectName(QStringLiteral("facilityType"));

        formLayout_5->setWidget(6, QFormLayout::FieldRole, facilityType);

        label_11 = new QLabel(formLayoutWidget_5);
        label_11->setObjectName(QStringLiteral("label_11"));
        label_11->setFont(font1);

        formLayout_5->setWidget(1, QFormLayout::LabelRole, label_11);

        createAccountBtn = new QPushButton(signupPage);
        createAccountBtn->setObjectName(QStringLiteral("createAccountBtn"));
        createAccountBtn->setGeometry(QRect(10, 250, 361, 23));
        errorLabel = new QLabel(signupPage);
        errorLabel->setObjectName(QStringLiteral("errorLabel"));
        errorLabel->setGeometry(QRect(10, 230, 361, 16));
        errorLabel->setAlignment(Qt::AlignCenter);
        backToLogin = new QPushButton(signupPage);
        backToLogin->setObjectName(QStringLiteral("backToLogin"));
        backToLogin->setGeometry(QRect(10, 280, 361, 23));
        stackedWidget->addWidget(signupPage);

        retranslateUi(Login);

        stackedWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(Login);
    } // setupUi

    void retranslateUi(QWidget *Login)
    {
        Login->setWindowTitle(QApplication::translate("Login", "Login", 0));
        loginStatusLabel->setText(QString());
        label->setText(QApplication::translate("Login", "Username", 0));
        label_2->setText(QApplication::translate("Login", "Password", 0));
        label_9->setText(QApplication::translate("Login", "Type", 0));
        comboBox->clear();
        comboBox->insertItems(0, QStringList()
         << QApplication::translate("Login", "GP", 0)
         << QApplication::translate("Login", "Specialist", 0)
        );
        loginBtn->setText(QApplication::translate("Login", "Login", 0));
        signupBtn->setText(QApplication::translate("Login", "Create Account", 0));
        label_7->setText(QApplication::translate("Login", "Login", 0));
        label_8->setText(QApplication::translate("Login", "Create Account", 0));
        label_6->setText(QApplication::translate("Login", "First Name", 0));
        label_5->setText(QApplication::translate("Login", "Last Name", 0));
        label_3->setText(QApplication::translate("Login", "Username", 0));
        label_4->setText(QApplication::translate("Login", "Password", 0));
        typeLabel->setText(QApplication::translate("Login", "Type", 0));
        accountType->clear();
        accountType->insertItems(0, QStringList()
         << QApplication::translate("Login", "GP", 0)
         << QApplication::translate("Login", "Specialist", 0)
        );
        label_10->setText(QApplication::translate("Login", "Account Details", 0));
        label_22->setText(QApplication::translate("Login", "Facility Name", 0));
        label_19->setText(QApplication::translate("Login", "Street", 0));
        label_20->setText(QApplication::translate("Login", "City", 0));
        label_21->setText(QApplication::translate("Login", "PostCode", 0));
        typeLabel_3->setText(QApplication::translate("Login", "Type", 0));
        facilityType->clear();
        facilityType->insertItems(0, QStringList()
         << QApplication::translate("Login", "Clinic", 0)
         << QApplication::translate("Login", "Hospital", 0)
        );
        label_11->setText(QApplication::translate("Login", "Facility Details", 0));
        createAccountBtn->setText(QApplication::translate("Login", "Create", 0));
        errorLabel->setText(QString());
        backToLogin->setText(QApplication::translate("Login", "Back", 0));
    } // retranslateUi

};

namespace Ui {
    class Login: public Ui_Login {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LOGIN_H
