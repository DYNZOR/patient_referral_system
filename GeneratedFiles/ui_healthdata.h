/********************************************************************************
** Form generated from reading UI file 'healthdata.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_HEALTHDATA_H
#define UI_HEALTHDATA_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_HealthDataDialog
{
public:
    QWidget *formLayoutWidget;
    QFormLayout *formLayout;
    QLabel *label_7;
    QSpinBox *bloodPressureSpinbox;
    QSpinBox *cholesterolSpinBox;
    QLabel *label_10;
    QPushButton *closeHealthData;
    QPushButton *updateHealthDatabtn;

    void setupUi(QWidget *HealthDataDialog)
    {
        if (HealthDataDialog->objectName().isEmpty())
            HealthDataDialog->setObjectName(QStringLiteral("HealthDataDialog"));
        HealthDataDialog->resize(254, 135);
        formLayoutWidget = new QWidget(HealthDataDialog);
        formLayoutWidget->setObjectName(QStringLiteral("formLayoutWidget"));
        formLayoutWidget->setGeometry(QRect(20, 20, 214, 106));
        formLayout = new QFormLayout(formLayoutWidget);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        formLayout->setContentsMargins(0, 0, 0, 0);
        label_7 = new QLabel(formLayoutWidget);
        label_7->setObjectName(QStringLiteral("label_7"));

        formLayout->setWidget(0, QFormLayout::LabelRole, label_7);

        bloodPressureSpinbox = new QSpinBox(formLayoutWidget);
        bloodPressureSpinbox->setObjectName(QStringLiteral("bloodPressureSpinbox"));
        bloodPressureSpinbox->setMinimum(60);
        bloodPressureSpinbox->setMaximum(300);

        formLayout->setWidget(0, QFormLayout::FieldRole, bloodPressureSpinbox);

        cholesterolSpinBox = new QSpinBox(formLayoutWidget);
        cholesterolSpinBox->setObjectName(QStringLiteral("cholesterolSpinBox"));
        cholesterolSpinBox->setMaximum(500);

        formLayout->setWidget(1, QFormLayout::FieldRole, cholesterolSpinBox);

        label_10 = new QLabel(formLayoutWidget);
        label_10->setObjectName(QStringLiteral("label_10"));

        formLayout->setWidget(1, QFormLayout::LabelRole, label_10);

        closeHealthData = new QPushButton(formLayoutWidget);
        closeHealthData->setObjectName(QStringLiteral("closeHealthData"));

        formLayout->setWidget(3, QFormLayout::SpanningRole, closeHealthData);

        updateHealthDatabtn = new QPushButton(formLayoutWidget);
        updateHealthDatabtn->setObjectName(QStringLiteral("updateHealthDatabtn"));

        formLayout->setWidget(2, QFormLayout::SpanningRole, updateHealthDatabtn);


        retranslateUi(HealthDataDialog);

        QMetaObject::connectSlotsByName(HealthDataDialog);
    } // setupUi

    void retranslateUi(QWidget *HealthDataDialog)
    {
        HealthDataDialog->setWindowTitle(QApplication::translate("HealthDataDialog", "Health Data", 0));
        label_7->setText(QApplication::translate("HealthDataDialog", "Blood Pressure (Systolic)", 0));
        label_10->setText(QApplication::translate("HealthDataDialog", "Cholesterol (LPDL)", 0));
        closeHealthData->setText(QApplication::translate("HealthDataDialog", "Close", 0));
        updateHealthDatabtn->setText(QApplication::translate("HealthDataDialog", "Update", 0));
    } // retranslateUi

};

namespace Ui {
    class HealthDataDialog: public Ui_HealthDataDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_HEALTHDATA_H
