/********************************************************************************
** Form generated from reading UI file 'MainScreen.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINSCREEN_H
#define UI_MAINSCREEN_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainScreen
{
public:
    QPushButton *logoutBtn;
    QPushButton *viewPatientsBtn;
    QPushButton *viewDetailsBtn;
    QPushButton *viewReferralsBtn;
    QLabel *welcomeLabel;
    QLabel *label_2;

    void setupUi(QWidget *MainScreen)
    {
        if (MainScreen->objectName().isEmpty())
            MainScreen->setObjectName(QStringLiteral("MainScreen"));
        MainScreen->resize(789, 636);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainScreen->sizePolicy().hasHeightForWidth());
        MainScreen->setSizePolicy(sizePolicy);
        logoutBtn = new QPushButton(MainScreen);
        logoutBtn->setObjectName(QStringLiteral("logoutBtn"));
        logoutBtn->setGeometry(QRect(130, 410, 541, 61));
        viewPatientsBtn = new QPushButton(MainScreen);
        viewPatientsBtn->setObjectName(QStringLiteral("viewPatientsBtn"));
        viewPatientsBtn->setGeometry(QRect(310, 170, 181, 191));
        viewPatientsBtn->setMaximumSize(QSize(200, 300));
        viewDetailsBtn = new QPushButton(MainScreen);
        viewDetailsBtn->setObjectName(QStringLiteral("viewDetailsBtn"));
        viewDetailsBtn->setGeometry(QRect(500, 170, 171, 191));
        viewDetailsBtn->setMaximumSize(QSize(30000, 16777215));
        viewReferralsBtn = new QPushButton(MainScreen);
        viewReferralsBtn->setObjectName(QStringLiteral("viewReferralsBtn"));
        viewReferralsBtn->setGeometry(QRect(130, 170, 171, 191));
        welcomeLabel = new QLabel(MainScreen);
        welcomeLabel->setObjectName(QStringLiteral("welcomeLabel"));
        welcomeLabel->setGeometry(QRect(130, 90, 541, 41));
        QFont font;
        font.setPointSize(14);
        font.setBold(true);
        font.setWeight(75);
        welcomeLabel->setFont(font);
        welcomeLabel->setAlignment(Qt::AlignCenter);
        label_2 = new QLabel(MainScreen);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(130, 20, 541, 41));
        QFont font1;
        font1.setPointSize(18);
        font1.setBold(true);
        font1.setWeight(75);
        label_2->setFont(font1);
        label_2->setAlignment(Qt::AlignCenter);

        retranslateUi(MainScreen);

        QMetaObject::connectSlotsByName(MainScreen);
    } // setupUi

    void retranslateUi(QWidget *MainScreen)
    {
        MainScreen->setWindowTitle(QApplication::translate("MainScreen", "Main Screen", 0));
        logoutBtn->setText(QApplication::translate("MainScreen", "Log Out", 0));
        viewPatientsBtn->setText(QApplication::translate("MainScreen", "View Patients", 0));
        viewDetailsBtn->setText(QApplication::translate("MainScreen", "View Details", 0));
        viewReferralsBtn->setText(QApplication::translate("MainScreen", "View Referrals", 0));
        welcomeLabel->setText(QApplication::translate("MainScreen", "Welcome Dr. ", 0));
        label_2->setText(QApplication::translate("MainScreen", "Main Screen", 0));
    } // retranslateUi

};

namespace Ui {
    class MainScreen: public Ui_MainScreen {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINSCREEN_H
