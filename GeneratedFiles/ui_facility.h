/********************************************************************************
** Form generated from reading UI file 'facility.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FACILITY_H
#define UI_FACILITY_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_FacilityDialog
{
public:
    QWidget *formLayoutWidget_2;
    QFormLayout *formLayout_2;
    QLabel *label_6;
    QLineEdit *nameEdit;
    QLabel *label_5;
    QLineEdit *streetEdit;
    QLabel *label_3;
    QLineEdit *cityEdit;
    QLabel *label_4;
    QLineEdit *postcodeEdit;
    QLabel *typeLabel;
    QComboBox *facilityType;
    QPushButton *addFacilityBtn;

    void setupUi(QWidget *FacilityDialog)
    {
        if (FacilityDialog->objectName().isEmpty())
            FacilityDialog->setObjectName(QStringLiteral("FacilityDialog"));
        FacilityDialog->resize(232, 180);
        formLayoutWidget_2 = new QWidget(FacilityDialog);
        formLayoutWidget_2->setObjectName(QStringLiteral("formLayoutWidget_2"));
        formLayoutWidget_2->setGeometry(QRect(10, 10, 211, 161));
        formLayout_2 = new QFormLayout(formLayoutWidget_2);
        formLayout_2->setObjectName(QStringLiteral("formLayout_2"));
        formLayout_2->setContentsMargins(0, 0, 0, 0);
        label_6 = new QLabel(formLayoutWidget_2);
        label_6->setObjectName(QStringLiteral("label_6"));

        formLayout_2->setWidget(1, QFormLayout::LabelRole, label_6);

        nameEdit = new QLineEdit(formLayoutWidget_2);
        nameEdit->setObjectName(QStringLiteral("nameEdit"));
        nameEdit->setEnabled(true);

        formLayout_2->setWidget(1, QFormLayout::FieldRole, nameEdit);

        label_5 = new QLabel(formLayoutWidget_2);
        label_5->setObjectName(QStringLiteral("label_5"));

        formLayout_2->setWidget(2, QFormLayout::LabelRole, label_5);

        streetEdit = new QLineEdit(formLayoutWidget_2);
        streetEdit->setObjectName(QStringLiteral("streetEdit"));
        streetEdit->setEnabled(true);

        formLayout_2->setWidget(2, QFormLayout::FieldRole, streetEdit);

        label_3 = new QLabel(formLayoutWidget_2);
        label_3->setObjectName(QStringLiteral("label_3"));

        formLayout_2->setWidget(3, QFormLayout::LabelRole, label_3);

        cityEdit = new QLineEdit(formLayoutWidget_2);
        cityEdit->setObjectName(QStringLiteral("cityEdit"));
        cityEdit->setEnabled(true);

        formLayout_2->setWidget(3, QFormLayout::FieldRole, cityEdit);

        label_4 = new QLabel(formLayoutWidget_2);
        label_4->setObjectName(QStringLiteral("label_4"));

        formLayout_2->setWidget(4, QFormLayout::LabelRole, label_4);

        postcodeEdit = new QLineEdit(formLayoutWidget_2);
        postcodeEdit->setObjectName(QStringLiteral("postcodeEdit"));

        formLayout_2->setWidget(4, QFormLayout::FieldRole, postcodeEdit);

        typeLabel = new QLabel(formLayoutWidget_2);
        typeLabel->setObjectName(QStringLiteral("typeLabel"));

        formLayout_2->setWidget(5, QFormLayout::LabelRole, typeLabel);

        facilityType = new QComboBox(formLayoutWidget_2);
        facilityType->setObjectName(QStringLiteral("facilityType"));

        formLayout_2->setWidget(5, QFormLayout::FieldRole, facilityType);

        addFacilityBtn = new QPushButton(formLayoutWidget_2);
        addFacilityBtn->setObjectName(QStringLiteral("addFacilityBtn"));

        formLayout_2->setWidget(6, QFormLayout::SpanningRole, addFacilityBtn);


        retranslateUi(FacilityDialog);

        QMetaObject::connectSlotsByName(FacilityDialog);
    } // setupUi

    void retranslateUi(QWidget *FacilityDialog)
    {
        FacilityDialog->setWindowTitle(QApplication::translate("FacilityDialog", "Facility", 0));
        label_6->setText(QApplication::translate("FacilityDialog", "Facility Name", 0));
        label_5->setText(QApplication::translate("FacilityDialog", "Street", 0));
        label_3->setText(QApplication::translate("FacilityDialog", "City", 0));
        label_4->setText(QApplication::translate("FacilityDialog", "PostCode", 0));
        typeLabel->setText(QApplication::translate("FacilityDialog", "Type", 0));
        facilityType->clear();
        facilityType->insertItems(0, QStringList()
         << QApplication::translate("FacilityDialog", "Clinic", 0)
         << QApplication::translate("FacilityDialog", "Hospital", 0)
        );
        addFacilityBtn->setText(QApplication::translate("FacilityDialog", "Add", 0));
    } // retranslateUi

};

namespace Ui {
    class FacilityDialog: public Ui_FacilityDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FACILITY_H
