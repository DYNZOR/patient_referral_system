/********************************************************************************
** Form generated from reading UI file 'viewspecialists.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_VIEWSPECIALISTS_H
#define UI_VIEWSPECIALISTS_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QTableView>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ViewSpecialists
{
public:
    QTableView *specialistsTableView;

    void setupUi(QWidget *ViewSpecialists)
    {
        if (ViewSpecialists->objectName().isEmpty())
            ViewSpecialists->setObjectName(QStringLiteral("ViewSpecialists"));
        ViewSpecialists->resize(411, 330);
        specialistsTableView = new QTableView(ViewSpecialists);
        specialistsTableView->setObjectName(QStringLiteral("specialistsTableView"));
        specialistsTableView->setGeometry(QRect(10, 10, 391, 311));

        retranslateUi(ViewSpecialists);

        QMetaObject::connectSlotsByName(ViewSpecialists);
    } // setupUi

    void retranslateUi(QWidget *ViewSpecialists)
    {
        ViewSpecialists->setWindowTitle(QApplication::translate("ViewSpecialists", "Choose Specialist", 0));
    } // retranslateUi

};

namespace Ui {
    class ViewSpecialists: public Ui_ViewSpecialists {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_VIEWSPECIALISTS_H
