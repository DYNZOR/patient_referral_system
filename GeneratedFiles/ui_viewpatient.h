/********************************************************************************
** Form generated from reading UI file 'viewpatient.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_VIEWPATIENT_H
#define UI_VIEWPATIENT_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ViewPatientDialog
{
public:

    void setupUi(QWidget *ViewPatientDialog)
    {
        if (ViewPatientDialog->objectName().isEmpty())
            ViewPatientDialog->setObjectName(QStringLiteral("ViewPatientDialog"));
        ViewPatientDialog->resize(420, 291);

        retranslateUi(ViewPatientDialog);

        QMetaObject::connectSlotsByName(ViewPatientDialog);
    } // setupUi

    void retranslateUi(QWidget *ViewPatientDialog)
    {
        ViewPatientDialog->setWindowTitle(QApplication::translate("ViewPatientDialog", "View Patient", 0));
    } // retranslateUi

};

namespace Ui {
    class ViewPatientDialog: public Ui_ViewPatientDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_VIEWPATIENT_H
