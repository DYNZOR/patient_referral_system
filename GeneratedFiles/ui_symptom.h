/********************************************************************************
** Form generated from reading UI file 'symptom.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SYMPTOM_H
#define UI_SYMPTOM_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_SymptomDialog
{
public:
    QWidget *formLayoutWidget;
    QFormLayout *formLayout_2;
    QLabel *label_6;
    QLineEdit *symptomName;
    QLabel *label_9;
    QComboBox *locationComboBox;
    QLabel *label_7;
    QSpinBox *discomfortSpinBox;
    QLabel *label_10;
    QSpinBox *durationSpinBox;
    QPushButton *addSymptomBtnDialog;
    QPushButton *deleteBtn;

    void setupUi(QWidget *SymptomDialog)
    {
        if (SymptomDialog->objectName().isEmpty())
            SymptomDialog->setObjectName(QStringLiteral("SymptomDialog"));
        SymptomDialog->resize(223, 175);
        formLayoutWidget = new QWidget(SymptomDialog);
        formLayoutWidget->setObjectName(QStringLiteral("formLayoutWidget"));
        formLayoutWidget->setGeometry(QRect(10, 10, 203, 184));
        formLayout_2 = new QFormLayout(formLayoutWidget);
        formLayout_2->setObjectName(QStringLiteral("formLayout_2"));
        formLayout_2->setContentsMargins(0, 0, 0, 0);
        label_6 = new QLabel(formLayoutWidget);
        label_6->setObjectName(QStringLiteral("label_6"));

        formLayout_2->setWidget(0, QFormLayout::LabelRole, label_6);

        symptomName = new QLineEdit(formLayoutWidget);
        symptomName->setObjectName(QStringLiteral("symptomName"));

        formLayout_2->setWidget(0, QFormLayout::FieldRole, symptomName);

        label_9 = new QLabel(formLayoutWidget);
        label_9->setObjectName(QStringLiteral("label_9"));

        formLayout_2->setWidget(1, QFormLayout::LabelRole, label_9);

        locationComboBox = new QComboBox(formLayoutWidget);
        locationComboBox->setObjectName(QStringLiteral("locationComboBox"));

        formLayout_2->setWidget(1, QFormLayout::FieldRole, locationComboBox);

        label_7 = new QLabel(formLayoutWidget);
        label_7->setObjectName(QStringLiteral("label_7"));

        formLayout_2->setWidget(2, QFormLayout::LabelRole, label_7);

        discomfortSpinBox = new QSpinBox(formLayoutWidget);
        discomfortSpinBox->setObjectName(QStringLiteral("discomfortSpinBox"));
        discomfortSpinBox->setMaximum(10);

        formLayout_2->setWidget(2, QFormLayout::FieldRole, discomfortSpinBox);

        label_10 = new QLabel(formLayoutWidget);
        label_10->setObjectName(QStringLiteral("label_10"));

        formLayout_2->setWidget(3, QFormLayout::LabelRole, label_10);

        durationSpinBox = new QSpinBox(formLayoutWidget);
        durationSpinBox->setObjectName(QStringLiteral("durationSpinBox"));
        durationSpinBox->setMaximum(365);

        formLayout_2->setWidget(3, QFormLayout::FieldRole, durationSpinBox);

        addSymptomBtnDialog = new QPushButton(formLayoutWidget);
        addSymptomBtnDialog->setObjectName(QStringLiteral("addSymptomBtnDialog"));

        formLayout_2->setWidget(4, QFormLayout::SpanningRole, addSymptomBtnDialog);

        deleteBtn = new QPushButton(formLayoutWidget);
        deleteBtn->setObjectName(QStringLiteral("deleteBtn"));

        formLayout_2->setWidget(5, QFormLayout::SpanningRole, deleteBtn);


        retranslateUi(SymptomDialog);

        QMetaObject::connectSlotsByName(SymptomDialog);
    } // setupUi

    void retranslateUi(QWidget *SymptomDialog)
    {
        SymptomDialog->setWindowTitle(QApplication::translate("SymptomDialog", "Symptom", 0));
        label_6->setText(QApplication::translate("SymptomDialog", "Symptom", 0));
        label_9->setText(QApplication::translate("SymptomDialog", "Body Location", 0));
        locationComboBox->clear();
        locationComboBox->insertItems(0, QStringList()
         << QApplication::translate("SymptomDialog", "Head", 0)
         << QApplication::translate("SymptomDialog", "Neck", 0)
         << QApplication::translate("SymptomDialog", "Torso", 0)
         << QApplication::translate("SymptomDialog", "Arm", 0)
         << QApplication::translate("SymptomDialog", "Palm", 0)
         << QApplication::translate("SymptomDialog", "Fingers", 0)
         << QApplication::translate("SymptomDialog", "Abdomen", 0)
         << QApplication::translate("SymptomDialog", "Leg", 0)
         << QApplication::translate("SymptomDialog", "Foot", 0)
         << QApplication::translate("SymptomDialog", "Toes", 0)
         << QApplication::translate("SymptomDialog", "Ankle", 0)
         << QApplication::translate("SymptomDialog", "Shin", 0)
         << QApplication::translate("SymptomDialog", "Knee", 0)
         << QApplication::translate("SymptomDialog", "Thigh", 0)
         << QApplication::translate("SymptomDialog", "Hamstring", 0)
         << QApplication::translate("SymptomDialog", "Calf", 0)
         << QApplication::translate("SymptomDialog", "Buttock", 0)
         << QApplication::translate("SymptomDialog", "Lower Spine", 0)
         << QApplication::translate("SymptomDialog", "Upper Spine", 0)
        );
        label_7->setText(QApplication::translate("SymptomDialog", "Discomfort (1-10)", 0));
        label_10->setText(QApplication::translate("SymptomDialog", "Duration (Days)", 0));
        addSymptomBtnDialog->setText(QApplication::translate("SymptomDialog", "Add", 0));
        deleteBtn->setText(QApplication::translate("SymptomDialog", "Delete", 0));
    } // retranslateUi

};

namespace Ui {
    class SymptomDialog: public Ui_SymptomDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SYMPTOM_H
