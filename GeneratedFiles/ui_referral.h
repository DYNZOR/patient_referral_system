/********************************************************************************
** Form generated from reading UI file 'referral.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_REFERRAL_H
#define UI_REFERRAL_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDateEdit>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QTableView>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Referral
{
public:
    QStackedWidget *stackedWidget;
    QWidget *refer_patient_page;
    QTableView *patientTableView;
    QPushButton *newPatientReferralBtn;
    QWidget *referral_page;
    QWidget *formLayoutWidget_12;
    QFormLayout *gpDetailsForm_2;
    QLabel *label_46;
    QLabel *label_47;
    QLabel *label_76;
    QLineEdit *gpLastName;
    QLabel *label_48;
    QLineEdit *gpFacility;
    QPushButton *pushButton_2;
    QLineEdit *gpFirstName;
    QLabel *label_4;
    QLineEdit *gpIDEdit;
    QWidget *formLayoutWidget_9;
    QFormLayout *patientDetailsForm1_2;
    QLabel *label_30;
    QLabel *label_41;
    QLabel *label_2;
    QLineEdit *patientIdTextEdit;
    QLabel *label_31;
    QLineEdit *patientFirstName;
    QLabel *label_32;
    QLineEdit *patientLastName;
    QLabel *label_33;
    QLabel *label_34;
    QComboBox *genderComboBox;
    QSpinBox *ageSpinbox;
    QPushButton *healthDataBtn;
    QWidget *formLayoutWidget_7;
    QFormLayout *healthDetails_2;
    QTextEdit *reasonForReferral_2;
    QLabel *label_71;
    QWidget *horizontalLayoutWidget_2;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *submitButton;
    QPushButton *saveButton;
    QPushButton *backButton;
    QWidget *gridLayoutWidget_2;
    QGridLayout *healthDetailsGrid_2;
    QLabel *label_72;
    QPushButton *addAllergieBtn;
    QLabel *label_74;
    QLabel *label_75;
    QLabel *label_69;
    QLabel *label_73;
    QPushButton *addTreatmentBtn;
    QLabel *label_68;
    QPushButton *addSymptomBtn;
    QPushButton *addConditionBtn;
    QPushButton *refreshListsBtn;
    QTableView *symptomsTableView;
    QTableView *currentConditionTableView;
    QTableView *pastConditionsTableView;
    QTableView *familyConditionTableView;
    QTableView *treatmentsTableView;
    QTableView *allergiesTableView;
    QPushButton *lifestyleData;
    QWidget *formLayoutWidget_10;
    QFormLayout *referralDetailsForm_2;
    QLabel *label_37;
    QLabel *label_38;
    QLineEdit *referralID;
    QLabel *label_39;
    QDateEdit *referralDate;
    QLabel *label_9;
    QLineEdit *referralStatusEdit;
    QPushButton *diagnosisBtn;
    QWidget *formLayoutWidget_8;
    QFormLayout *specialistDetailsForm_2;
    QLabel *label_29;
    QLabel *label_5;
    QLineEdit *specialistIDEdit;
    QLabel *label_25;
    QLineEdit *specialistFirstName_2;
    QLabel *label_26;
    QLineEdit *specialistLastName_2;
    QLabel *label_28;
    QLineEdit *specialistFacility_2;
    QLabel *label_27;
    QComboBox *comboBox_2;
    QPushButton *chooseSpecialistBtn;
    QLabel *specialistErrorLabel;
    QPushButton *refreshSpecialist;
    QWidget *formLayoutWidget_2;
    QFormLayout *formLayout_2;
    QLabel *label_40;
    QLineEdit *streetText;
    QLabel *label_101;
    QLineEdit *cityText;
    QLineEdit *postcode;
    QLineEdit *phoneNumberTex;
    QLineEdit *emailText;
    QLabel *label_102;
    QLabel *label_103;
    QLabel *label_104;
    QLabel *label_100;
    QLabel *label_105;
    QLabel *label_98;
    QLabel *label_8;
    QWidget *submit_referral_page;
    QPushButton *sendReferralBtn;
    QPushButton *evaluateEmergencyBtn;
    QWidget *formLayoutWidget;
    QFormLayout *formLayout;
    QLabel *label;
    QLabel *label_3;
    QLabel *symptomSeverityResultLabel;
    QLabel *label_7;
    QLabel *healthResultLabel;
    QLabel *symptomSeverityResultLabel_3;
    QLabel *priorityLabelResult;
    QLabel *referralEmergencyResultLabel;
    QLabel *label_6;

    void setupUi(QWidget *Referral)
    {
        if (Referral->objectName().isEmpty())
            Referral->setObjectName(QStringLiteral("Referral"));
        Referral->resize(789, 637);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(Referral->sizePolicy().hasHeightForWidth());
        Referral->setSizePolicy(sizePolicy);
        Referral->setMaximumSize(QSize(789, 637));
        stackedWidget = new QStackedWidget(Referral);
        stackedWidget->setObjectName(QStringLiteral("stackedWidget"));
        stackedWidget->setEnabled(true);
        stackedWidget->setGeometry(QRect(0, 0, 791, 641));
        stackedWidget->setLineWidth(1);
        refer_patient_page = new QWidget();
        refer_patient_page->setObjectName(QStringLiteral("refer_patient_page"));
        patientTableView = new QTableView(refer_patient_page);
        patientTableView->setObjectName(QStringLiteral("patientTableView"));
        patientTableView->setGeometry(QRect(30, 60, 731, 401));
        newPatientReferralBtn = new QPushButton(refer_patient_page);
        newPatientReferralBtn->setObjectName(QStringLiteral("newPatientReferralBtn"));
        newPatientReferralBtn->setGeometry(QRect(30, 480, 731, 71));
        stackedWidget->addWidget(refer_patient_page);
        referral_page = new QWidget();
        referral_page->setObjectName(QStringLiteral("referral_page"));
        formLayoutWidget_12 = new QWidget(referral_page);
        formLayoutWidget_12->setObjectName(QStringLiteral("formLayoutWidget_12"));
        formLayoutWidget_12->setGeometry(QRect(410, 10, 151, 151));
        gpDetailsForm_2 = new QFormLayout(formLayoutWidget_12);
        gpDetailsForm_2->setObjectName(QStringLiteral("gpDetailsForm_2"));
        gpDetailsForm_2->setContentsMargins(0, 0, 0, 0);
        label_46 = new QLabel(formLayoutWidget_12);
        label_46->setObjectName(QStringLiteral("label_46"));
        QFont font;
        font.setBold(true);
        font.setWeight(75);
        label_46->setFont(font);

        gpDetailsForm_2->setWidget(0, QFormLayout::LabelRole, label_46);

        label_47 = new QLabel(formLayoutWidget_12);
        label_47->setObjectName(QStringLiteral("label_47"));

        gpDetailsForm_2->setWidget(2, QFormLayout::LabelRole, label_47);

        label_76 = new QLabel(formLayoutWidget_12);
        label_76->setObjectName(QStringLiteral("label_76"));

        gpDetailsForm_2->setWidget(3, QFormLayout::LabelRole, label_76);

        gpLastName = new QLineEdit(formLayoutWidget_12);
        gpLastName->setObjectName(QStringLiteral("gpLastName"));

        gpDetailsForm_2->setWidget(3, QFormLayout::FieldRole, gpLastName);

        label_48 = new QLabel(formLayoutWidget_12);
        label_48->setObjectName(QStringLiteral("label_48"));

        gpDetailsForm_2->setWidget(4, QFormLayout::LabelRole, label_48);

        gpFacility = new QLineEdit(formLayoutWidget_12);
        gpFacility->setObjectName(QStringLiteral("gpFacility"));

        gpDetailsForm_2->setWidget(4, QFormLayout::FieldRole, gpFacility);

        pushButton_2 = new QPushButton(formLayoutWidget_12);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));

        gpDetailsForm_2->setWidget(5, QFormLayout::SpanningRole, pushButton_2);

        gpFirstName = new QLineEdit(formLayoutWidget_12);
        gpFirstName->setObjectName(QStringLiteral("gpFirstName"));

        gpDetailsForm_2->setWidget(2, QFormLayout::FieldRole, gpFirstName);

        label_4 = new QLabel(formLayoutWidget_12);
        label_4->setObjectName(QStringLiteral("label_4"));

        gpDetailsForm_2->setWidget(1, QFormLayout::LabelRole, label_4);

        gpIDEdit = new QLineEdit(formLayoutWidget_12);
        gpIDEdit->setObjectName(QStringLiteral("gpIDEdit"));
        gpIDEdit->setReadOnly(true);

        gpDetailsForm_2->setWidget(1, QFormLayout::FieldRole, gpIDEdit);

        formLayoutWidget_9 = new QWidget(referral_page);
        formLayoutWidget_9->setObjectName(QStringLiteral("formLayoutWidget_9"));
        formLayoutWidget_9->setGeometry(QRect(10, 120, 191, 151));
        patientDetailsForm1_2 = new QFormLayout(formLayoutWidget_9);
        patientDetailsForm1_2->setObjectName(QStringLiteral("patientDetailsForm1_2"));
        patientDetailsForm1_2->setContentsMargins(0, 0, 0, 0);
        label_30 = new QLabel(formLayoutWidget_9);
        label_30->setObjectName(QStringLiteral("label_30"));
        label_30->setFont(font);

        patientDetailsForm1_2->setWidget(0, QFormLayout::LabelRole, label_30);

        label_41 = new QLabel(formLayoutWidget_9);
        label_41->setObjectName(QStringLiteral("label_41"));

        patientDetailsForm1_2->setWidget(0, QFormLayout::FieldRole, label_41);

        label_2 = new QLabel(formLayoutWidget_9);
        label_2->setObjectName(QStringLiteral("label_2"));

        patientDetailsForm1_2->setWidget(1, QFormLayout::LabelRole, label_2);

        patientIdTextEdit = new QLineEdit(formLayoutWidget_9);
        patientIdTextEdit->setObjectName(QStringLiteral("patientIdTextEdit"));
        patientIdTextEdit->setReadOnly(true);

        patientDetailsForm1_2->setWidget(1, QFormLayout::FieldRole, patientIdTextEdit);

        label_31 = new QLabel(formLayoutWidget_9);
        label_31->setObjectName(QStringLiteral("label_31"));

        patientDetailsForm1_2->setWidget(2, QFormLayout::LabelRole, label_31);

        patientFirstName = new QLineEdit(formLayoutWidget_9);
        patientFirstName->setObjectName(QStringLiteral("patientFirstName"));

        patientDetailsForm1_2->setWidget(2, QFormLayout::FieldRole, patientFirstName);

        label_32 = new QLabel(formLayoutWidget_9);
        label_32->setObjectName(QStringLiteral("label_32"));

        patientDetailsForm1_2->setWidget(3, QFormLayout::LabelRole, label_32);

        patientLastName = new QLineEdit(formLayoutWidget_9);
        patientLastName->setObjectName(QStringLiteral("patientLastName"));

        patientDetailsForm1_2->setWidget(3, QFormLayout::FieldRole, patientLastName);

        label_33 = new QLabel(formLayoutWidget_9);
        label_33->setObjectName(QStringLiteral("label_33"));

        patientDetailsForm1_2->setWidget(4, QFormLayout::LabelRole, label_33);

        label_34 = new QLabel(formLayoutWidget_9);
        label_34->setObjectName(QStringLiteral("label_34"));

        patientDetailsForm1_2->setWidget(5, QFormLayout::LabelRole, label_34);

        genderComboBox = new QComboBox(formLayoutWidget_9);
        genderComboBox->setObjectName(QStringLiteral("genderComboBox"));

        patientDetailsForm1_2->setWidget(5, QFormLayout::FieldRole, genderComboBox);

        ageSpinbox = new QSpinBox(formLayoutWidget_9);
        ageSpinbox->setObjectName(QStringLiteral("ageSpinbox"));

        patientDetailsForm1_2->setWidget(4, QFormLayout::FieldRole, ageSpinbox);

        healthDataBtn = new QPushButton(referral_page);
        healthDataBtn->setObjectName(QStringLiteral("healthDataBtn"));
        healthDataBtn->setGeometry(QRect(10, 290, 171, 21));
        formLayoutWidget_7 = new QWidget(referral_page);
        formLayoutWidget_7->setObjectName(QStringLiteral("formLayoutWidget_7"));
        formLayoutWidget_7->setGeometry(QRect(10, 380, 401, 211));
        healthDetails_2 = new QFormLayout(formLayoutWidget_7);
        healthDetails_2->setObjectName(QStringLiteral("healthDetails_2"));
        healthDetails_2->setContentsMargins(0, 0, 0, 0);
        reasonForReferral_2 = new QTextEdit(formLayoutWidget_7);
        reasonForReferral_2->setObjectName(QStringLiteral("reasonForReferral_2"));

        healthDetails_2->setWidget(1, QFormLayout::SpanningRole, reasonForReferral_2);

        label_71 = new QLabel(formLayoutWidget_7);
        label_71->setObjectName(QStringLiteral("label_71"));
        label_71->setFont(font);

        healthDetails_2->setWidget(0, QFormLayout::SpanningRole, label_71);

        horizontalLayoutWidget_2 = new QWidget(referral_page);
        horizontalLayoutWidget_2->setObjectName(QStringLiteral("horizontalLayoutWidget_2"));
        horizontalLayoutWidget_2->setGeometry(QRect(100, 600, 591, 31));
        horizontalLayout_2 = new QHBoxLayout(horizontalLayoutWidget_2);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalLayout_2->setSizeConstraint(QLayout::SetMinAndMaxSize);
        horizontalLayout_2->setContentsMargins(0, 0, 0, 0);
        submitButton = new QPushButton(horizontalLayoutWidget_2);
        submitButton->setObjectName(QStringLiteral("submitButton"));

        horizontalLayout_2->addWidget(submitButton);

        saveButton = new QPushButton(horizontalLayoutWidget_2);
        saveButton->setObjectName(QStringLiteral("saveButton"));

        horizontalLayout_2->addWidget(saveButton);

        backButton = new QPushButton(horizontalLayoutWidget_2);
        backButton->setObjectName(QStringLiteral("backButton"));

        horizontalLayout_2->addWidget(backButton);

        gridLayoutWidget_2 = new QWidget(referral_page);
        gridLayoutWidget_2->setObjectName(QStringLiteral("gridLayoutWidget_2"));
        gridLayoutWidget_2->setGeometry(QRect(430, 250, 351, 341));
        healthDetailsGrid_2 = new QGridLayout(gridLayoutWidget_2);
        healthDetailsGrid_2->setObjectName(QStringLiteral("healthDetailsGrid_2"));
        healthDetailsGrid_2->setContentsMargins(0, 0, 0, 0);
        label_72 = new QLabel(gridLayoutWidget_2);
        label_72->setObjectName(QStringLiteral("label_72"));

        healthDetailsGrid_2->addWidget(label_72, 0, 0, 1, 1);

        addAllergieBtn = new QPushButton(gridLayoutWidget_2);
        addAllergieBtn->setObjectName(QStringLiteral("addAllergieBtn"));

        healthDetailsGrid_2->addWidget(addAllergieBtn, 6, 2, 1, 1);

        label_74 = new QLabel(gridLayoutWidget_2);
        label_74->setObjectName(QStringLiteral("label_74"));

        healthDetailsGrid_2->addWidget(label_74, 4, 0, 1, 1);

        label_75 = new QLabel(gridLayoutWidget_2);
        label_75->setObjectName(QStringLiteral("label_75"));

        healthDetailsGrid_2->addWidget(label_75, 4, 2, 1, 1);

        label_69 = new QLabel(gridLayoutWidget_2);
        label_69->setObjectName(QStringLiteral("label_69"));

        healthDetailsGrid_2->addWidget(label_69, 0, 2, 1, 1);

        label_73 = new QLabel(gridLayoutWidget_2);
        label_73->setObjectName(QStringLiteral("label_73"));

        healthDetailsGrid_2->addWidget(label_73, 4, 1, 1, 1);

        addTreatmentBtn = new QPushButton(gridLayoutWidget_2);
        addTreatmentBtn->setObjectName(QStringLiteral("addTreatmentBtn"));

        healthDetailsGrid_2->addWidget(addTreatmentBtn, 6, 1, 1, 1);

        label_68 = new QLabel(gridLayoutWidget_2);
        label_68->setObjectName(QStringLiteral("label_68"));

        healthDetailsGrid_2->addWidget(label_68, 0, 1, 1, 1);

        addSymptomBtn = new QPushButton(gridLayoutWidget_2);
        addSymptomBtn->setObjectName(QStringLiteral("addSymptomBtn"));

        healthDetailsGrid_2->addWidget(addSymptomBtn, 6, 0, 1, 1);

        addConditionBtn = new QPushButton(gridLayoutWidget_2);
        addConditionBtn->setObjectName(QStringLiteral("addConditionBtn"));

        healthDetailsGrid_2->addWidget(addConditionBtn, 3, 0, 1, 3);

        refreshListsBtn = new QPushButton(gridLayoutWidget_2);
        refreshListsBtn->setObjectName(QStringLiteral("refreshListsBtn"));

        healthDetailsGrid_2->addWidget(refreshListsBtn, 7, 0, 1, 3);

        symptomsTableView = new QTableView(gridLayoutWidget_2);
        symptomsTableView->setObjectName(QStringLiteral("symptomsTableView"));
        QSizePolicy sizePolicy1(QSizePolicy::Minimum, QSizePolicy::Minimum);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(symptomsTableView->sizePolicy().hasHeightForWidth());
        symptomsTableView->setSizePolicy(sizePolicy1);
        symptomsTableView->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);
        symptomsTableView->setSelectionBehavior(QAbstractItemView::SelectRows);
        symptomsTableView->horizontalHeader()->setVisible(true);
        symptomsTableView->horizontalHeader()->setCascadingSectionResizes(true);
        symptomsTableView->horizontalHeader()->setDefaultSectionSize(50);
        symptomsTableView->horizontalHeader()->setMinimumSectionSize(0);
        symptomsTableView->horizontalHeader()->setStretchLastSection(false);
        symptomsTableView->verticalHeader()->setVisible(false);
        symptomsTableView->verticalHeader()->setCascadingSectionResizes(true);
        symptomsTableView->verticalHeader()->setDefaultSectionSize(20);
        symptomsTableView->verticalHeader()->setMinimumSectionSize(0);

        healthDetailsGrid_2->addWidget(symptomsTableView, 5, 0, 1, 1);

        currentConditionTableView = new QTableView(gridLayoutWidget_2);
        currentConditionTableView->setObjectName(QStringLiteral("currentConditionTableView"));
        QSizePolicy sizePolicy2(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(currentConditionTableView->sizePolicy().hasHeightForWidth());
        currentConditionTableView->setSizePolicy(sizePolicy2);
        currentConditionTableView->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);
        currentConditionTableView->setSelectionBehavior(QAbstractItemView::SelectRows);
        currentConditionTableView->horizontalHeader()->setVisible(true);
        currentConditionTableView->horizontalHeader()->setCascadingSectionResizes(true);
        currentConditionTableView->horizontalHeader()->setDefaultSectionSize(50);
        currentConditionTableView->horizontalHeader()->setMinimumSectionSize(0);
        currentConditionTableView->horizontalHeader()->setStretchLastSection(false);
        currentConditionTableView->verticalHeader()->setVisible(false);
        currentConditionTableView->verticalHeader()->setCascadingSectionResizes(true);
        currentConditionTableView->verticalHeader()->setDefaultSectionSize(20);
        currentConditionTableView->verticalHeader()->setMinimumSectionSize(0);

        healthDetailsGrid_2->addWidget(currentConditionTableView, 1, 0, 1, 1);

        pastConditionsTableView = new QTableView(gridLayoutWidget_2);
        pastConditionsTableView->setObjectName(QStringLiteral("pastConditionsTableView"));
        sizePolicy2.setHeightForWidth(pastConditionsTableView->sizePolicy().hasHeightForWidth());
        pastConditionsTableView->setSizePolicy(sizePolicy2);
        pastConditionsTableView->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);
        pastConditionsTableView->setSelectionBehavior(QAbstractItemView::SelectRows);
        pastConditionsTableView->horizontalHeader()->setVisible(true);
        pastConditionsTableView->horizontalHeader()->setCascadingSectionResizes(true);
        pastConditionsTableView->horizontalHeader()->setDefaultSectionSize(50);
        pastConditionsTableView->horizontalHeader()->setMinimumSectionSize(0);
        pastConditionsTableView->horizontalHeader()->setStretchLastSection(false);
        pastConditionsTableView->verticalHeader()->setVisible(false);
        pastConditionsTableView->verticalHeader()->setCascadingSectionResizes(true);
        pastConditionsTableView->verticalHeader()->setDefaultSectionSize(20);
        pastConditionsTableView->verticalHeader()->setMinimumSectionSize(0);

        healthDetailsGrid_2->addWidget(pastConditionsTableView, 1, 1, 1, 1);

        familyConditionTableView = new QTableView(gridLayoutWidget_2);
        familyConditionTableView->setObjectName(QStringLiteral("familyConditionTableView"));
        sizePolicy2.setHeightForWidth(familyConditionTableView->sizePolicy().hasHeightForWidth());
        familyConditionTableView->setSizePolicy(sizePolicy2);
        familyConditionTableView->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);
        familyConditionTableView->setSelectionBehavior(QAbstractItemView::SelectRows);
        familyConditionTableView->horizontalHeader()->setVisible(true);
        familyConditionTableView->horizontalHeader()->setCascadingSectionResizes(true);
        familyConditionTableView->horizontalHeader()->setDefaultSectionSize(50);
        familyConditionTableView->horizontalHeader()->setMinimumSectionSize(0);
        familyConditionTableView->horizontalHeader()->setStretchLastSection(false);
        familyConditionTableView->verticalHeader()->setVisible(false);
        familyConditionTableView->verticalHeader()->setCascadingSectionResizes(true);
        familyConditionTableView->verticalHeader()->setDefaultSectionSize(20);
        familyConditionTableView->verticalHeader()->setMinimumSectionSize(0);

        healthDetailsGrid_2->addWidget(familyConditionTableView, 1, 2, 1, 1);

        treatmentsTableView = new QTableView(gridLayoutWidget_2);
        treatmentsTableView->setObjectName(QStringLiteral("treatmentsTableView"));
        sizePolicy1.setHeightForWidth(treatmentsTableView->sizePolicy().hasHeightForWidth());
        treatmentsTableView->setSizePolicy(sizePolicy1);
        treatmentsTableView->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);
        treatmentsTableView->setSelectionBehavior(QAbstractItemView::SelectRows);
        treatmentsTableView->horizontalHeader()->setVisible(true);
        treatmentsTableView->horizontalHeader()->setCascadingSectionResizes(true);
        treatmentsTableView->horizontalHeader()->setDefaultSectionSize(50);
        treatmentsTableView->horizontalHeader()->setMinimumSectionSize(0);
        treatmentsTableView->horizontalHeader()->setStretchLastSection(false);
        treatmentsTableView->verticalHeader()->setVisible(false);
        treatmentsTableView->verticalHeader()->setCascadingSectionResizes(true);
        treatmentsTableView->verticalHeader()->setDefaultSectionSize(20);
        treatmentsTableView->verticalHeader()->setMinimumSectionSize(0);

        healthDetailsGrid_2->addWidget(treatmentsTableView, 5, 1, 1, 1);

        allergiesTableView = new QTableView(gridLayoutWidget_2);
        allergiesTableView->setObjectName(QStringLiteral("allergiesTableView"));
        allergiesTableView->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);
        allergiesTableView->setSelectionBehavior(QAbstractItemView::SelectRows);
        allergiesTableView->horizontalHeader()->setVisible(true);
        allergiesTableView->horizontalHeader()->setCascadingSectionResizes(true);
        allergiesTableView->horizontalHeader()->setDefaultSectionSize(50);
        allergiesTableView->horizontalHeader()->setMinimumSectionSize(0);
        allergiesTableView->horizontalHeader()->setStretchLastSection(false);
        allergiesTableView->verticalHeader()->setVisible(false);
        allergiesTableView->verticalHeader()->setCascadingSectionResizes(true);
        allergiesTableView->verticalHeader()->setDefaultSectionSize(20);
        allergiesTableView->verticalHeader()->setMinimumSectionSize(0);

        healthDetailsGrid_2->addWidget(allergiesTableView, 5, 2, 1, 1);

        lifestyleData = new QPushButton(referral_page);
        lifestyleData->setObjectName(QStringLiteral("lifestyleData"));
        lifestyleData->setGeometry(QRect(10, 320, 171, 21));
        formLayoutWidget_10 = new QWidget(referral_page);
        formLayoutWidget_10->setObjectName(QStringLiteral("formLayoutWidget_10"));
        formLayoutWidget_10->setGeometry(QRect(10, 10, 213, 93));
        referralDetailsForm_2 = new QFormLayout(formLayoutWidget_10);
        referralDetailsForm_2->setObjectName(QStringLiteral("referralDetailsForm_2"));
        referralDetailsForm_2->setContentsMargins(0, 0, 0, 0);
        label_37 = new QLabel(formLayoutWidget_10);
        label_37->setObjectName(QStringLiteral("label_37"));
        label_37->setFont(font);

        referralDetailsForm_2->setWidget(0, QFormLayout::LabelRole, label_37);

        label_38 = new QLabel(formLayoutWidget_10);
        label_38->setObjectName(QStringLiteral("label_38"));

        referralDetailsForm_2->setWidget(1, QFormLayout::LabelRole, label_38);

        referralID = new QLineEdit(formLayoutWidget_10);
        referralID->setObjectName(QStringLiteral("referralID"));
        referralID->setReadOnly(true);

        referralDetailsForm_2->setWidget(1, QFormLayout::FieldRole, referralID);

        label_39 = new QLabel(formLayoutWidget_10);
        label_39->setObjectName(QStringLiteral("label_39"));

        referralDetailsForm_2->setWidget(2, QFormLayout::LabelRole, label_39);

        referralDate = new QDateEdit(formLayoutWidget_10);
        referralDate->setObjectName(QStringLiteral("referralDate"));

        referralDetailsForm_2->setWidget(2, QFormLayout::FieldRole, referralDate);

        label_9 = new QLabel(formLayoutWidget_10);
        label_9->setObjectName(QStringLiteral("label_9"));

        referralDetailsForm_2->setWidget(3, QFormLayout::LabelRole, label_9);

        referralStatusEdit = new QLineEdit(formLayoutWidget_10);
        referralStatusEdit->setObjectName(QStringLiteral("referralStatusEdit"));
        referralStatusEdit->setReadOnly(true);

        referralDetailsForm_2->setWidget(3, QFormLayout::FieldRole, referralStatusEdit);

        diagnosisBtn = new QPushButton(referral_page);
        diagnosisBtn->setObjectName(QStringLiteral("diagnosisBtn"));
        diagnosisBtn->setEnabled(false);
        diagnosisBtn->setGeometry(QRect(10, 350, 171, 21));
        formLayoutWidget_8 = new QWidget(referral_page);
        formLayoutWidget_8->setObjectName(QStringLiteral("formLayoutWidget_8"));
        formLayoutWidget_8->setGeometry(QRect(570, 10, 263, 222));
        specialistDetailsForm_2 = new QFormLayout(formLayoutWidget_8);
        specialistDetailsForm_2->setObjectName(QStringLiteral("specialistDetailsForm_2"));
        specialistDetailsForm_2->setContentsMargins(0, 0, 0, 0);
        label_29 = new QLabel(formLayoutWidget_8);
        label_29->setObjectName(QStringLiteral("label_29"));
        QFont font1;
        font1.setBold(true);
        font1.setUnderline(false);
        font1.setWeight(75);
        label_29->setFont(font1);

        specialistDetailsForm_2->setWidget(0, QFormLayout::LabelRole, label_29);

        label_5 = new QLabel(formLayoutWidget_8);
        label_5->setObjectName(QStringLiteral("label_5"));

        specialistDetailsForm_2->setWidget(1, QFormLayout::LabelRole, label_5);

        specialistIDEdit = new QLineEdit(formLayoutWidget_8);
        specialistIDEdit->setObjectName(QStringLiteral("specialistIDEdit"));
        specialistIDEdit->setReadOnly(true);

        specialistDetailsForm_2->setWidget(1, QFormLayout::FieldRole, specialistIDEdit);

        label_25 = new QLabel(formLayoutWidget_8);
        label_25->setObjectName(QStringLiteral("label_25"));

        specialistDetailsForm_2->setWidget(2, QFormLayout::LabelRole, label_25);

        specialistFirstName_2 = new QLineEdit(formLayoutWidget_8);
        specialistFirstName_2->setObjectName(QStringLiteral("specialistFirstName_2"));

        specialistDetailsForm_2->setWidget(2, QFormLayout::FieldRole, specialistFirstName_2);

        label_26 = new QLabel(formLayoutWidget_8);
        label_26->setObjectName(QStringLiteral("label_26"));

        specialistDetailsForm_2->setWidget(3, QFormLayout::LabelRole, label_26);

        specialistLastName_2 = new QLineEdit(formLayoutWidget_8);
        specialistLastName_2->setObjectName(QStringLiteral("specialistLastName_2"));

        specialistDetailsForm_2->setWidget(3, QFormLayout::FieldRole, specialistLastName_2);

        label_28 = new QLabel(formLayoutWidget_8);
        label_28->setObjectName(QStringLiteral("label_28"));

        specialistDetailsForm_2->setWidget(4, QFormLayout::LabelRole, label_28);

        specialistFacility_2 = new QLineEdit(formLayoutWidget_8);
        specialistFacility_2->setObjectName(QStringLiteral("specialistFacility_2"));

        specialistDetailsForm_2->setWidget(4, QFormLayout::FieldRole, specialistFacility_2);

        label_27 = new QLabel(formLayoutWidget_8);
        label_27->setObjectName(QStringLiteral("label_27"));

        specialistDetailsForm_2->setWidget(5, QFormLayout::LabelRole, label_27);

        comboBox_2 = new QComboBox(formLayoutWidget_8);
        comboBox_2->setObjectName(QStringLiteral("comboBox_2"));

        specialistDetailsForm_2->setWidget(5, QFormLayout::FieldRole, comboBox_2);

        chooseSpecialistBtn = new QPushButton(formLayoutWidget_8);
        chooseSpecialistBtn->setObjectName(QStringLiteral("chooseSpecialistBtn"));

        specialistDetailsForm_2->setWidget(6, QFormLayout::SpanningRole, chooseSpecialistBtn);

        specialistErrorLabel = new QLabel(formLayoutWidget_8);
        specialistErrorLabel->setObjectName(QStringLiteral("specialistErrorLabel"));
        specialistErrorLabel->setAlignment(Qt::AlignCenter);

        specialistDetailsForm_2->setWidget(8, QFormLayout::SpanningRole, specialistErrorLabel);

        refreshSpecialist = new QPushButton(formLayoutWidget_8);
        refreshSpecialist->setObjectName(QStringLiteral("refreshSpecialist"));

        specialistDetailsForm_2->setWidget(7, QFormLayout::SpanningRole, refreshSpecialist);

        formLayoutWidget_2 = new QWidget(referral_page);
        formLayoutWidget_2->setObjectName(QStringLiteral("formLayoutWidget_2"));
        formLayoutWidget_2->setGeometry(QRect(210, 140, 191, 131));
        formLayout_2 = new QFormLayout(formLayoutWidget_2);
        formLayout_2->setObjectName(QStringLiteral("formLayout_2"));
        formLayout_2->setContentsMargins(0, 0, 0, 0);
        label_40 = new QLabel(formLayoutWidget_2);
        label_40->setObjectName(QStringLiteral("label_40"));

        formLayout_2->setWidget(0, QFormLayout::LabelRole, label_40);

        streetText = new QLineEdit(formLayoutWidget_2);
        streetText->setObjectName(QStringLiteral("streetText"));

        formLayout_2->setWidget(0, QFormLayout::FieldRole, streetText);

        label_101 = new QLabel(formLayoutWidget_2);
        label_101->setObjectName(QStringLiteral("label_101"));

        formLayout_2->setWidget(1, QFormLayout::LabelRole, label_101);

        cityText = new QLineEdit(formLayoutWidget_2);
        cityText->setObjectName(QStringLiteral("cityText"));

        formLayout_2->setWidget(1, QFormLayout::FieldRole, cityText);

        postcode = new QLineEdit(formLayoutWidget_2);
        postcode->setObjectName(QStringLiteral("postcode"));

        formLayout_2->setWidget(2, QFormLayout::FieldRole, postcode);

        phoneNumberTex = new QLineEdit(formLayoutWidget_2);
        phoneNumberTex->setObjectName(QStringLiteral("phoneNumberTex"));

        formLayout_2->setWidget(3, QFormLayout::FieldRole, phoneNumberTex);

        emailText = new QLineEdit(formLayoutWidget_2);
        emailText->setObjectName(QStringLiteral("emailText"));

        formLayout_2->setWidget(4, QFormLayout::FieldRole, emailText);

        label_102 = new QLabel(formLayoutWidget_2);
        label_102->setObjectName(QStringLiteral("label_102"));

        formLayout_2->setWidget(2, QFormLayout::LabelRole, label_102);

        label_103 = new QLabel(formLayoutWidget_2);
        label_103->setObjectName(QStringLiteral("label_103"));

        formLayout_2->setWidget(3, QFormLayout::LabelRole, label_103);

        label_104 = new QLabel(formLayoutWidget_2);
        label_104->setObjectName(QStringLiteral("label_104"));

        formLayout_2->setWidget(4, QFormLayout::LabelRole, label_104);

        label_100 = new QLabel(referral_page);
        label_100->setObjectName(QStringLiteral("label_100"));
        label_100->setGeometry(QRect(211, 90, 16, 16));
        label_105 = new QLabel(referral_page);
        label_105->setObjectName(QStringLiteral("label_105"));
        label_105->setGeometry(QRect(287, 71, 16, 16));
        label_98 = new QLabel(referral_page);
        label_98->setObjectName(QStringLiteral("label_98"));
        label_98->setGeometry(QRect(211, 71, 16, 16));
        label_8 = new QLabel(referral_page);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setGeometry(QRect(211, 109, 16, 16));
        stackedWidget->addWidget(referral_page);
        submit_referral_page = new QWidget();
        submit_referral_page->setObjectName(QStringLiteral("submit_referral_page"));
        sendReferralBtn = new QPushButton(submit_referral_page);
        sendReferralBtn->setObjectName(QStringLiteral("sendReferralBtn"));
        sendReferralBtn->setGeometry(QRect(310, 430, 201, 51));
        evaluateEmergencyBtn = new QPushButton(submit_referral_page);
        evaluateEmergencyBtn->setObjectName(QStringLiteral("evaluateEmergencyBtn"));
        evaluateEmergencyBtn->setGeometry(QRect(310, 190, 201, 31));
        formLayoutWidget = new QWidget(submit_referral_page);
        formLayoutWidget->setObjectName(QStringLiteral("formLayoutWidget"));
        formLayoutWidget->setGeometry(QRect(310, 230, 201, 181));
        formLayout = new QFormLayout(formLayoutWidget);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        formLayout->setContentsMargins(0, 0, 0, 0);
        label = new QLabel(formLayoutWidget);
        label->setObjectName(QStringLiteral("label"));

        formLayout->setWidget(1, QFormLayout::FieldRole, label);

        label_3 = new QLabel(formLayoutWidget);
        label_3->setObjectName(QStringLiteral("label_3"));

        formLayout->setWidget(3, QFormLayout::FieldRole, label_3);

        symptomSeverityResultLabel = new QLabel(formLayoutWidget);
        symptomSeverityResultLabel->setObjectName(QStringLiteral("symptomSeverityResultLabel"));

        formLayout->setWidget(4, QFormLayout::FieldRole, symptomSeverityResultLabel);

        label_7 = new QLabel(formLayoutWidget);
        label_7->setObjectName(QStringLiteral("label_7"));

        formLayout->setWidget(5, QFormLayout::FieldRole, label_7);

        healthResultLabel = new QLabel(formLayoutWidget);
        healthResultLabel->setObjectName(QStringLiteral("healthResultLabel"));

        formLayout->setWidget(6, QFormLayout::FieldRole, healthResultLabel);

        symptomSeverityResultLabel_3 = new QLabel(formLayoutWidget);
        symptomSeverityResultLabel_3->setObjectName(QStringLiteral("symptomSeverityResultLabel_3"));

        formLayout->setWidget(7, QFormLayout::FieldRole, symptomSeverityResultLabel_3);

        priorityLabelResult = new QLabel(formLayoutWidget);
        priorityLabelResult->setObjectName(QStringLiteral("priorityLabelResult"));

        formLayout->setWidget(8, QFormLayout::FieldRole, priorityLabelResult);

        referralEmergencyResultLabel = new QLabel(formLayoutWidget);
        referralEmergencyResultLabel->setObjectName(QStringLiteral("referralEmergencyResultLabel"));

        formLayout->setWidget(2, QFormLayout::FieldRole, referralEmergencyResultLabel);

        label_6 = new QLabel(submit_referral_page);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(290, 30, 221, 21));
        QFont font2;
        font2.setPointSize(18);
        font2.setBold(true);
        font2.setWeight(75);
        label_6->setFont(font2);
        stackedWidget->addWidget(submit_referral_page);

        retranslateUi(Referral);

        stackedWidget->setCurrentIndex(1);


        QMetaObject::connectSlotsByName(Referral);
    } // setupUi

    void retranslateUi(QWidget *Referral)
    {
        Referral->setWindowTitle(QApplication::translate("Referral", "Referral", 0));
        newPatientReferralBtn->setText(QApplication::translate("Referral", "New Patient", 0));
        label_46->setText(QApplication::translate("Referral", "GP Details", 0));
        label_47->setText(QApplication::translate("Referral", "First name", 0));
        label_76->setText(QApplication::translate("Referral", "Last name", 0));
        label_48->setText(QApplication::translate("Referral", "Facility", 0));
        pushButton_2->setText(QApplication::translate("Referral", "View GP", 0));
        gpFirstName->setText(QString());
        label_4->setText(QApplication::translate("Referral", "GP ID", 0));
        label_30->setText(QApplication::translate("Referral", "Patient Details", 0));
        label_41->setText(QString());
        label_2->setText(QApplication::translate("Referral", "Patient ID", 0));
        label_31->setText(QApplication::translate("Referral", "First name", 0));
        label_32->setText(QApplication::translate("Referral", "Last Name", 0));
        label_33->setText(QApplication::translate("Referral", "Age", 0));
        label_34->setText(QApplication::translate("Referral", "Gender", 0));
        genderComboBox->clear();
        genderComboBox->insertItems(0, QStringList()
         << QApplication::translate("Referral", "Male", 0)
         << QApplication::translate("Referral", "Female", 0)
        );
        genderComboBox->setCurrentText(QApplication::translate("Referral", "Male", 0));
        healthDataBtn->setText(QApplication::translate("Referral", "View Health Data", 0));
        label_71->setText(QApplication::translate("Referral", "Reason for referral", 0));
        submitButton->setText(QApplication::translate("Referral", "Submit referral", 0));
        saveButton->setText(QApplication::translate("Referral", "Save", 0));
        backButton->setText(QApplication::translate("Referral", "Return to Menu", 0));
        label_72->setText(QApplication::translate("Referral", "Current conditions", 0));
        addAllergieBtn->setText(QApplication::translate("Referral", "Add Allergie", 0));
        label_74->setText(QApplication::translate("Referral", "Symptoms", 0));
        label_75->setText(QApplication::translate("Referral", "Allergies", 0));
        label_69->setText(QApplication::translate("Referral", "Family conditions", 0));
        label_73->setText(QApplication::translate("Referral", "Treatments", 0));
        addTreatmentBtn->setText(QApplication::translate("Referral", "Add Treatment", 0));
        label_68->setText(QApplication::translate("Referral", "Past conditions", 0));
        addSymptomBtn->setText(QApplication::translate("Referral", "Add Symptom", 0));
        addConditionBtn->setText(QApplication::translate("Referral", "Add Condition", 0));
        refreshListsBtn->setText(QApplication::translate("Referral", "Refresh", 0));
        lifestyleData->setText(QApplication::translate("Referral", "View Lifestyle Data", 0));
        label_37->setText(QApplication::translate("Referral", "Referral Details", 0));
        label_38->setText(QApplication::translate("Referral", "ID", 0));
        referralID->setText(QString());
        label_39->setText(QApplication::translate("Referral", "Referral Date", 0));
        label_9->setText(QApplication::translate("Referral", "Status ", 0));
        diagnosisBtn->setText(QApplication::translate("Referral", "Patient Diagnosis", 0));
        label_29->setText(QApplication::translate("Referral", "Specialist Details", 0));
        label_5->setText(QApplication::translate("Referral", "Specialist ID", 0));
        label_25->setText(QApplication::translate("Referral", "First name", 0));
        label_26->setText(QApplication::translate("Referral", "Last name", 0));
        label_28->setText(QApplication::translate("Referral", "Facility", 0));
        label_27->setText(QApplication::translate("Referral", "Speciality", 0));
        comboBox_2->clear();
        comboBox_2->insertItems(0, QStringList()
         << QApplication::translate("Referral", "Pediatrician", 0)
         << QApplication::translate("Referral", "Diagnostic", 0)
         << QApplication::translate("Referral", "Addiction Psychiatrist", 0)
         << QApplication::translate("Referral", "Allergist", 0)
         << QApplication::translate("Referral", "Breast Surgeon", 0)
         << QApplication::translate("Referral", "Bariatic Surgeon", 0)
         << QApplication::translate("Referral", "Cardiac Surgeon ", 0)
         << QApplication::translate("Referral", "Cardiologist", 0)
         << QApplication::translate("Referral", "Colorectal Surgeon", 0)
         << QApplication::translate("Referral", "Dermatologist", 0)
         << QApplication::translate("Referral", "Diabetologist", 0)
         << QApplication::translate("Referral", "Oncologist", 0)
         << QApplication::translate("Referral", "Endocrinologist", 0)
         << QApplication::translate("Referral", "Epileptologist", 0)
         << QApplication::translate("Referral", "Gastroenterologist", 0)
         << QApplication::translate("Referral", "Hematologist", 0)
         << QApplication::translate("Referral", "Hepatologist", 0)
         << QApplication::translate("Referral", "Infectious disease specialist", 0)
         << QApplication::translate("Referral", "Gynecologist", 0)
        );
        chooseSpecialistBtn->setText(QApplication::translate("Referral", "Choose Specialist", 0));
        specialistErrorLabel->setText(QString());
        refreshSpecialist->setText(QApplication::translate("Referral", "Refresh", 0));
        label_40->setText(QApplication::translate("Referral", "Street", 0));
        label_101->setText(QApplication::translate("Referral", "City", 0));
        label_102->setText(QApplication::translate("Referral", "Post Code", 0));
        label_103->setText(QApplication::translate("Referral", "Phone Number", 0));
        label_104->setText(QApplication::translate("Referral", "Email", 0));
        label_100->setText(QString());
        label_105->setText(QString());
        label_98->setText(QString());
        label_8->setText(QString());
        sendReferralBtn->setText(QApplication::translate("Referral", "Send", 0));
        evaluateEmergencyBtn->setText(QApplication::translate("Referral", "Evaluate Referral Emergency", 0));
        label->setText(QApplication::translate("Referral", "The emergency of the referral is:", 0));
        label_3->setText(QApplication::translate("Referral", "Overall Symptom Severity: ", 0));
        symptomSeverityResultLabel->setText(QString());
        label_7->setText(QApplication::translate("Referral", "Health Rating:", 0));
        healthResultLabel->setText(QString());
        symptomSeverityResultLabel_3->setText(QApplication::translate("Referral", "Priority Rating:", 0));
        priorityLabelResult->setText(QString());
        referralEmergencyResultLabel->setText(QString());
        label_6->setText(QApplication::translate("Referral", "Referral Overview", 0));
    } // retranslateUi

};

namespace Ui {
    class Referral: public Ui_Referral {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_REFERRAL_H
