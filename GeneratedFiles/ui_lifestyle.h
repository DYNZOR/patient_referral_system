/********************************************************************************
** Form generated from reading UI file 'lifestyle.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LIFESTYLE_H
#define UI_LIFESTYLE_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_LifestyleDialog
{
public:
    QWidget *formLayoutWidget;
    QFormLayout *formLayout_2;
    QLabel *label_6;
    QComboBox *drinkerCombo;
    QLabel *label_9;
    QComboBox *smokerCombo;
    QLabel *label_7;
    QPushButton *updateLifestyle;
    QPushButton *closeLifestyleBtn;
    QComboBox *exerciseCombo;

    void setupUi(QWidget *LifestyleDialog)
    {
        if (LifestyleDialog->objectName().isEmpty())
            LifestyleDialog->setObjectName(QStringLiteral("LifestyleDialog"));
        LifestyleDialog->resize(220, 158);
        formLayoutWidget = new QWidget(LifestyleDialog);
        formLayoutWidget->setObjectName(QStringLiteral("formLayoutWidget"));
        formLayoutWidget->setGeometry(QRect(10, 10, 203, 141));
        formLayout_2 = new QFormLayout(formLayoutWidget);
        formLayout_2->setObjectName(QStringLiteral("formLayout_2"));
        formLayout_2->setContentsMargins(0, 0, 0, 0);
        label_6 = new QLabel(formLayoutWidget);
        label_6->setObjectName(QStringLiteral("label_6"));

        formLayout_2->setWidget(0, QFormLayout::LabelRole, label_6);

        drinkerCombo = new QComboBox(formLayoutWidget);
        drinkerCombo->setObjectName(QStringLiteral("drinkerCombo"));

        formLayout_2->setWidget(0, QFormLayout::FieldRole, drinkerCombo);

        label_9 = new QLabel(formLayoutWidget);
        label_9->setObjectName(QStringLiteral("label_9"));

        formLayout_2->setWidget(1, QFormLayout::LabelRole, label_9);

        smokerCombo = new QComboBox(formLayoutWidget);
        smokerCombo->setObjectName(QStringLiteral("smokerCombo"));

        formLayout_2->setWidget(1, QFormLayout::FieldRole, smokerCombo);

        label_7 = new QLabel(formLayoutWidget);
        label_7->setObjectName(QStringLiteral("label_7"));

        formLayout_2->setWidget(2, QFormLayout::LabelRole, label_7);

        updateLifestyle = new QPushButton(formLayoutWidget);
        updateLifestyle->setObjectName(QStringLiteral("updateLifestyle"));

        formLayout_2->setWidget(3, QFormLayout::SpanningRole, updateLifestyle);

        closeLifestyleBtn = new QPushButton(formLayoutWidget);
        closeLifestyleBtn->setObjectName(QStringLiteral("closeLifestyleBtn"));

        formLayout_2->setWidget(4, QFormLayout::SpanningRole, closeLifestyleBtn);

        exerciseCombo = new QComboBox(formLayoutWidget);
        exerciseCombo->setObjectName(QStringLiteral("exerciseCombo"));

        formLayout_2->setWidget(2, QFormLayout::FieldRole, exerciseCombo);


        retranslateUi(LifestyleDialog);

        QMetaObject::connectSlotsByName(LifestyleDialog);
    } // setupUi

    void retranslateUi(QWidget *LifestyleDialog)
    {
        LifestyleDialog->setWindowTitle(QApplication::translate("LifestyleDialog", "Lifestyle", 0));
        label_6->setText(QApplication::translate("LifestyleDialog", "Drinker", 0));
        drinkerCombo->clear();
        drinkerCombo->insertItems(0, QStringList()
         << QApplication::translate("LifestyleDialog", "None", 0)
         << QApplication::translate("LifestyleDialog", "Low", 0)
         << QApplication::translate("LifestyleDialog", "Moderate", 0)
         << QApplication::translate("LifestyleDialog", "Heavy", 0)
        );
        label_9->setText(QApplication::translate("LifestyleDialog", "Smoker", 0));
        smokerCombo->clear();
        smokerCombo->insertItems(0, QStringList()
         << QApplication::translate("LifestyleDialog", "None", 0)
         << QApplication::translate("LifestyleDialog", "Light", 0)
         << QApplication::translate("LifestyleDialog", "Moderate", 0)
         << QApplication::translate("LifestyleDialog", "Heavy", 0)
        );
        label_7->setText(QApplication::translate("LifestyleDialog", "Exercise", 0));
        updateLifestyle->setText(QApplication::translate("LifestyleDialog", "Update", 0));
        closeLifestyleBtn->setText(QApplication::translate("LifestyleDialog", "Close", 0));
        exerciseCombo->clear();
        exerciseCombo->insertItems(0, QStringList()
         << QApplication::translate("LifestyleDialog", "Daily", 0)
         << QApplication::translate("LifestyleDialog", "Weekly", 0)
         << QApplication::translate("LifestyleDialog", "Monthly", 0)
         << QApplication::translate("LifestyleDialog", "Never", 0)
        );
    } // retranslateUi

};

namespace Ui {
    class LifestyleDialog: public Ui_LifestyleDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LIFESTYLE_H
