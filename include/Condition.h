#ifndef CONDITION_H
#define CONDITION_H


#include <QtCore/QString>

enum Status {
	ONGOING,
	PAST
};

class Condition
{
private:

	QString sName; //

	const char* description;

	QString sBodyLocation;

	Status conditionStatus;

	double fDiscomfort;
	double fDurationDays;

	double fSeverity;

protected:

public:
	Condition(QString sNameIn, QString sBodyLocationIn, double discomfortIn, double daysIn);
	~Condition();

	void setDescription();

	void setDiscomfort(double discomfortIn);
	void setDurationInDays(double daysIn);

	void setSeverity(double severityIn);

	double getDiscomfort();
	double getDurationInDays();
	double getSeverity();

};

#endif 