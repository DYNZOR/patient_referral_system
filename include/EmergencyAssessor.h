#ifndef EMERGENCYASSESSOR_H
#define EMERGENCYASSESSOR_H

#include <string>
#include <fl/Headers.h>

using namespace fl;

class EmergencyAssessor
{
private:

	Engine* engine;

	InputVariable* inputVariable1;
	InputVariable* inputVariable2;
	InputVariable* inputVariable3;

	OutputVariable* outputVariable;

	RuleBlock* ruleBlock;

protected:

public:
	EmergencyAssessor();

	void setOverralSymptomSeverity(double overallSymptomSeverityIn);
	void setHealth(double healthIn);
	void setPriority(double priorityIn);

	double getEmergency();

};

#endif 