#ifndef SYMPTOMDIALOG_H
#define SYMPTOMDIALOG_H

#include "State.h"
#include "StateManager.h"
#include "ReferralManager.h"
#include <QDialog>

#include "ui_symptom.h"

class SymptomDialog : public QWidget
{
	Q_OBJECT

private:

	Ui::SymptomDialog uiDialog;

	std::shared_ptr<Symptom> activeSymptom;


	bool bIsNewSymptom;

	void loadSymptom(); //!< Loads current active symptom 


protected:

public:

	SymptomDialog(std::shared_ptr<Symptom> symptomIn, bool bIsNew, QWidget* parent = 0);

	void setupUserInterface();
	void setupSignals();

	private slots:

	void addSymptom(); //!< Insert new symptom into database and updates referral 
	void removeSymptom();
	void updateSymptom();

	void closeDialog();
};

#endif 