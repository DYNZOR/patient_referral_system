#ifndef LIFESTYLEASSESSOR_H
#define LIFESTYLEASSESSOR_H

#include <string>
#include <fl/Headers.h>

using namespace fl;

class LifestyleAssessor
{
private:

	Engine* engine;

	InputVariable* inputVariable1;
	InputVariable* inputVariable2;
	InputVariable* inputVariable3;

	OutputVariable* outputVariable;

	RuleBlock* ruleBlock;

protected:


public:
	LifestyleAssessor();

	void setAlcohol(int alcoholIn);
	void setSmoke(int smokeIn);
	void setExercise(int exerciseIn);

	double getRating();


};

#endif 