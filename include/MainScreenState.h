#ifndef MAINSCREENSTATE_H
#define MAINSCREENSTATE_H

#include "StateManager.h"
#include "ViewReferralsState.h"
#include "LoginState.h"

class MainScreenState: public QWidget
{
	Q_OBJECT

private:

	Ui_MainScreen uiMainScreen; //!< User interface 

protected:
public:


	MainScreenState(QWidget* parent = 0);

	void setupUserInterface();
	void setupSignals();

	private slots:

	void logOut();
	void viewReferrals();

};

#endif 