#ifndef STATEMANAGER_H
#define STATEMANAGER_H

//#include <QtWidgets/QMainWindow>
#include "ui_Login.h"
#include "ui_MainScreen.h"
#include "ui_referral.h"
#include "ui_view_referrals.h"
#include "Connection.h"
#include "SymptomDialog.h"

class StateManager
{

private:
	static StateManager* stateManager;

	std::vector<QWidget*> states;

	QWidget* activeDialog;
	//std::vector<QDialog*> dialogs;

protected:

	StateManager() {

	}

public:
	
	static StateManager* StateManagerInstance() {
		if (!stateManager) {
			return stateManager = new StateManager();
		}
		else {
			return stateManager;

		}

	}  //!< Returns a pointer to a StateManager instance

	// Setup Contructor 
	void changeState(QWidget* newState); 

	void showDialog(QWidget* dialog);
	void closeDialog();
	void pushState(QWidget* newState);
	void popState(QWidget* newState);

	QWidget* getCurrentState();

};

#endif 