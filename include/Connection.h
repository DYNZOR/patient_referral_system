#ifndef CONNECTION_H
#define CONNECTION_H

#include "QtSql"
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlDriver>
#include <QtSql/QSqlQuery>

class Connection {

public:

	static Connection* DatabaseConnection(){
		if (!pDbConnection) {

			return pDbConnection = new Connection();
		}
		else {
			return pDbConnection;
		}

	}
	QSqlDatabase getHandle();
	bool connected();

protected:

	Connection();
	~Connection();

private: 
	static Connection* pDbConnection;

	QSqlDatabase db;

	QString sHostName;
	QString sDatabaseName;
	QString sUserName;
	QString sPassword;
	
	void connect();
	bool establishedConnection;
};

#endif
