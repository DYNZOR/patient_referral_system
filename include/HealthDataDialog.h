#ifndef HEALTHDATADIALOG_H
#define HEALTHDATADIALOG_H

#include "ui_healthdata.h"
#include "MedicalData.h"
#include <memory>
#include "StateManager.h"

class HealthDataDialog : public QWidget
{
	Q_OBJECT

private:

	Ui::HealthDataDialog uiDialog;

	//std::shared_ptr<MedicalData> pMedicalData;

protected:

public:

	HealthDataDialog( QWidget* parent = 0);

	void setupUserInterface();
	void setupSignals();

	private slots:

	void updateHealthData(); //!< Updates patients health data 
	void closeHealthDataDialog(); 
};

#endif 