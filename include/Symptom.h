#ifndef SYMPTOM_H
#define SYMPTOM_H

#include <QString>

class Symptom
{
private:

	unsigned int uiSymptomID;

	QString sName;

	const char* description;

	QString sBodyLocation;

	unsigned int fDiscomfort;
	unsigned int fDurationDays;


	double fSeverity;

protected:

public:
	Symptom();
	Symptom(unsigned int uiSymptomID, QString sName, QString sBodyLocation, unsigned int discomfortIn, unsigned int daysIn);
	~Symptom();

	void setDescription();

	void setDiscomfort(unsigned int discomfortIn);
	void setDurationInDays(unsigned int daysIn);

	void setID(unsigned int idIn);
	void setSeverity(double severityIn);

	unsigned int getID();
	unsigned int getDiscomfort();
	unsigned int getDurationInDays(); 
	double getSeverity();
	QString getName();
	QString getBodyLocation();	
};

#endif 