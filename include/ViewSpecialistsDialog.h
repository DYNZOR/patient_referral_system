#ifndef VIEWSPECIALISTSDIALOG_H
#define VIEWSPECIALISTSDIALOG_H

#include "ui_viewspecialists.h"
#include <memory>
#include "StateManager.h"

class ViewSpecialistsDialog : public QWidget
{
	Q_OBJECT

private:

	Ui::ViewSpecialists uiDialog;

	QSqlQueryModel* specialistsQueryModel;

protected:

public:

	ViewSpecialistsDialog(QWidget* parent = 0);

	void setupUserInterface();
	void setupSignals();

	private slots:

	void selectSpecialist(const QModelIndex& index); //!< Updates patients health data 
	void closeDialog();


};

#endif 