#ifndef FACILITY_H
#define FACILITY_H

#include <memory>
#include <QString>
#include "Address.h"

class Facility
{
private:

	unsigned int id; 
	QString name;
	QString type;

	std::shared_ptr<Address> pAddress;

protected:


public:
	Facility();
	Facility(unsigned int idIn, QString name, QString type);

	void setName(QString nameIn);
	QString getName();

	unsigned int getId();

	void setAddress(std::shared_ptr<Address> addressIn);
	std::shared_ptr<Address> getAddress();


};

#endif 