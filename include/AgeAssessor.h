#ifndef AGEASSESSOR_H
#define AGEASSESSOR_H

#include <fl/Headers.h>

using namespace fl;

class AgeAssessor
{
private:

	Engine* engine;

	InputVariable* inputVariable;

	OutputVariable* outputVariable;

	RuleBlock* ruleBlock;

protected:


public:
	AgeAssessor();

	void setAge(int valueIn);

	double getPriority();


};

#endif 