#ifndef FUZZYMANAGER_H
#define FUZZYMANAGER_H

#include "SymptomAssessor.h"
#include "HealthAssessor.h"
#include "LifestyleAssessor.h"
#include "EmergencyAssessor.h"
#include "AgeAssessor.h"
#include "TreatmentAssessor.h"

#include <memory>

#include "Referral.h"


class FuzzyManager {

private:
	static FuzzyManager * pFuzzyManager;

	std::shared_ptr<SymptomAssessor> pSymptomAssessor;
	std::shared_ptr<HealthAssessor> pHealthAssessor;
	std::shared_ptr<AgeAssessor> pAgeAssessor;

	std::shared_ptr<EmergencyAssessor> pEmergencyAssessor;
	std::shared_ptr<LifestyleAssessor> pLifestyleAssessor;

	FuzzyManager();
	~FuzzyManager();

public:

	static FuzzyManager* Instance(); //!< Fuzzy manager singleton instance 

	double getSymptomSeverity(std::shared_ptr<Symptom> symptomIn);

	double evaluateReferralEmergency(std::shared_ptr<Referral> referralIn);

};

#endif