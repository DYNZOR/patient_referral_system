#ifndef SPECIALIST_H
#define SPECIALIST_H

#include <QString>
#include <memory>
#include "Facility.h"


class Specialist
{

private:

	//	Increase size of integer for final release 
	unsigned int uiSpecialistID;

	QString sDoctorTitle;
	QString sFirstName;
	QString sLastName;

	QString speciality;

	QString specialistEmail; //!< Patient email 


	std::shared_ptr<Facility> pCurrentFacility; //!< Specialist facility 


protected:

public:

	Specialist();
	Specialist(unsigned int uiSpecialistIDin, QString sFirstNameIn, QString sLastNameIn, QString specialityIn);
	~Specialist();

	void setName(QString titleIn, QString firstNameIn, QString lastNameIn);
	void setSpeciality(QString speciality);

	void setFacility(std::shared_ptr<Facility> addressIn);

	void setContactDetails(QString numberIn, QString emailIn);

	QString getFirstName();
	QString getLastName();
	QString getSpeciality();

	std::shared_ptr<Facility> getFacility();

	QString getEmail();

	unsigned int getID();


};

#endif 