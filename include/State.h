#ifndef STATE_H
#define STATE_H

#include <QtWidgets/QWidget>


enum Type {
	EMPTY,
	LOGIN,
	MAINSCREEN,
	REFERRAL,
	VIEW_REFERRALS,
	PATIENT,
	VIEW_PATIENTS
};

class State
{
protected:
	//UserInterface* ui;

	//QWidget * centralWidget; 
	Type stateType;
	
	State();
public:

	void setType(Type newType);
	Type getType();

	//virtual void setupUserInterface(QMainWindow* window) = 0;
	//virtual void setupSignals(QMainWindow* window) = 0;
};

#endif 