#ifndef TREATMENT_H
#define TREATMENT_H

#include <string>

class Treatment
{
private:
	std::string sName;

	const char* description;

	double fIntensity; //!< Intensity of the treatment 
	double fEffectiveness; //!< Effectiveness of treament on patient

	double fOverall;

protected:

public:
	Treatment(std::string sNameIn, double fIntensityIn, double fEffectivenessIn);
	~Treatment();


	void setDescription();

	void setIntensity(double fIntensityIn);
	void setEffectiveness(double fEffectivenessIn);


	double getIntensity();
	double getEffectiveness();

};

#endif 