#ifndef LIFESTYLEDIALOG_H
#define LIFESTYLEDIALOG_H

#include "ui_lifestyle.h"
#include "Lifestyle.h"
#include <memory>
#include "StateManager.h"

class LifestyleDialog : public QWidget
{
	Q_OBJECT

private:

	Ui::LifestyleDialog uiDialog;

protected:

public:

	LifestyleDialog(QWidget* parent = 0);

	void setupUserInterface();
	void setupSignals();

	private slots:

	void updateLifestyle(); //!< Updates patients health data 
	void closeLifestyleDialog(); 
};

#endif 