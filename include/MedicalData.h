#ifndef MEDICALDATA_H
#define MEDICALDATA_H

#include <QString>

class MedicalData
{
private:

	unsigned int uiBloodPressure;
	unsigned int uiCholesterolLDL;

protected:

public:
	MedicalData();
	MedicalData(unsigned int bpIn, unsigned int cholIn);

	unsigned int getCholesterol();
	unsigned int getBloodPressure();

};

#endif 