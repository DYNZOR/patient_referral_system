#ifndef GP_H
#define GP_H

#include <QString>
#include "Facility.h"
#include <memory>

class GP
{

private:

	unsigned int uiGeneralPractitionerID;

	QString sUsername;
	QString sPassword;

	QString sDoctorTitle;
	QString sFirstName;
	QString sLastName;

	std::shared_ptr<Facility> pCurrentFacility; //!< GP facility 

	QString sEmail; //!< Patient email 
	QString sNumber;

protected:

public:
	GP();
	GP(unsigned int uiGeneralPractitionerIDin, QString titleIn, QString sFirstNameIn, QString sLastNameIn, QString sUsernameIn, QString sPasswordIn);
	~GP();
	
	void setID(unsigned int idIn);


	void setName(QString titleIn, QString firstNameIn, QString lastNameIn);
	void setFacility(std::shared_ptr<Facility> addressIn);
	void setContactDetails(QString numberIn, QString emailIn);

	unsigned int getID();
	QString getFirstName();
	QString getLastName();
	QString getEmail();

	std::shared_ptr<Facility> getFacility();

	


};

#endif 