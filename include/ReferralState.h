#ifndef REFERRALSTATE_H
#define REFERRALSTATE_H

#include "StateManager.h"
#include "ui_symptom.h"
#include "SymptomDialog.h"
#include "LifestyleDialog.h"
#include "HealthDataDialog.h"
#include "ViewSpecialistsDialog.h"
#include "ViewReferralsState.h"
#include "ReferralManager.h"
#include "UserManager.h"
#include "FuzzyManager.h"
#include <QtSql/QSqlQueryModel>

#include <QDebug>


class ReferralState : public QWidget
{
	Q_OBJECT

private:

	Ui_Referral uiReferral;



	QSqlQueryModel* patientSelectQueryModel;
	QSqlQueryModel* symptomsQueryModel;

	void loadActiveReferral();
	void updateLists();
	void loadNewReferral();

protected:

public:

	ReferralState(bool bViewReferral, QWidget* parent = 0);

	void setupUserInterface();
	void setupSignals();

	private slots:

	void newReferral();

	void loadExistingPatient(const QModelIndex& index);

	void submitReferral();
	void saveReferral();
	void addSymptom();

	void refreshSpecialist();
	void refreshLists();

	void chooseSpecialist();
	void viewSymptom(const QModelIndex& index);
	void viewTreatment(const QModelIndex& index);
	void viewCondition(const QModelIndex& index);
	void viewHealthData();
	void viewLifestyleData();

	void back();


	void evaluateEmergency();
	void confirmReferral();

};

#endif 