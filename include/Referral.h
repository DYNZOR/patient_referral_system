#ifndef REFERRAL_H
#define REFERRAL_H


#include <QDate>
#include <QString>

#include "Facility.h"
#include "Symptom.h"
#include "Condition.h"
#include "Treatment.h"
#include "Patient.h"
#include "Specialist.h"
#include "GP.h"
//#include "FuzzyManager.h"

#include <memory>
#include <vector>

class FuzzyManager;

class Referral
{

public:
	Referral();
	Referral(unsigned int uiReferralIdIn, QDate dateIn);

	void setID(unsigned int idIn);
	void setDate(QDate dateIn);

	void setReferralReason(QString textIn);

	void setStatus(QString statusIn);
	void addPatient(std::shared_ptr<Patient> pPatientIn);
	void addGeneralPractitioner(std::shared_ptr<GP> pGPin);
	void addSpecialist(std::shared_ptr<Specialist> pSpecialistIn);

	void addSymptom(std::shared_ptr<Symptom> pSymptomIn);
	void addTreatment(std::shared_ptr<Treatment> pTreatmentIn);
	void addCondition(std::shared_ptr<Condition> pConditionIn);


	QDate getDate();
	QString getStatus();

	QString getReferralReasonText();

	
	unsigned int getID();

	std::shared_ptr<Patient> getPatient();
	std::shared_ptr<GP> getGP();
	std::shared_ptr<Specialist> getSpecialist();

	double getOverallSymptomSeverity();

	//std::shared_ptr<>

protected:

private:

	unsigned int uiReferralID;

	QDate referralDate;
	QString sStatus;


	QString referralReasonText;

	std::shared_ptr<Patient> pPatient;
	std::shared_ptr<GP> pGP;
	std::shared_ptr<Specialist> pSpecialist;
	
	std::vector<std::shared_ptr<Symptom>> symptoms;

	std::vector<std::shared_ptr<Treatment>> treatments;

	std::vector<std::shared_ptr<Condition>> conditions;

};

#endif 