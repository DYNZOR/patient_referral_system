#ifndef LOGINSTATE_H
#define LOGINSTATE_H

#include "State.h"
#include "StateManager.h"
#include "UserManager.h"
#include "MainScreenState.h"
#include "FacilityDialog.h"

class LoginState : public QWidget
{
	Q_OBJECT

private:
	Ui_Login uiLogin;

protected:

public:

	LoginState(QWidget* parent = 0);

	void setupUserInterface();
	void setupSignals();
	
private slots:

	void login();
	void signUp(); 

	void createAccount();

};

#endif 