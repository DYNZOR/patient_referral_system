#ifndef SymptomAssessor_H
#define SymptomAssessor_H

#include <string>
#include <fl/Headers.h>

using namespace fl;

class SymptomAssessor
{
private:
	
	Engine* engine;

	InputVariable* inputVariable1;
	InputVariable* inputVariable2;

	OutputVariable* outputVariable;

	RuleBlock* ruleBlock;

protected:

	
public:
	SymptomAssessor();

	void setDiscomfort(double valueIn);
	void setDuration(double daysValueIn);

	double getSeverity();


};

#endif 