#ifndef REFERRALMANAGER_H
#define REFERRALMANAGER_H

#include "Referral.h"
#include <map>
#include <memory>

class ReferralManager {

private:
	static ReferralManager * pReferralManager;

	std::shared_ptr<Referral> pActiveReferral;

	ReferralManager();
	~ReferralManager();
public:

	static ReferralManager* Instance();

	void createReferral();

	void setActiveReferral(std::shared_ptr<Referral> referralIn); 

	std::shared_ptr<Referral> getActiveReferral();

};

#endif