#ifndef VIEWREFERRALSSTATE_H
#define VIEWREFERRALSSTATE_H

#include <memory>
#include "StateManager.h"
#include <QDebug>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlQueryModel>
#include <QSqlTableModel>
#include <QMessageBox>
#include <qsqlerror.h>
#include "ReferralState.h"
#include "MainScreenState.h"


class ViewReferralsState : public QWidget
{
	Q_OBJECT

private:
	Ui_ViewReferrals uiViewReferrals;

	QSqlQueryModel* referralsQueryModel;

protected:
public:
	ViewReferralsState(QWidget* parent = 0);

	void setupUserInterface();
	void setupSignals();

	void loadReferrals();
	void loadPatients();
	void loadSpecialists();

	private slots:

	void selectReferral(const QModelIndex& index);
	void loadSelectedReferral(const QModelIndex& index);
	void newReferral();
	void backToMainMenu();

};

#endif 