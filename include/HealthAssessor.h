#ifndef HEALTHASSESSOR_H
#define HEALTHASSESSOR_H

#include <string>
#include <fl/Headers.h>

using namespace fl;

class HealthAssessor
{
private:

	Engine* engine;

	InputVariable* inputVariable1;
	InputVariable* inputVariable2;
	InputVariable* inputVariable3;

	OutputVariable* outputVariable;

	RuleBlock* ruleBlock;

protected:


public:
	HealthAssessor();

	void setCholesterol(unsigned int cholesterolIn);
	void setBloodPressure(unsigned int bpIn);

	double getHealth();


};

#endif 