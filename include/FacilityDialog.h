#ifndef FACILITYDIALOG_H
#define FACILITYDIALOG_H


#include "ui_facility.h"
#include <QsqlQuery>
#include <QDebug>
#include <qsqlerror.h>
#include <memory>
#include "Facility.h"
#include "UserManager.h"

class FacilityDialog : public QWidget
{
	Q_OBJECT

private:

	Ui::FacilityDialog uiDialog;

protected:

public:

	FacilityDialog(QWidget* parent = 0);

	void setupUserInterface();
	void setupSignals();

	private slots:

	void addFacility(); //!< Insert new symptom into database and updates referral 

};

#endif 