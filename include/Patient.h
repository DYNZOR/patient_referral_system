#ifndef PATIENT_H
#define PATIENT_H

#include "Address.h"
#include "Lifestyle.h"
#include "MedicalData.h"
#include <QString>
#include <QDate>

#include <memory>

enum Gender {
	MALE,
	FEMALE
};

class Patient
{

private:

	//	Increase size of integer for final release 
	unsigned int uiPatientId; 

	QString sTitle;
	QString sFirstName;
	QString sLastName;
	 
	QDate dateOfBirth;

	unsigned int uiAge;

	QString gender;

	float uiHeight; //!< Height in cm
	float uiWeight; //!< Weight in kg

	std::shared_ptr<Address> pCurrentPatientAdress; //!< Patient address 

	std::shared_ptr<Lifestyle> pLifestyleData; //!< Lifestyle data
	std::shared_ptr<MedicalData> pMedicalData; //!< Medical data

	QString patientNumber; //!< Patient number 
	QString patientEmail; //!< Patient email 

	
protected:

public:

	Patient();
	Patient(unsigned int uiPatientIdIn, QString sFirstNameIn, QString sLastNameIn, unsigned int ageIn);
	~Patient();

	void setName(QString titleIn, QString firstNameIn, QString lastNameIn);
	void setAge(unsigned short int ageIn);
	void setDOB(QDate dobIn);
	void setGender(QString genderIn);
	void setHeight(float uiHeight);
	void setWeight(float uiWeight);

	void setAddress(std::shared_ptr<Address> addressIn);
	void setContactDetails(QString numberIn, QString emailIn);

	void setLifestyle(std::shared_ptr<Lifestyle> pLifestyleIn);
	void setMedicalData(std::shared_ptr<MedicalData> pMedicalDataIn);


	std::shared_ptr<Address> getAddress(); //!< Returns patient address 

	unsigned int getID();
	QString getFirstName();
	QString getLastName();
	QString getTitle();
	QString getGender();
	QString getNumber();
	QString getEmail();

	std::shared_ptr<Lifestyle> getLifestyle();
	std::shared_ptr<MedicalData> getMedicalData();

	QDate getDOB();

	unsigned int getAge();
	float getWeight();
	float getHeight();
};

#endif 