#ifndef ADDRESS_H
#define ADDRESS_H

#include <QString>

class Address
{
private:
	unsigned int addressId;
	QString street;
	QString city;
	QString postcode;

protected:

public:
	Address();

	Address(unsigned int idIn, QString streetIn, QString cityIn, QString postcodeIn);

	~Address();

	void setNewAddress(QString streetIn, QString cityIn, QString postcodeIn);

	QString getStreet();
	QString getCity();
	QString getPostcode();
	unsigned int getID();

};

#endif 









