#ifndef USERMANAGER_H
#define USERMANAGER_H

#include "GP.h"
#include "Specialist.h"
#include <memory>

class UserManager	 {

private:
	static UserManager * pUserManager;

	std::shared_ptr<GP> pActiveGP;

	std::shared_ptr<Specialist> pActiveSpecialist;

	void checkUser(std::shared_ptr<GP> gpIn);

	UserManager();
	~UserManager();
public:

	static UserManager* Instance();

	void setActiveGP(std::shared_ptr<GP> gpIn);

	std::shared_ptr<GP> getActiveGP();

};

#endif