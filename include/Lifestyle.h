#ifndef LIFESTYLE_H
#define LIFESTYLE_H

#include <QString>

class Lifestyle
{
private:

	QString drinker;
	QString smoker;
	QString exercise;

protected:

public:
	Lifestyle();
	Lifestyle(QString drinkerIn, QString smokerIn, QString exerciseIn);

	QString getDrinkerStatus();
	QString getSmokerStatus();
	QString getExerciseStatus();



};

#endif 